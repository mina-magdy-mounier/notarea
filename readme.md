# Notarea
### Mid-Year-Plan
##### DeadLine 3-2-2018


|Name   | Task to present
|-------|------------------------------------------------
|Mina   | Gantt + Screenshots + ERD
|Bob    | 2 Reqs + Stakeholders 
|Taher  | Use Cases + Use Case Diagram
|Hazem  | Idea + Sys Arch + Class Diagram + Conclusion
|Noura  | Problem Significance + Solution + Sequences



- [x] GitLab
- [ ] Documentation
- [ ] Prototype
- [ ] Implementation
- [ ] Design Patterns
- [ ] Machine Learning
- [ ] Presentation

