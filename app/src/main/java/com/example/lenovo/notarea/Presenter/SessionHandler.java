package com.example.lenovo.notarea.Presenter;

import android.content.SharedPreferences;
import android.util.Log;

import com.example.lenovo.notarea.Model.User;
import com.example.lenovo.notarea.R;
import com.example.lenovo.notarea.View.activity.ProfileEditor;

import org.json.JSONObject;

public class SessionHandler {

    final String TAG = "SessionHandler";

    public SessionHandler() {
    }

    public void setSession(JSONObject root, SharedPreferences sharedPreferences) {

        SharedPreferences.Editor session = sharedPreferences.edit();
        session.putString("user_id", String.valueOf(root.optJSONObject("user").optInt("id")));
        session.putString("user_name", root.optJSONObject("user").optString("name"));
        if (root.optJSONObject("user").has("photo")) {
            if (root.optJSONObject("user").optString("photo") != null) {
                if (!root.optJSONObject("user").optString("photo").equals("null")) {
                    session.putString("user_photo", root.optJSONObject("user").optString("photo"));
                }
            }
        }
        session.putString("user_email", root.optJSONObject("user").optString("email"));
        session.putString("user_gender", root.optJSONObject("user").optString("gender"));
        session.putString("user_age", root.optJSONObject("user").optString("age"));
        session.putString("user_phone", root.optJSONObject("user").optString("userphone"));
        session.putInt("user_premium", root.optJSONObject("user").optInt("premium"));

        session.putString("user_location", root.optJSONObject("location").toString());
        session.putString("user_extranumbers", root.optJSONArray("extranumbers").toString());
        session.apply();
    }

    public void getSession(SharedPreferences session, User user) {
        JsonParser jsonParser = new JsonParser();
        user.setId(session.getString("user_id", null));
        user.setName(session.getString("user_name", null));
        jsonParser.setProfileImage(user, session.getString("user_photo", null));
        user.setEmail(session.getString("user_email", null));
        user.setGender(session.getString("user_gender", null));
        user.setAge(session.getString("user_age", null));
        user.setPhone(session.getString("user_phone", null));
        user.setPremium(session.getInt("user_premium", -1));

        jsonParser.setExtraNumbers(user, session.getString("user_extranumbers", null));
        jsonParser.setUserLocation(user, session.getString("user_location", null));
    }

    public void set_token(SharedPreferences session, String name) {
        SharedPreferences.Editor editor = session.edit();
        editor.putString("token", name);
        editor.apply();
    }

    public String get_token(SharedPreferences session) {
        return session.getString("token", null);

    }

    public void set_emergency_id(SharedPreferences session, String name) {
        SharedPreferences.Editor editor = session.edit();
        editor.putString("emergencyID", name);
        editor.apply();
    }

    public String get_emergency_id(SharedPreferences session) {
        return session.getString("emergencyID", null);

    }


    public void update_user_name(SharedPreferences session, String name) {
        SharedPreferences.Editor editor = session.edit();
        editor.putString("user_name", name);
        editor.apply();
    }

    public void update_theme(SharedPreferences session, int theme_indx) {
        SharedPreferences.Editor editor = session.edit();
        editor.putInt("theme", theme_indx);
        editor.apply();
    }

    public int get_theme(SharedPreferences session) {
        return session.getInt("theme", R.style.AppTheme);

    }

    public void update_notification_state(SharedPreferences session, int state) {
        SharedPreferences.Editor editor = session.edit();
        editor.putInt("notification", state);
        editor.apply();
    }

    public int get_notification_state(SharedPreferences session) {
        return session.getInt("notification", 1);

    }

    public void update_profile_info(final ProfileEditor context, SharedPreferences session, User user, JSONObject done, boolean edited_parts[]) {
        SharedPreferences.Editor editor = session.edit();
        JsonParser jsonParser = new JsonParser();
        if (edited_parts[0]) {
            if (done.optString("image").equals("Changed")) {
//                Log.d(TAG, "update_profile_info: "+);
                editor.putString("user_photo", user.getProfile_Img());
            }
        }
        if (edited_parts[1]) {
            if (done.optString("phonenumber").equals("Changed")) {
                editor.putString("user_phone", user.getPhone());
            }
        }

        if (edited_parts[2]) {
            if (done.optString("age").equals("Changed")) {
                editor.putString("user_age", user.getAge());
                Log.d(TAG, "update_profile_info: " + user.getAge());
            }
        }

        if (edited_parts[3]) {
            if (done.optString("gender").equals("Changed")) {
                editor.putString("user_gender", user.getGender());
            }
        }

        if (edited_parts[4] || edited_parts[5]) {
            if (done.optJSONArray("extranumbers") != null) {
                if (done.optJSONArray("extranumbers").length() > 0) {
                    jsonParser.setExtraNumbers(user, done.optJSONArray("extranumbers").toString());
                    editor.putString("user_extranumbers", done.optJSONArray("extranumbers").toString());
                }
            }
        }

        editor.apply();
    }

    public void remember_me(SharedPreferences userSession) {
        SharedPreferences.Editor session = userSession.edit();
        session.putString("remeberMe", "yes");
        session.apply();
    }

}
