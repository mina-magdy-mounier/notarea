package com.example.lenovo.notarea.Presenter;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.example.lenovo.notarea.Model.Emergency;
import com.example.lenovo.notarea.View.activity.MainActivity;

import java.util.List;
import java.util.Locale;


public class LocationServices extends Service

{
    public static final String BROADCAST_ACTION = "Hello World";
    private int requistInterval = 1000 * 5 * 60;  //5 minutes
    public LocationManager locationManager;
    public MyLocationListener listener;
    public Location previousBestLocation = null;
    boolean gps_enabled = false;
    boolean network_enabled = false;
    Emergency emergency = new Emergency();

    Intent intent;
    int counter = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        intent = new Intent(BROADCAST_ACTION);
    }

    @Override
    public void onStart(Intent intent, int startId) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            listener = new MyLocationListener();
            try {
                gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            } catch (Exception ex) {
            }
            try {
                network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            } catch (Exception ex) {
            }

            //don't start listeners if no provider is enabled
            while (!gps_enabled && !network_enabled) {
            }
            if (network_enabled) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }

                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 10f, listener);
                //Toast.makeText( getApplicationContext(), "Network Get location", Toast.LENGTH_SHORT ).show();

            }
            else if(gps_enabled) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10f, listener);
                //Toast.makeText( getApplicationContext(), "GPS Get location", Toast.LENGTH_SHORT ).show();
            }
//            CameraManager mCameraManager;
//            String mCameraId = null;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            mCameraManager = (CameraManager) getApplicationContext().getSystemService(Context.CAMERA_SERVICE);
//            try {
//                for (String camID : mCameraManager.getCameraIdList()){
//                    CameraCharacteristics cameraCharacteristics = mCameraManager.getCameraCharacteristics(camID);
//                    int lensFacing = cameraCharacteristics.get(CameraCharacteristics.LENS_FACING);
//                    if (lensFacing == CameraCharacteristics.LENS_FACING_FRONT
//                            && cameraCharacteristics.get(CameraCharacteristics.FLASH_INFO_AVAILABLE)) {
//                        mCameraId = camID;
//                        break;
//                    } else if (lensFacing == CameraCharacteristics.LENS_FACING_BACK
//                            && cameraCharacteristics.get(CameraCharacteristics.FLASH_INFO_AVAILABLE)) {
//                        mCameraId = camID;
//                    }
//                    if (mCameraId != null) {
//                        mCameraManager.setTorchMode(mCameraId, true);
//                    }
//                }
//            } catch (CameraAccessException e) {
//                e.printStackTrace();
//            }
//        }

    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > requistInterval;
        boolean isSignificantlyOlder = timeDelta < -requistInterval;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }
    /**/
    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    @Override
    public void onDestroy() {
        // handler.removeCallbacks(sendUpdatesToUI);
        super.onDestroy();
        Log.v("STOP_SERVICE", "DONE");
        locationManager.removeUpdates((LocationListener) listener);
    }

    public static Thread performOnBackgroundThread(final Runnable runnable) {
        final Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    runnable.run();
                } finally {

                }
            }
        };
        t.start();
        return t;
    }

    public class MyLocationListener implements LocationListener
    {

        public void onLocationChanged(final Location loc)
        {
            String victim = getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("victim", "");
            if(victim.equals("true")){
                requistInterval=1000 * 60; // 1 minutes
                //update location for victim
            }
            else{requistInterval = 1000 * 60 * 5;}
            if(isBetterLocation(loc, previousBestLocation)) {
                loc.getLatitude();
                loc.getLongitude();
                getApplicationContext().getSharedPreferences("currentUser",Context.MODE_MULTI_PROCESS).edit().putString("currentLat", loc.getLatitude()+"").apply();
                getApplicationContext().getSharedPreferences("currentUser",Context.MODE_MULTI_PROCESS).edit().putString("currentLng", loc.getLongitude()+"").apply();
                getApplicationContext().getSharedPreferences("currentUser",Context.MODE_MULTI_PROCESS).edit().putString("address", getCompleteAddressString(loc.getLatitude(),loc.getLongitude())).apply();
               // Toast.makeText(getApplicationContext(), getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLng", "")+"  Service", Toast.LENGTH_SHORT ).show();
                emergency.setContext(getApplicationContext());
                String userID = getApplicationContext().getSharedPreferences("currentUser", Context.MODE_PRIVATE).getString("user_id", "");
                if(!userID.equals(""))emergency.updateLocation();
            }
        }

        public void onProviderDisabled(String provider)
        {
            Toast.makeText( getApplicationContext(), "Gps Disabled", Toast.LENGTH_SHORT ).show();
        }


        public void onProviderEnabled(String provider)
        {
            Toast.makeText( getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();
        }


        public void onStatusChanged(String provider, int status, Bundle extras)
        {

        }

    }

    public String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        // this.setContext(ma);
        String strAdd = "";
        StringBuilder strReturnedAddress = new StringBuilder("");
        try {

            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);

            if (addresses != null) {
                Address returnedAddress = addresses.get(0);

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            }

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            //Toast.makeText(this, "Address : " + address, Toast.LENGTH_SHORT).show();
            String city = addresses.get(0).getLocality();
            //Toast.makeText(this, "City : " + city, Toast.LENGTH_SHORT).show();
            String state = addresses.get(0).getAdminArea();
            //Toast.makeText(this, "State : " + state, Toast.LENGTH_SHORT).show();
            String country = addresses.get(0).getCountryName();
            //Toast.makeText(this, "Country : " + country, Toast.LENGTH_SHORT).show();
            //String postalCode = addresses.get(0).getPostalCode();
            //Toast.makeText(this,"Postal Code : " + postalCode,Toast.LENGTH_SHORT).show();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
            //Toast.makeText(this, "Known Name : " + knownName, Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
            //Log.w("My Current location address", "Cannot get Address!");
        }

        return strAdd;
    }

}