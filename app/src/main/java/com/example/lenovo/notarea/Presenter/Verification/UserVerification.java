package com.example.lenovo.notarea.Presenter.Verification;

import com.example.lenovo.notarea.Model.User;

public class UserVerification {
    User user = new User();

    public UserVerification() {
    }

    public String verifyUserName(String userName) {
        if(user.checkUserName(userName))
        {
            return "this Name exist plz choose another email";
        }
        return null;
    }

    public String verifyUserMail(String userMail) {
        if(user.checkUserEmail(userMail))
        {
            return "this Email exist plz choose another email";
        }
        return null;
    }

    /**
     * get sure that current user password is correct
     * @return
     */
    public String verifyPassword(String currentPass)
    {
        // use volley to connect to backend to get current password
        // then compare old password with one that comes from backend
        return null;
    }
}
