package com.example.lenovo.notarea.View.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.lenovo.notarea.View.fragments.UserAdsPosts;
import com.example.lenovo.notarea.View.fragments.UserGeneralPosts;

public class UserPostsAdapter extends FragmentPagerAdapter {

    public UserPostsAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);}

    @Override
    public Fragment getItem(int position) {
        if (position == 0)
            return new UserGeneralPosts();
        else
            return new UserAdsPosts();
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0)
            return "General";
        else
            return "ADS";
    }
}
