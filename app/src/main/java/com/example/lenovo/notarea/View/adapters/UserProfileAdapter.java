package com.example.lenovo.notarea.View.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.lenovo.notarea.View.fragments.UserInfoFragment;
import com.example.lenovo.notarea.View.fragments.UserPostsFragment;

public class UserProfileAdapter extends FragmentPagerAdapter {

    public UserProfileAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0)
            return new UserInfoFragment();
        else
            return new UserPostsFragment();
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0)
            return "Info";
        else
            return "My Post";
    }
}
