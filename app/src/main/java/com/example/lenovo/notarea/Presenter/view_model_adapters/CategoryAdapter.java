package com.example.lenovo.notarea.Presenter.view_model_adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.example.lenovo.notarea.Model.Category;
import com.example.lenovo.notarea.R;


import java.util.ArrayList;

public class CategoryAdapter extends ArrayAdapter<Category> {

    public CategoryAdapter(@NonNull Context context, @NonNull ArrayList<Category> objects) {
        super(context, 0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listViewItem = convertView;
        if (listViewItem == null) {
            listViewItem = LayoutInflater.from(getContext()).inflate(R.layout.category_box_item, parent, false);
        }

        final Category category = getItem(position);
        final CheckBox category_check_box = listViewItem.findViewById(R.id.check_box);

        if (category.getName() != null) {
            category_check_box.setText(category.getName());
        }

        if (!category.getId().equals("id")) {
            if (category.isChecked()) {
                category_check_box.setChecked(true);
            } else {
                category_check_box.setChecked(false);
            }
            category_check_box.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (category_check_box.isChecked()) {
                        category.setChecked(true);
                    } else {
                        category.setChecked(false);
                    }
                }
            });
        } else {
            category_check_box.setChecked(true);
            category_check_box.setClickable(false);
        }

        return listViewItem;
    }
}
