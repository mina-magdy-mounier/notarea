package com.example.lenovo.notarea.Model;

import android.content.Context;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Constants;
import com.example.lenovo.notarea.Presenter.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class Post {

    private Constants constants = new Constants();
    final private String TAG = "Post";
    final protected String URL = constants.getURL();

    private String postID;
    private String userFeeling;
    private String postBody;
    private String date;
    private String title;
    private String userID;
    private String attachmentID;
    private int numberOfLikes;
    private int numberOfDislikes;
    private int isLiked;
    private int isDisliked;
    private int isSaved;
    private int userFeelingImg;
    private Attachment attachment;
    private User user;
    private Location location;
    private Context context;
    private int distance;
    private String report;

    public Post() {
    }

    public Post(String postID, int userFeelingImg, String userFeeling, String title, String postBody, String date, int numberOfLikes, int numberOfDislikes, int isLiked,
                int isDisliked, int isSaved, String attachmentID, Attachment attachment, String userID, User user, Location location) {
        this.postID = postID;
        this.userFeelingImg = userFeelingImg;
        this.userFeeling/**/ = userFeeling;
        this.title = title;
        this.postBody = postBody;
        this.date = date;
        this.numberOfLikes = numberOfLikes;
        this.numberOfDislikes = numberOfDislikes;
        this.isLiked = isLiked;
        this.isDisliked = isDisliked;
        this.isSaved = isSaved;
        this.attachmentID = attachmentID;
        this.attachment = attachment;
        this.userID = userID;
        this.user = user;
        this.location = location;
    }


    public Post(String title, String postBody, String userFeeling, int userFeelingImg, int numberOfLikes, int numberOfDislikes, int isLiked,
                int isDisliked, int isSaved, Attachment attachment, String userID, User user, Location location, int distance) {
        this.userFeeling = userFeeling;
        this.postBody = postBody;
        this.title = title;
        this.userID = userID;
        this.numberOfLikes = numberOfLikes;
        this.numberOfDislikes = numberOfDislikes;
        this.isLiked = isLiked;
        this.isDisliked = isDisliked;
        this.isSaved = isSaved;
        this.userFeelingImg = userFeelingImg;
        this.attachment = attachment;
        this.user = user;
        this.location = location;
        this.distance = distance;
    }

// ===========================  Setters & Getters ============================================//

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getPostID() {
        return postID;
    }

    public void setPostID(String postID) {
        this.postID = postID;
    }

    public int getUserFeelingImg() {
        return userFeelingImg;
    }

    public void setUserFeelingImg(int userFeelingImg) {
        this.userFeelingImg = userFeelingImg;
    }

    public String getUserFeeling() {
        return userFeeling;
    }

    public void setUserFeeling(String userFeeling) {
        this.userFeeling = userFeeling;
    }

    public String getPostBody() {
        return postBody;
    }

    public void setPostBody(String postBody) {
        this.postBody = postBody;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getNumberOfLikes() {
        return numberOfLikes;
    }

    public void setNumberOfLikes(int numberOfLikes) {
        this.numberOfLikes = numberOfLikes;
    }

    public int getNumberOfDislikes() {
        return numberOfDislikes;
    }

    public void setNumberOfDislikes(int numberOfDislikes) {
        this.numberOfDislikes = numberOfDislikes;
    }

    public int isLiked() {
        return isLiked;
    }

    public void setIsLiked(int isLiked) {
        this.isLiked = isLiked;
    }

    public int isDisliked() {
        return isDisliked;
    }

    public void setIsDisliked(int isDisliked) {
        this.isDisliked = isDisliked;
    }

    public int isSaved() {
        return isSaved;
    }

    public void setIsSaved(int isSaved) {
        this.isSaved = isSaved;
    }

    public String getAttachmentID() {
        return attachmentID;
    }

    public void setAttachmentID(String attachmentID) {
        this.attachmentID = attachmentID;
    }

    public Attachment getAttachment() {
        return attachment;
    }

    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    JsonParser jsonParser = new JsonParser();
    private ArrayList<Post> posts = new ArrayList<>();

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    // ===========================  CRUD ============================================//

    public void addPost(JSONObject postJson, String user_id, final ServerCallback callback) {
        try {
            String url = URL + "api/notarea/users/" + user_id + "/posts";
            Log.d(TAG, "addPost: "+postJson);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                    (Request.Method.POST, url, postJson, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject responsee) {
                            Log.d(TAG, "addPost respnse: "+responsee.toString());
                            callback.onSuccess(responsee.toString());
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d(TAG, error.toString());
                        }
                    });
            // Access the RequestQueue through your singleton class.
            Singleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
    }

    public void editPost(String userID, String postID, JSONObject postJson, final ServerCallback callback) {
        try {
            String url = URL + "api/notarea/users/" + userID + "/posts/" + postID;
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                    (Request.Method.PUT, url, postJson, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject responsee) {
                            callback.onSuccess(responsee.toString());
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d(TAG, error.toString());
                        }
                    });
            Singleton.getInstance(context).addToRequestQueue(jsonObjectRequest);

        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
    }

    public void getUserPosts(String currentUserID, final ServerCallback callback) {
        String url = URL + "api/notarea/users/" + currentUserID + "/userposts";
        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray responsee) {
                        Log.d(TAG, "onResponse: " + responsee.toString());
                        callback.onSuccess(responsee.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse response = error.networkResponse;
                        if (response != null && response.data != null) {
                            switch (response.statusCode) {
                                case 404:
                                    callback.onSuccess("error");
                                    break;

                                case 500:
                                    callback.onSuccess("php_error");
                                    break;
                            }
                        }
                    }
                });
        Singleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public void getUserSavedPosts(String currentUserID, final ServerCallback callback) {
        String url = URL + "api/notarea/users/" + currentUserID + "/savedposts";
        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray responsee) {
                        Log.d(TAG, "onResponse: " + responsee.toString());
                        callback.onSuccess(responsee.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse response = error.networkResponse;
                        if (response != null && response.data != null) {
                            switch (response.statusCode) {
                                case 404:
                                    callback.onSuccess("error");
                                    break;

                                case 500:
                                    callback.onSuccess("php_error");
                                    break;
                            }
                        }
                    }
                });
        Singleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public void getPosts(JSONObject root, final ServerCallback callback) {
        String url = URL + "api/notarea/users/nearestposts";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, root, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject responsee) {
                        Log.d(TAG, "onResponse: " + responsee);
                        callback.onSuccess(responsee.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse response = error.networkResponse;
                        if (response != null && response.data != null) {
                            switch (response.statusCode) {
                                case 404:
                                    callback.onSuccess("error");
                                    break;

                                case 500:
                                    callback.onSuccess("php_error");
                                    break;
                            }
                        }
                    }
                });
        Singleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public void deletePost(String currentUserID, String postID, final ServerCallback callback) {
        try {
            String url = URL + "api/notarea/users/" + currentUserID + "/posts/" + postID;
            StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    callback.onSuccess(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, error.toString());
                }
            });
            // Access the RequestQueue through your singleton class.
            Singleton.getInstance(context).addToRequestQueue(stringRequest);
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
    }

    public void likeStateChange(final String userID, final String postID, final ServerCallback callback) {
        try {
            String url = URL + "api/notarea/users/" + userID + "/posts/" + postID + "/like";

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d(TAG, "onResponse: " + response);
                    callback.onSuccess(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "onErrorResponse: " + error);
                }
            });
            // Access the RequestQueue through your singleton class.
            Singleton.getInstance(context).addToRequestQueue(stringRequest);

        } catch (Exception e) {
            Log.d(TAG, e.toString());

        }
    }

    public void disLikeStateChange(final String userID, final String postID, final ServerCallback callback) {
        try {
            String url = URL + "api/notarea/users/" + userID + "/posts/" + postID + "/dislike";
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    callback.onSuccess(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "onErrorResponse: " + error);
                }
            });
            Singleton.getInstance(context).addToRequestQueue(stringRequest);

        } catch (Exception e) {
            Log.d(TAG, e.toString());

        }
    }

    public void savePostStateChange(final String userID, final String postID, final ServerCallback callback) {
        try {
            String url = URL + "api/notarea/users/" + userID + "/posts/" + postID + "/save";
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    callback.onSuccess(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "onErrorResponse: " + error);
                }
            });
            Singleton.getInstance(context).addToRequestQueue(stringRequest);

        } catch (Exception e) {
            Log.d(TAG, e.toString());

        }
    }

    public void report(JSONObject report, final ServerCallback callback) {
        try {
            String url = URL + "api/notarea/users/posts/report";
            Log.d(TAG, "report: " + report);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                    (Request.Method.POST, url, report, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject responsee) {
                            callback.onSuccess(responsee.toString());
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            NetworkResponse response = error.networkResponse;
                            if (response != null && response.data != null) {
                                switch (response.statusCode) {
                                    case 400:
                                        callback.onSuccess("error");
                                        break;
                                    case 422:
                                        callback.onSuccess("error");
                                        break;
                                    case 404:
                                        callback.onSuccess("error");
                                        break;
                                    case 505:
                                        callback.onSuccess("error");
                                        break;
                                }
                            }
                        }
                    });

            Singleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
    }

}
