package com.example.lenovo.notarea.HttpRequests;


import android.os.AsyncTask;
import android.util.Log;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class POST  extends AsyncTask<String , Void , String> {

    private final String TAG = "POST";

    @Override
    protected String doInBackground(String... strings) {
        String JSPN_Request = "";
        String response = "";
        URL url = null;
        String jsonObject = null;
        HttpURLConnection conn = null;
        OutputStream outputStream = null;
        OutputStreamWriter outputStreamWriter = null;
        BufferedWriter bufferedWriter = null;
        try
        {
            url = new URL(strings[0]);
            jsonObject = strings[1];
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-z8");
            OutputStream os = conn.getOutputStream();
            outputStreamWriter = new OutputStreamWriter(os);
            bufferedWriter = new BufferedWriter(outputStreamWriter);
            bufferedWriter.write(jsonObject);
            bufferedWriter.flush();
            bufferedWriter.close();

            if(conn.getResponseCode() == 200 || conn.getResponseCode() == 201) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    response += line;
                }
                reader.close();
            }else
            {
                return null;
            }
        }catch (Exception e)
        {
            Log.d(TAG, "----------------------URL Conn Error in POST.java----------------------------");
            Log.d(TAG, "POSTING ERROR" + e);
            Log.d(TAG, "----------------------URL Conn Error in POST.java-----------------------------");
            return null;
        }finally {
            if (outputStream != null) try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            return response;
        }

    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }
}
