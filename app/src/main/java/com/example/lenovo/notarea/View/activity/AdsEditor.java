package com.example.lenovo.notarea.View.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.notarea.CallBacks.FunctionCallback;
import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Constants;
import com.example.lenovo.notarea.FireBase.FirebaseStorageService;
import com.example.lenovo.notarea.Presenter.AdsPresenter;
import com.example.lenovo.notarea.Presenter.SessionHandler;
import com.example.lenovo.notarea.Presenter.UserPreseneter;
import com.example.lenovo.notarea.Presenter.view_model_adapters.BusinessListAdapter;
import com.example.lenovo.notarea.R;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;

public class AdsEditor extends AppCompatActivity {

    private Constants constants = new Constants();

    private int MAX_DISTANCE = constants.getADS_MAX_DISTANCE();
    private int MIN_DISTANCE = constants.getADS_MIN_DISTANCE();
    private int DISTANCE_STEP = constants.getADS_DISTANCE_STEP();

    // Days of ADS
    private int MAX_DAYS = constants.getADS_MAX_DAYS();
    private int MIN_DAYS = constants.getADS_MIN_DAYS();
    private int DAYS_STEP = constants.getADS_DAYS_STEP();

    private boolean hasImg = false;
    private Uri ADS_IMG = null;

    FirebaseStorageService firebaseStorageService = new FirebaseStorageService();
    EditText postTitle, postBody, faceBook;
    Menu menu;
    TextView happy, sad, intrested, shocked;
    TextView faceBookAttachment, locationAttachment, phoneAttachment, distance_wheel_title, days_wheel_title;
    FloatingActionMenu fabMenu;
    FloatingActionButton image_picker, facebook, location, phone, cancelImg, distance, daysOFAdsBtn, categoryBtn;
    NumberPicker distance_wheel, days_wheel;
    ImageView adsImg, arrow;
    Button closeAttachment, addFaceBookBtn, distance_wheel_ok, distance_wheel_cancel, days_wheel_ok, days_wheel_cancel, cancelFaceBookBtn;
    LinearLayout userFeelingSection, contact_my_layout;
    public ProgressBar adding_in_progress;
    int feelingEmoji = 0;
    String feelingDesc = "", FaceBook = null, shared_distance = String.valueOf(MIN_DISTANCE), temp_distance = null, temp_days = null, days_of_ads = String.valueOf(MIN_DAYS);
    static Bitmap AdsImgBitMap = null;
    boolean isExpanded = true,
            addLocation = false;
    Dialog phoneListDialog, addFacebookUrlDialog, distanceDialog, daysOfAdsDialog;
    ListView phoneList;
    ArrayList<String> attachedBusinessNumbersIDs = new ArrayList<>();
    UserPreseneter userPreseneter;
    FormHandler formHandler;
    AdsPresenter adsPresenter;
    SharedPreferences session;
    BusinessListAdapter businessListAdapter;
    Bundle bundle;
    SessionHandler sessionHandler = new SessionHandler();


    // title-0 , body-1  ,face-2 , numbers-3 , location-4 , feelings-5 , image-6 , distance-7
    Boolean editor[] = {false, false, false, false, false, false, false, false};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        if (sessionHandler.get_theme(session) != R.style.AppTheme) {
            setTheme(sessionHandler.get_theme(session));
        }
        setContentView(R.layout.ads_post_form);

        firebaseStorageService.setActivity(AdsEditor.this);
//        session = getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        bundle = getIntent().getExtras();
        userPreseneter = new UserPreseneter(session);
        formHandler = new FormHandler();
        adsPresenter = new AdsPresenter(session);
        phoneListDialog = new Dialog(this, R.style.CustomDialogTheme);
        phoneListDialog.setContentView(R.layout.phone_attachment);
        phoneList = phoneListDialog.findViewById(R.id.phoneAttachment);
        contact_my_layout = findViewById(R.id.contact_my_layout);
        closeAttachment = phoneListDialog.findViewById(R.id.close_attachment);
        addFacebookUrlDialog = new Dialog(this, R.style.CustomDialogTheme);
        addFacebookUrlDialog.setContentView(R.layout.add_facebook_url);
        cancelFaceBookBtn = addFacebookUrlDialog.findViewById(R.id.cancelFaceBookBtn);
        categoryBtn = findViewById(R.id.category_btn);
        categoryBtn.setVisibility(View.GONE);
        faceBook = addFacebookUrlDialog.findViewById(R.id.facebookUrl);
        addFaceBookBtn = addFacebookUrlDialog.findViewById(R.id.addFaceBookBtn);
        postBody = findViewById(R.id.post_body_input);
        happy = findViewById(R.id.happy);
        sad = findViewById(R.id.sad);
        intrested = findViewById(R.id.intrested);
        shocked = findViewById(R.id.shocked);
        adsImg = findViewById(R.id.adsImg);
        userFeelingSection = findViewById(R.id.user_feeling_section);
        arrow = findViewById(R.id.userFeelingToggle);
        image_picker = findViewById(R.id.image_picker);
        facebook = findViewById(R.id.facebook);
        location = findViewById(R.id.location);
        phone = findViewById(R.id.phoneNumber);
        faceBookAttachment = findViewById(R.id.add_facebook);
        locationAttachment = findViewById(R.id.add_location);
        phoneAttachment = findViewById(R.id.add_phone);
        postTitle = findViewById(R.id.post_title);
        fabMenu = findViewById(R.id.fob_menu);
        adding_in_progress = findViewById(R.id.adding_post_progress_bar);
        adding_in_progress.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.loading), PorterDuff.Mode.MULTIPLY);

        adsPresenter.initEditor(bundle);
        // Distance Wheel
        distanceDialog = new Dialog(this, R.style.CustomDialogTheme);
        distanceDialog.setContentView(R.layout.number_wheel);
        distance = findViewById(R.id.distance_picker);
        distance_wheel = distanceDialog.findViewById(R.id.number_picker);
        distance_wheel_ok = distanceDialog.findViewById(R.id.ok);
        distance_wheel_cancel = distanceDialog.findViewById(R.id.cancel);
        distance_wheel_title = distanceDialog.findViewById(R.id.number_wheel_title);
        distance_wheel_title.setText("Choose distance in Meter");
        formHandler.cancel_dialog(distance_wheel_cancel, distanceDialog);
        formHandler.set_wheel(MAX_DISTANCE, MIN_DISTANCE, DISTANCE_STEP, distance_wheel);
        formHandler.show_dialog(distance, distanceDialog);
        formHandler.cancel_dialog(cancelFaceBookBtn, addFacebookUrlDialog);
        get_distance();
        distance_listener();

        // Days of ADS
        daysOfAdsDialog = new Dialog(this, R.style.CustomDialogTheme);
        daysOfAdsDialog.setContentView(R.layout.number_wheel);
        daysOFAdsBtn = findViewById(R.id.days_of_ads);
        days_wheel = daysOfAdsDialog.findViewById(R.id.number_picker);
        days_wheel_ok = daysOfAdsDialog.findViewById(R.id.ok);
        days_wheel_cancel = daysOfAdsDialog.findViewById(R.id.cancel);
        days_wheel_title = daysOfAdsDialog.findViewById(R.id.number_wheel_title);
        days_wheel_title.setText("Choose days that ads remains by days");
        formHandler.show_dialog(daysOFAdsBtn, daysOfAdsDialog);
        formHandler.cancel_dialog(days_wheel_cancel, daysOfAdsDialog);
        formHandler.set_wheel(MAX_DAYS, MIN_DAYS, DAYS_STEP, days_wheel);
        days_wheel_listener();
        get_days_of_ads();
        daysOFAdsBtn.setVisibility(View.GONE);

        initEditor();
        if (adsPresenter.getAdvertisement().getAttachment() != null) {
            if (adsPresenter.getAdvertisement().getAttachment().getPhoneNumber() != null) {
                for (int i = 0; i < adsPresenter.getAdvertisement().getAttachment().getPhoneNumber().size(); i++) {
                    String id = adsPresenter.getAdvertisement().getAttachment().getPhoneNumber().get(i).getId();
                    String name = adsPresenter.getAdvertisement().getAttachment().getPhoneNumber().get(i).getName();
                    String number = adsPresenter.getAdvertisement().getAttachment().getPhoneNumber().get(i).getNumber();
                    attachedBusinessNumbersIDs.add(id);
                    formHandler.addPhoneView(id, name, number, attachedBusinessNumbersIDs, contact_my_layout, getApplicationContext(), editor);
                }
            }
        }
        if (userPreseneter.getUser().getBusinessNumbers() != null || userPreseneter.getUser().getBusinessNumbers().size() != 0) {
            businessListAdapter = new BusinessListAdapter(getApplicationContext(), userPreseneter.getUser().getBusinessNumbers());
            phoneList.setAdapter(businessListAdapter);
        }

        initFeelingListener();
        PickImage();
        FormHandler.collapse(userFeelingSection);
        formHandler.showUserFeeling(arrow, isExpanded, userFeelingSection);
        formHandler.addLocation(location, locationAttachment, postBody, editor);
        formHandler.addPhone(phoneList, phoneListDialog, phone, attachedBusinessNumbersIDs, contact_my_layout, this, editor);
        formHandler.cancelLocationAttachment(locationAttachment, editor, postBody);
        formHandler.closePhoneAttachment(closeAttachment, phoneListDialog);
        formHandler.onEditingPost(postBody, editor);
        formHandler.onEditingTitle(postTitle, postBody, editor);
        formHandler.addFaceBook(addFacebookUrlDialog, faceBook, facebook, addFaceBookBtn, faceBookAttachment, new FunctionCallback() {
            @Override
            public void onSuccess(String result) {
                if (result != null) {
                    FaceBook = result;
                    canEdit();
                }
            }

            @Override
            public void onError(String result) {
                if (result != null) {
                    FaceBook = null;
                    Toast.makeText(AdsEditor.this, result, Toast.LENGTH_LONG).show();
                }
            }
        });
        formHandler.cancelFaceBookAttachment(faceBookAttachment, new FunctionCallback() {
            @Override
            public void onSuccess(String result) {
                if (result == null) {
                    FaceBook = null;
                    faceBook.setText("");
                    canEdit();
                }
            }

            @Override
            public void onError(String result) {
                if (result.equals("error"))
                    Toast.makeText(AdsEditor.this, "somthing wrong happened", Toast.LENGTH_LONG).show();
            }
        });
        feelingListener();
        fabMenu.setClosedOnTouchOutside(true);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    public void canEdit() {
        if (!postBody.getText().toString().equals("")) {
            formHandler.enableOption(R.id.action_name);
        }
    }

    //distance added mina_magdy 19-6-2018
    private void get_distance() {
        distance_wheel_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shared_distance = temp_distance;
                distanceDialog.dismiss();
                canEdit();
            }
        });
    }

    // Days of ADS
    private void get_days_of_ads() {
        days_wheel_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                days_of_ads = temp_days;
                daysOfAdsDialog.dismiss();
            }
        });
    }

    //distance added mina_magdy 19-6-2018
    private void distance_listener() {
        distance_wheel.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int old_number, int new_number) {
                temp_distance = numberPicker.getDisplayedValues()[new_number];
            }
        });
    }

    // Days of ADS
    private void days_wheel_listener() {
        days_wheel.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int old_number, int new_number) {
                temp_days = numberPicker.getDisplayedValues()[new_number];
            }
        });
    }

    private void initEditor() {
        if (adsPresenter.getAdvertisement().getTitle() != null) {
            postTitle.setText(adsPresenter.getAdvertisement().getTitle());
        }
        if (adsPresenter.getAdvertisement().getDistance() > 0) {
            int tmp = adsPresenter.getAdvertisement().getDistance() / 1000;
            int indx = tmp / DISTANCE_STEP;
            shared_distance = Integer.toString(tmp);
            distance_wheel.setValue(indx - 1);
        }
        if (adsPresenter.getAdvertisement().getPostBody() != null) {
            postBody.setText(adsPresenter.getAdvertisement().getPostBody());
        }
        if (adsPresenter.getAdvertisement().getAds_Img() != null) {
            if (!adsPresenter.getAdvertisement().getAds_Img().equals("null")) {
                Picasso.get().load(adsPresenter.getAdvertisement().getAds_Img()).into(adsImg);
                adsImg.setVisibility(View.VISIBLE);
                hasImg = true;
                image_picker.setImageResource(R.drawable.ic_close);
            }
        }
        if (adsPresenter.getAdvertisement().getAttachment() != null) {
            if (adsPresenter.getAdvertisement().getAttachment().getFacebook_url() != null && !adsPresenter.getAdvertisement().getAttachment().getFacebook_url().equals("")) {
                faceBook.setText(adsPresenter.getAdvertisement().getAttachment().getFacebook_url());
                faceBookAttachment.setVisibility(View.VISIBLE);
            }

            if (adsPresenter.getAdvertisement().getAttachment().getLocation() != null && !adsPresenter.getAdvertisement().getAttachment().getLocation().equals("")) {
                locationAttachment.setVisibility(View.VISIBLE);
            }
        }
    }

    public void PickImage() {
        image_picker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!hasImg) {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("image/jpeg");
                    intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                    startActivityForResult(intent, 101);
                    editor[6] = true;
                    canEdit();
                } else {
                    AdsImgBitMap = null;
                    adsImg.setImageBitmap(null);
                    hasImg = false;
                    adsImg.setVisibility(View.GONE);
                    image_picker.setImageResource(R.drawable.ic_insert_photo_white_20dp);
                    editor[6] = true;
                    ADS_IMG = null;
                    canEdit();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == 101) {
                Uri imgUri = data.getData();
                try {
                    ADS_IMG = imgUri;
                    AdsImgBitMap = MediaStore.Images.Media.getBitmap(getContentResolver(), imgUri);
                    adsImg.setImageBitmap(AdsImgBitMap);
                    adsImg.setVisibility(View.VISIBLE);
                    image_picker.setImageResource(R.drawable.ic_close);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_post, menu);
        this.menu = menu;
        formHandler.setMenu(menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_name) {
            if (String.valueOf(locationAttachment.getVisibility()).equals(String.valueOf(View.VISIBLE))) {
                addLocation = true;
            }
        }

        if (id == R.id.action_name) {
            if (String.valueOf(locationAttachment.getVisibility()).equals(String.valueOf(View.VISIBLE)) && editor[4]) {
                addLocation = true;
            } else {
                addLocation = false;
            }
            if (ADS_IMG != null) {
                adding_in_progress.setVisibility(View.VISIBLE);
                firebaseStorageService.get_photo_local_uri(ADS_IMG, new ServerCallback() {
                    @Override
                    public void onSuccess(String result) {

                        adsPresenter.editAds(AdsEditor.this, adsPresenter.getAdvertisement().getAdsID(), postTitle.getText().toString(), postBody.getText().toString().trim(), feelingDesc, feelingEmoji, FaceBook, attachedBusinessNumbersIDs, addLocation, result, shared_distance, editor);
                    }
                });
            } else {
                adding_in_progress.setVisibility(View.VISIBLE);
                adsPresenter.editAds(AdsEditor.this, adsPresenter.getAdvertisement().getAdsID(), postTitle.getText().toString(), postBody.getText().toString().trim(), feelingDesc, feelingEmoji, FaceBook, attachedBusinessNumbersIDs, addLocation, null, shared_distance, editor);
            }

        }
        return super.onOptionsItemSelected(item);
    }

    public void feelingListener() {
        happy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!feelingDesc.equals("Feeling Happy")) {
                    feelingEmoji = R.drawable.ic_emoji;
                    feelingDesc = "Feeling Happy";
                    formHandler.setBorder(happy, sad, intrested, shocked);
                } else {
                    feelingEmoji = 0;
                    feelingDesc = "";
                    formHandler.removeBorder(happy);
                }
            }
        });

        sad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!feelingDesc.equals("Feeling Sad")) {
                    feelingEmoji = R.drawable.ic_sad_emoji;
                    feelingDesc = "Feeling Sad";
                    formHandler.setBorder(sad, happy, intrested, shocked);
                } else {
                    feelingEmoji = 0;
                    feelingDesc = "";
                    formHandler.removeBorder(sad);
                }
            }
        });

        intrested.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!feelingDesc.equals("Feeling Intrested")) {
                    feelingEmoji = R.drawable.ic_intrested_emoji;
                    feelingDesc = "Feeling Intrested";
                    formHandler.setBorder(intrested, happy, sad, shocked);
                } else {
                    feelingEmoji = 0;
                    feelingDesc = "";
                    formHandler.removeBorder(intrested);
                }
            }
        });

        shocked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!feelingDesc.equals("Feeling Shocked")) {
                    feelingEmoji = R.drawable.ic_shocked_emoji;
                    feelingDesc = "Feeling Shocked";
                    formHandler.setBorder(shocked, happy, sad, intrested);
                } else {
                    feelingEmoji = 0;
                    feelingDesc = "";
                    formHandler.removeBorder(shocked);
                }
            }
        });
    }

    public void initFeelingListener() {
        if (adsPresenter.getAdvertisement().getUserFeelingImg() == R.drawable.ic_emoji) {
            feelingEmoji = R.drawable.ic_emoji;
            feelingDesc = "Feeling Happy";
            formHandler.setBorder(happy, sad, intrested, shocked);
            happy.getBackground();
        } else if (adsPresenter.getAdvertisement().getUserFeelingImg() == R.drawable.ic_sad_emoji) {
            feelingEmoji = R.drawable.ic_sad_emoji;
            feelingDesc = "Feeling Sad";
            formHandler.setBorder(sad, happy, intrested, shocked);
        } else if (adsPresenter.getAdvertisement().getUserFeelingImg() == R.drawable.ic_intrested_emoji) {
            feelingEmoji = R.drawable.ic_intrested_emoji;
            feelingDesc = "Feeling Intrested";
            formHandler.setBorder(intrested, happy, sad, shocked);
        } else if (adsPresenter.getAdvertisement().getUserFeelingImg() == R.drawable.ic_shocked_emoji) {
            feelingEmoji = R.drawable.ic_shocked_emoji;
            feelingDesc = "Feeling Shocked";
            formHandler.setBorder(shocked, happy, sad, intrested);
        }
    }

}
