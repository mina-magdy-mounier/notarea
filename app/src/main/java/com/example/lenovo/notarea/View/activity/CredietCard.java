package com.example.lenovo.notarea.View.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.braintreepayments.cardform.view.CardForm;
import com.example.lenovo.notarea.R;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Token;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Charge;

import java.util.HashMap;
import java.util.Map;


import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.braintreepayments.cardform.view.CardForm;
import com.parse.FunctionCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Customer;
import com.stripe.android.model.Token;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Charge;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

public class CredietCard extends AppCompatActivity {

    String TAG = "CredietCard";
    CardForm cardForm;
    com.stripe.android.model.Card myCard;
    Charge charge;
    ProgressDialog progress;
    Token token;
    public static final String PUBLISHABLE_KEY = "pk_test_iI8cRWVlCcBOSUUBUntpKfSK";
    public static final String APPLICATION_ID = "RKNck9SdN6sqcznBvy5lqnN2ln1FrrSabNcq8YEK";
    public static final String CLIENT_KEY = "ca_D5jAeIrwVPw4zl5ntUMM4OvNN7BymckE";
    public static final String BACK4PAPP_API = "https://parseapi.back4app.com/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crediet_card);



        cardForm = (CardForm) findViewById(R.id.card_form);
        progress = new ProgressDialog(this);

        Parse.initialize(new Parse.Configuration.Builder(getApplicationContext())
                //.applicationId(APPLICATION_ID)
                .clientKey(CLIENT_KEY)
                .server(BACK4PAPP_API).build());

        setUp(this);

        if(!cardForm.isValid())
        {
            //Toast.makeText(CredietCard.this, "Card is being Validated", Toast.LENGTH_SHORT).show();
            cardForm.validate();
        }
        if(cardForm.isCardScanningAvailable())
        {
            //Toast.makeText(CredietCard.this, "Card is being Scanned", Toast.LENGTH_SHORT).show();
            cardForm.scanCard(this);
        }

        Button goPremiumBtn = (Button) findViewById(R.id.go_premium);
        goPremiumBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                //Toast.makeText(CredietCard.this, cardForm.getExpirationMonth() + "/" + cardForm.getExpirationYear() + "         " + cardForm.getCvv(), Toast.LENGTH_SHORT).show();

                myCard = new com.stripe.android.model.Card(cardForm.getCardNumber(),Integer.parseInt(cardForm.getExpirationMonth()),Integer.parseInt(cardForm.getExpirationYear()),cardForm.getCvv());
                if(myCard!=null)
                {
                    buy();
                    //Toast.makeText(CredietCard.this, "/*/*/*/*/*", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void setUp(android.app.Activity activity) {
        cardForm.cardRequired(true)
                .expirationRequired(true)
                .cvvRequired(true)
                .postalCodeRequired(true)
                .mobileNumberRequired(true)
                .mobileNumberExplanation("SMS is required on this number")
                .actionLabel("Purchase")
                .setup(activity);
    }

    private void buy(){
        final com.stripe.android.model.Card card =  myCard;
        boolean validation = card.validateCard();
        if(validation)
        {
            startProgress("Validating Credit Card");
            //stripe = new Stripe(this);
            Stripe stripe =  new Stripe(getApplicationContext(),PUBLISHABLE_KEY);
            stripe.createToken(
                    card,
                    new TokenCallback() {
                        public void onSuccess(Token token) {
                            // Send token to your server
                            finishProgress();
                           // Toast.makeText(getApplicationContext(), token.toString(), Toast.LENGTH_SHORT).show();
                            charge(token);
                        }
                        public void onError(Exception error) {
                            // Show localized error message
                            //Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
            );
//             try {
//                 token = stripe.createTokenSynchronous(card, PUBLISHABLE_KEY);
//                 if(token != null) {
//                     finishProgress();
//                     Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
//                     charge(token);
//                 }
//             } catch (com.stripe.android.exception.AuthenticationException e) {
//                 e.printStackTrace();
//             } catch (com.stripe.android.exception.InvalidRequestException e) {
//                 e.printStackTrace();
//             } catch (com.stripe.android.exception.APIConnectionException e) {
//                 e.printStackTrace();
//             } catch (com.stripe.android.exception.CardException e) {
//                 e.printStackTrace();
//             } catch (com.stripe.android.exception.APIException e) {
//                 e.printStackTrace();
//             }


//             stripe.createToken(card, new Executor() {
//                 @Override
//                 public void execute(@NonNull Runnable command) {
//                     Toast.makeText(getApplicationContext(), "OnExecute "+command, Toast.LENGTH_LONG).show();
//                     Log.d(TAG,"OnExecute");
//                 }
//             }, new TokenCallback() {
//                 public void onSuccess(Token token) {
//                     finishProgress();
//                     Toast.makeText(getApplicationContext(), token+"   --> OnSuccess", Toast.LENGTH_LONG).show();
//                     Log.d(TAG,"OnSuccess");
//                     charge(token);
//                     // Send token to your server
//                 }
//                 public void onError(Exception error) {
//                     // Show localized error message
//                     Log.d(TAG,"OnError");
//                     Toast.makeText(getApplicationContext(), error+"    --> OnError", Toast.LENGTH_LONG).show();
//                 }
//             });
//             stripe.createToken(card, new TokenCallback() {
//                 public void onSuccess(Token token) {
//                     Toast.makeText(getApplicationContext(), token+"   --> OnSuccess", Toast.LENGTH_LONG).show();
//                     Log.d(TAG,"OnSuccess");
//                     // Send token to your server
//                 }
//                 public void onError(Exception error) {
//                     // Show localized error message
//                     Log.d(TAG,"OnError");
//                     Toast.makeText(getApplicationContext(), error+"    --> OnError", Toast.LENGTH_LONG).show();
//                 }
//             });
        }
        else if (!card.validateNumber()) {
            Log.d("Stripe","The card number that you entered is invalid");
            Toast.makeText(CredietCard.this, "The card number that you entered is invalid" , Toast.LENGTH_LONG).show();
        } else if (!card.validateExpiryDate()) {
            Log.d("Stripe","The expiration date that you entered is invalid");
            Toast.makeText(CredietCard.this, "The expiration date that you entered is invalid" , Toast.LENGTH_LONG).show();
        } else if (!card.validateCVC()) {
            Log.d("Stripe","The CVC code that you entered is invalid");
            Toast.makeText(CredietCard.this, "The CVC code that you entered is invalid" , Toast.LENGTH_LONG).show();
        } else {
            Log.d("Stripe","The card details that you entered are invalid");
            Toast.makeText(CredietCard.this, "The card details that you entered are invalid" , Toast.LENGTH_LONG).show();
        }
        if(token != null) {
            charge(token);
        }
    }

    private void charge(Token cardToken){
        Map<String, Object> chargeParams = new HashMap<String, Object>();
        chargeParams.put("itemName", "test"); //This part you need to pass your item detail, implement yourself
        chargeParams.put("cardToken", cardToken.getId());
        chargeParams.put("name","Dominic Wong");
        chargeParams.put("email","dominwong4@gmail.com");
        chargeParams.put("address",myCard.getAddressCity() + " " + myCard.getAddressCountry());
        chargeParams.put("zip",myCard.getAddressZip());
        chargeParams.put("city_state",myCard.getAddressState());
        chargeParams.put("amount", 1);
        chargeParams.put("currency", "LE");

        try {
            charge = Charge.create(chargeParams);
            //charge.capture();
        } catch (AuthenticationException e) {

        } catch (InvalidRequestException e) {

        } catch (APIConnectionException e) {

        } catch (CardException e) {

        } catch (APIException e) {
        }

        startProgress("Purchasing Item");

        ParseCloud.callFunctionInBackground("purchaseItem", chargeParams, new FunctionCallback<Object>() {
            public void done(Object response, ParseException e) {
                //finishProgress();
                if (e == null) {
                    Log.d("Cloud Response", "There were no exceptions! " + response.toString());
                    Toast.makeText(getApplicationContext(), "Item Purchased Successfully ", Toast.LENGTH_LONG).show();
                } else {
                    Log.d("Cloud Response", "Exception: " + e);
                    Toast.makeText(getApplicationContext(), e.getMessage().toString(), Toast.LENGTH_LONG).show();
                }
                //for now we will make him premium even if error happened
                finishProgress();
                Toast.makeText(getApplicationContext(), "Your account upgraded to premium ! ", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(CredietCard.this, MainActivity.class);
                startActivity(intent);
                //pop make him premium from backend
            }
        });
    }

    private void startProgress(String title){
        progress.setTitle(title);
        progress.setMessage("Please Wait");
        progress.show();
    }

    private void finishProgress(){
        progress.dismiss();
    }

}

class Item {
    String name;
    int price = 1;
    int quantityAvailable = 10;

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setQuantityAvailable(int quantityAvailable) {
        this.quantityAvailable = quantityAvailable;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantityAvailable() {
        return quantityAvailable;
    }

}

class Order {
    String name;
    String email;
    String address;
    String zip;
    String city_state;
    Item item;
    int size;
    boolean fulfilled;
    boolean charged;
    int stripePaymentId ;


    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public void setCity_state(String city_state) {
        this.city_state = city_state;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setFulfilled(boolean fulfilled) {
        this.fulfilled = fulfilled;
    }

    public void setCharged(boolean charged) {
        this.charged = charged;
    }

    public void setStripePaymentId(int stripePaymentId) {
        this.stripePaymentId = stripePaymentId;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getZip() {
        return zip;
    }

    public String getCity_state() {
        return city_state;
    }

    public Item getItem() {
        return item;
    }

    public int getSize() {
        return size;
    }

    public boolean isFulfilled() {
        return fulfilled;
    }

    public boolean isCharged() {
        return charged;
    }

    public int getStripePaymentId() {
        return stripePaymentId;
    }


}
