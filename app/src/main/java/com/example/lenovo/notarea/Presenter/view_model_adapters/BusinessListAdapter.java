package com.example.lenovo.notarea.Presenter.view_model_adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lenovo.notarea.Model.ExtraNumbers;
import com.example.lenovo.notarea.R;

import java.util.ArrayList;

public class BusinessListAdapter extends ArrayAdapter<ExtraNumbers> {

    private String whereIam;

    public String getWhereIam() {
        return whereIam;
    }

    public void setWhereIam(String whereIam) {
        this.whereIam = whereIam;
    }


    public BusinessListAdapter(@NonNull Context context, @NonNull ArrayList<ExtraNumbers> objects) {
        super(context, 0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listViewItem = convertView;
        if (listViewItem == null) {
            listViewItem = LayoutInflater.from(getContext()).inflate(R.layout.extra_numbers_body, parent, false);
        }

        ExtraNumbers extraNumber = getItem(position);
        TextView id = listViewItem.findViewById(R.id.id);
        TextView name = listViewItem.findViewById(R.id.name);
        TextView number = listViewItem.findViewById(R.id.number);
        ImageView close = listViewItem.findViewById(R.id.closeBtn);
        ImageView call = listViewItem.findViewById(R.id.callBtn);
        if (whereIam != null) {
            if (whereIam.equals("call")) {
                call.setVisibility(View.VISIBLE);
            } else {
                call.setVisibility(View.GONE);
            }

            if (whereIam.equals("close")) {
                close.setVisibility(View.VISIBLE);
            } else {
                close.setVisibility(View.GONE);
            }
        }

        id.setText(extraNumber.getId());
        name.setText(extraNumber.getName());
        number.setText(extraNumber.getNumber());

        return listViewItem;
    }
}
