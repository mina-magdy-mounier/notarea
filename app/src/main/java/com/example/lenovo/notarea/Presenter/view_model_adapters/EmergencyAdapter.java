package com.example.lenovo.notarea.Presenter.view_model_adapters;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lenovo.notarea.Model.Emergency;
import com.example.lenovo.notarea.Model.Location;
import com.example.lenovo.notarea.Model.Post;
import com.example.lenovo.notarea.Presenter.Maps.MapsActivity;
import com.example.lenovo.notarea.R;
import com.example.lenovo.notarea.View.fragments.EmergencyPageFragment;
import com.example.lenovo.notarea.View.fragments.GeneralPageFragemnt;
import com.example.lenovo.notarea.View.fragments.UserInfoFragment;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.content.Context.LOCATION_SERVICE;

public class EmergencyAdapter extends ArrayAdapter<Emergency> {

    ArrayList<Emergency> emerginciesBodies;
    EmergencyPageFragment emergencyPageFragment;
    String currentUserID;
    AdapterHandler adapterHandler = new AdapterHandler();


    public EmergencyAdapter(Context context, ArrayList<Emergency> emerginciesBodies, String currentUserID) {
        super(context, 0, emerginciesBodies);
        this.currentUserID = currentUserID;
        this.emerginciesBodies = emerginciesBodies;
    }

    public ArrayList<Emergency> getEmerginciesBodies() {
        return emerginciesBodies;
    }

    public void setEmerginciesBodies(ArrayList<Emergency> emerginciesBodies) {
        this.emerginciesBodies = emerginciesBodies;
    }

    public EmergencyPageFragment getEmergencyPageFragment() {
        return emergencyPageFragment;
    }

    public void setEmergencyPageFragment(EmergencyPageFragment emergencyPageFragment) {
        this.emergencyPageFragment = emergencyPageFragment;
    }

    public String getCurrentUserID() {
        return currentUserID;
    }

    public void setCurrentUserID(String currentUserID) {
        this.currentUserID = currentUserID;
    }

    public AdapterHandler getAdapterHandler() {
        return adapterHandler;
    }

    public void setAdapterHandler(AdapterHandler adapterHandler) {
        this.adapterHandler = adapterHandler;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listViewItem = convertView;
        if (listViewItem == null) {
            listViewItem = LayoutInflater.from(getContext()).inflate(R.layout.emergency_post_body, parent, false);
        }

        Emergency emergency = getItem(position);
        TextView userName = listViewItem.findViewById(R.id.userName);
        TextView content = listViewItem.findViewById(R.id.content);
        TextView rate = listViewItem.findViewById(R.id.rate);
        TextView distance = listViewItem.findViewById(R.id.distance);
        TextView status = listViewItem.findViewById(R.id.status);
        LinearLayout emergencyPostParent = listViewItem.findViewById(R.id.emergencyPostParent);


        if (emergency.isActive()) {
            status.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_access_time_black_24dp, 0);
            emergencyPostParent.setBackgroundResource(R.drawable.emergency);
            userName.setText(emergency.getUser().getName());
            content.setText(emergency.getPostBody().toString());
            Location location = new Location();
            if (getContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLng", "").equals("")) {
                android.location.Location lastKnownLocation= getLastKnownLocation(getContext());
                location=new Location(lastKnownLocation.getLongitude() , lastKnownLocation.getLatitude());
            }
            else{
                location = new Location(Double.parseDouble(getContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLng", "")), Double.parseDouble(getContext().getApplicationContext().getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLat", "")));
            }
            float[] results = new float[10];
            android.location.Location.distanceBetween(location.getLongitude(),location.getLatitude(),emergency.getLocation().getLongitude(),emergency.getLocation().getLatitude(),results);
            DecimalFormat df2 = new DecimalFormat(".##");
            distance.setText(String.valueOf(df2.format(results[0])) + "M");
            Log.d("EmergencyAdapter",results[0]/1000  + "*/*");
            //distance.setText(String.valueOf(emergency.getDistance()));
            if((int)emergency.getRate() == -1.0)
            {
                emergency.setRate(0);
            }
            rate.setText(String.valueOf((int)emergency.getRate())+"/10");
        }
        else
        {
            emergencyPostParent.setBackgroundResource(R.drawable.emergency_done);
            status.setCompoundDrawablesWithIntrinsicBounds(0 , 0 , R.drawable.ic_done_green_24dp , 0);
            userName.setText(emergency.getUser().getName());
            content.setText(emergency.getPostBody().toString());
            distance.setText("----");
        }
        return listViewItem;
    }
    private android.location.Location getLastKnownLocation(Context context) {
        LocationManager mLocationManager;
        mLocationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        android.location.Location bestLocation = null;
        for (String provider : providers) {
            android.location.Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        context.getApplicationContext().getSharedPreferences("currentUser",Context.MODE_MULTI_PROCESS).edit().putString("currentLat", bestLocation.getLatitude()+"").apply();
        context.getApplicationContext().getSharedPreferences("currentUser",Context.MODE_MULTI_PROCESS).edit().putString("currentLng", bestLocation.getLongitude()+"").apply();
        context.getApplicationContext().getSharedPreferences("currentUser",Context.MODE_MULTI_PROCESS).edit().putString("address", getCompleteAddressString(context , bestLocation.getLatitude(),bestLocation.getLongitude())).apply();
        return bestLocation;
    }

    public String getCompleteAddressString(Context context , double LATITUDE, double LONGITUDE) {
        // this.setContext(ma);
        String strAdd = "";
        StringBuilder strReturnedAddress = new StringBuilder("");
        try {

            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);

            if (addresses != null) {
                Address returnedAddress = addresses.get(0);

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            }

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            //Toast.makeText(this, "Address : " + address, Toast.LENGTH_SHORT).show();
            String city = addresses.get(0).getLocality();
            //Toast.makeText(this, "City : " + city, Toast.LENGTH_SHORT).show();
            String state = addresses.get(0).getAdminArea();
            //Toast.makeText(this, "State : " + state, Toast.LENGTH_SHORT).show();
            String country = addresses.get(0).getCountryName();
            //Toast.makeText(this, "Country : " + country, Toast.LENGTH_SHORT).show();
            //String postalCode = addresses.get(0).getPostalCode();
            //Toast.makeText(this,"Postal Code : " + postalCode,Toast.LENGTH_SHORT).show();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
            //Toast.makeText(this, "Known Name : " + knownName, Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
            //Log.w("My Current location address", "Cannot get Address!");
        }

        return strAdd;
    }

}
