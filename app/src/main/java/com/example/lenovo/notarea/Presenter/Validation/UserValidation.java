package com.example.lenovo.notarea.Presenter.Validation;

import android.content.Context;
import android.widget.EditText;
import android.widget.Toast;

public class UserValidation {

    public UserValidation() {
    }

    public String validateUserPassword(String userPassword) {
        if (userPassword.equals("")) {
            return "can't be empty";
        }
        if (userPassword.length() > 20) {
            return "too long";
        }
        return null;
    }

    public String validateUserPhone(String userPhone) {
        if (userPhone.equals(""))
            return "can't be empty";

        if (userPhone.length() > 12)
            return "too long number";

        return null;
    }

    public boolean validateExtraNumberInput(EditText extra_number, Context context) {
        boolean isGood = true;
        String item = extra_number.getText().toString();
        if (item.equals("")) {
            isGood = false;
            Toast.makeText(context, "can't be empty", Toast.LENGTH_SHORT).show();
        } else if (item.contains("-")) {
            String name = item.split("-")[0];
            String number = item.split("-")[1];
            String name_regix = ".*[a-zA-Z]+.*";
            String number_regix = ".*[0-9]*";
            if (!name.matches(name_regix) || !number.matches(number_regix)) {
                Toast.makeText(context, "you must enter name followed by number like Mina-012xxx", Toast.LENGTH_SHORT).show();
                isGood = false;
            }
        } else {
            Toast.makeText(context, "you must enter name followed by number like Mina-012xxx", Toast.LENGTH_SHORT).show();
            isGood = false;
        }
        return isGood;
    }

    public String validateUserEmail(String userMail) {
        if (userMail.equals("")) {
            return "can't be empty";
        }
        String regx = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        if (!userMail.matches(regx)) {
            return "wrong email format";
        }
        if (userMail.length() > 45) {
            return "to long";
        }
        return null;
    }

    public String validateUserName(String userName) {
        String regx = "^[a-zA-Z0-9._ -]*";
        if (userName.equals(""))
            return "can't be empty";

        if (userName.length() > 25)
            return "too long user name";

        if (!userName.matches(regx))
            return "can't have characters expect alphapets , digits , - , _";

        return null;
    }

    public String passwordMatcher(String p, String rp) {
        if (!p.equals("") && !rp.equals("")) {
            if (p.equals(rp))
                return null;
            else
                return "new password and confirm password must be equal";
        } else {
            return "nither new password and confirm password can't be empty";
        }
    }

    public String validate_facebook_url(String url) {
        String regx = "((http|https)://)?((www[.])|m[.])?facebook.com/.+";
        if (url != null) {
            if (url.equals("")) {
                return "can't be empty";
            }

            if (!url.matches(regx)) {
                return "enter valid facebook url";
            }
        }
        return null;
    }
}
