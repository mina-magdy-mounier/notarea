package com.example.lenovo.notarea.View.fragments;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.lenovo.notarea.CallBacks.FunctionCallback;
import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Presenter.JsonParser;
import com.example.lenovo.notarea.Presenter.PostPresenter;
import com.example.lenovo.notarea.R;
import com.example.lenovo.notarea.Presenter.view_model_adapters.GeneralPostsAdapter;
import com.example.lenovo.notarea.View.activity.MainActivity;

import java.util.Collections;


public class GeneralPageFragemnt extends Fragment {

    final String TAG = "GeneralPageFragemnt";

    static PostPresenter postPresenter;
    public ListView generalListView;
    SharedPreferences session, rememberME;
    ProgressBar report_progress;
    static GeneralPostsAdapter generalPostsAdapter;
    MainActivity mainActivity;
    public LinearLayout no_general;
    JsonParser jsonParser;
    Dialog reportDialog;
    EditText eReport;
    Button dSubmit, dCancel;
    String reportBody = null;
    String postID;

    public GeneralPageFragemnt() {
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View genralPage = inflater.inflate(R.layout.general_page_fragment, container, false);

        session = genralPage.getContext().getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, new IntentFilter("GeneralPageFragemnt"));
        reportDialog = new Dialog(getActivity(), R.style.CustomDialogTheme);
        reportDialog.setContentView(R.layout.report);
        report_progress = reportDialog.findViewById(R.id.report_progress);
        report_progress.setVisibility(View.GONE);
        eReport = reportDialog.findViewById(R.id.report);
        dSubmit = reportDialog.findViewById(R.id.report_submit);
        dCancel = reportDialog.findViewById(R.id.report_cancel);
        no_general = genralPage.findViewById(R.id.no_general);
        submitReport();
        cancelReport();
        postPresenter = new PostPresenter(session);
        jsonParser = new JsonParser();
        postPresenter.setMainActivity(mainActivity);
        postPresenter.getPost().setContext(getContext());
        postPresenter.setGeneralPageFragemnt(this);
        postPresenter.init();
        //  }
        generalListView = genralPage.findViewById(R.id.general_list_view);
        setHasOptionsMenu(true);

        return genralPage;
    }

    public void postsReady() {
        Collections.reverse(postPresenter.getPosts());
        generalPostsAdapter = new GeneralPostsAdapter(getActivity(), postPresenter.getPosts(), postPresenter.getUser().getId());
        generalListView.setAdapter(generalPostsAdapter);
        generalPostsAdapter.setGeneralPageFragemnt(this);
    }

    public void RemoveListViewItems(final int postion, final String postID) {
        postPresenter.removePost(postion, postID, new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                if (result.equals("\"Post Deleted\"")) {
                    postPresenter.getPosts().remove(postion);
                    generalPostsAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.refresh, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menuRefresh) {
            postPresenter.refreshPostsArray();
        }
        return super.onOptionsItemSelected(item);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String postJson = intent.getStringExtra("json");
            if (generalPostsAdapter != null) {
                if (postPresenter.getPosts() != null) {
                    postPresenter.getPosts().add(0, jsonParser.decodeGPostNotification(postJson));
                    generalPostsAdapter.notifyDataSetChanged();
                }
            } else {
                postPresenter.refreshPostsArray();
            }
        }
    };



    public void submitReport() {
        dSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                report_progress.setVisibility(View.VISIBLE);
                if (!eReport.getText().toString().equals("")) {
                    reportBody = eReport.getText().toString();
                    postPresenter.report(reportBody, postID, new FunctionCallback() {
                        @Override
                        public void onSuccess(String result) {
                            if (result.equals("done")) {
                                reportDialog.dismiss();
                                eReport.setText("");
                                reportBody = null;
                                report_progress.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "your report submitted succsefully", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onError(String result) {
                            reportDialog.dismiss();
                            eReport.setText("");
                            reportBody = null;
                            report_progress.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), "you have already report this post", Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    report_progress.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "report can't be empty", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void cancelReport() {
        dCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reportDialog.dismiss();
                eReport.setText("");
                reportBody = null;
            }
        });
    }

    public void report(String postID) {
        this.postID = postID;
        eReport.setText("");
        reportDialog.show();
    }

}
