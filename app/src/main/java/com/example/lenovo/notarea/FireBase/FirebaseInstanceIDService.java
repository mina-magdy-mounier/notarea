package com.example.lenovo.notarea.FireBase;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.lenovo.notarea.Presenter.SessionHandler;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class FirebaseInstanceIDService extends FirebaseInstanceIdService {

    SharedPreferences token1;
    SessionHandler sessionHandler = new SessionHandler();

    @Override
    public void onTokenRefresh() {
        token1 = getSharedPreferences("device_token", Context.MODE_PRIVATE);
        String token = FirebaseInstanceId.getInstance().getToken();
        sessionHandler.set_token(token1, token);
    }
}
