package com.example.lenovo.notarea.Presenter;

import android.content.Context;

import com.example.lenovo.notarea.CallBacks.FunctionCallback;
import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Model.ExtraNumbers;

public class EmergencyNumbers {
    private ExtraNumbers extraNumbers = new ExtraNumbers();
    private Context context;
    private JsonParser jsonParser = new JsonParser();

    public EmergencyNumbers() {
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void getEmergencyNumbers(final FunctionCallback callback) {
        extraNumbers.setContext(context);
        extraNumbers.getEmergencyNumbers("eg", new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                if (!result.equals("error") && !result.equals("php_error")) {
                    callback.onSuccess(result);
                } else {
                    callback.onError("error");
                }
            }
        });
    }

}
