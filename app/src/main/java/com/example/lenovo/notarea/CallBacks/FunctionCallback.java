package com.example.lenovo.notarea.CallBacks;


public interface FunctionCallback {
    void onSuccess(String result);

    void onError(String result);
}
