package com.example.lenovo.notarea.View.activity;

import android.Manifest;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.notarea.CallBacks.FunctionCallback;
import com.example.lenovo.notarea.Constants;
import com.example.lenovo.notarea.Intents;
import com.example.lenovo.notarea.Presenter.EmergencyPresenter;
import com.example.lenovo.notarea.Presenter.LocationServices;
import com.example.lenovo.notarea.Presenter.SessionHandler;
import com.example.lenovo.notarea.Presenter.UserPreseneter;
import com.example.lenovo.notarea.View.adapters.MainMenuAdapter;
import com.example.lenovo.notarea.R;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    final String TAG = "MainActivity";
    public boolean forground = false;
    public FloatingActionButton fab;
    DrawerLayout drawer;
    NavigationView navigationView;
    TextView user_name;
    Toolbar toolbar;
    ViewPager MainViewPager;
    TabLayout MainTabs;
    Dialog GoPremium;
    LinearLayout drawer_header;
    public ProgressBar main_progress_bar;
    Button GoPremiumBtn, cancelGoPreimum;
    UserPreseneter userPreseneter;
    SharedPreferences session;
    static MainMenuAdapter mainMenuAdapter;
    EmergencyPresenter emergencyPresenter;
    String countryCode;
    String allIsDone[] = {"", "", ""};
    Constants constants = new Constants();
    Intents intents = new Intents();
    SessionHandler sessionHandler = new SessionHandler();

    public static final int MULTIPLE_PERMISSIONS = 10; // code you want.
    String[] permissions = new String[]{
            Manifest.permission.SEND_SMS,
            Manifest.permission.ACCESS_FINE_LOCATION};

    int numbersOfClicksEmergency = 0;

    public String[] getAllIsDone() {
        return allIsDone;
    }

    public MainActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        //////////////////////Permessions////////////////////////////////////
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //checkPermissions();
        }
        ////////////////////////////////////////////////////////////////////
        setContentView(R.layout.activity_main);
        this.forground = true;
        userPreseneter = new UserPreseneter(session);
        navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        user_name = headerView.findViewById(R.id.user_name_header);
        fab = findViewById(R.id.fab);
        drawer = findViewById(R.id.drawer_layout);
        toolbar = findViewById(R.id.toolbar);
        MainViewPager = findViewById(R.id.MainViewPager);
        MainTabs = findViewById(R.id.MainTabs);
        GoPremium = new Dialog(this, R.style.CustomDialogTheme);
        GoPremium.setContentView(R.layout.go_premium);
        GoPremiumBtn = GoPremium.findViewById(R.id.GoPrem);
        cancelGoPreimum = GoPremium.findViewById(R.id.cancelPrem);
        main_progress_bar = findViewById(R.id.main_progress_bar);
        drawer_header = headerView.findViewById(R.id.drawer_header);
        setSupportActionBar(toolbar);
        main_progress_bar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.loading), PorterDuff.Mode.MULTIPLY);
        mainMenuAdapter = new MainMenuAdapter(getSupportFragmentManager(), this);
        MainViewPager.setAdapter(mainMenuAdapter);
        MainViewPager.setOffscreenPageLimit(3);
        MainTabs.setTabMode(TabLayout.MODE_FIXED);
        MainTabs.setTabGravity(TabLayout.GRAVITY_FILL);
        MainTabs.setupWithViewPager(MainViewPager);
        onFabSetListener();
        actionBarDrawerToggle();
        onNavigationItemSelected();
        String victim = MainActivity.this.getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("victim", "");
        if (victim.equals("true")) numbersOfClicksEmergency = 0;
        if (getIntent().getStringExtra("which_activity") != null) {
            int indx = Integer.parseInt(getIntent().getStringExtra("which_activity"));
            MainViewPager.setCurrentItem(indx);
        }

        MainViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 1) {
                    if (userPreseneter.getUser().getPremium() == 0) {
                        fab.setVisibility(View.GONE);
                    } else {
                        fab.setVisibility(View.VISIBLE);
                    }
                } else {
                    fab.setVisibility(View.VISIBLE);
                }

                if (position == 2) {
                    String victim = MainActivity.this.getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("victim", "");
                    if (victim.equals("true")) fab.setImageResource(R.drawable.ic_close_white_24dp);
                    else fab.setImageResource(R.drawable.sos);
                } else {
                    fab.setImageResource(R.drawable.ic_pen);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        GoPremiumBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goPremium();
            }
        });
        cancelGoPreimum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelGoPrem();
            }
        });
        session = getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        emergencyPresenter = new EmergencyPresenter(session);
        TelephonyManager tm = (TelephonyManager) getSystemService(getApplicationContext().TELEPHONY_SERVICE);
        countryCode = tm.getSimCountryIso();
        getSharedPreferences("currentUser", Context.MODE_PRIVATE).edit().putString("countryCode", countryCode).apply();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (CheckLocationPermission()) {
                if (!isLocationServiceRunning(LocationServices.class))
                    startService(new Intent(this, LocationServices.class));
            }
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        user_name.setText(session.getString("user_name", ""));
        if (sessionHandler.get_theme(session) != R.style.AppTheme) {
            switch (sessionHandler.get_theme(session)) {
                case R.style.blue:
                    toolbar.setBackgroundColor(getResources().getColor(R.color.loading));
                    fab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.loading)));
                    drawer_header.setBackgroundColor(getResources().getColor(R.color.loading));
                    break;

                case R.style.green:
                    toolbar.setBackgroundColor(getResources().getColor(R.color.green));
                    fab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.green)));
                    drawer_header.setBackgroundColor(getResources().getColor(R.color.green));
                    break;
            }
        } else {
            toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            fab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            drawer_header.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        }
    }

    public void onNavigationItemSelected() {
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void actionBarDrawerToggle() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void goPremium() {
        Intent intent = new Intent(MainActivity.this, CredietCard.class);
        startActivity(intent);
        // Toast.makeText(this, "Credit Card API", Toast.LENGTH_LONG).show();
    }

    private void cancelGoPrem() {
        GoPremium.dismiss();
    }

    public void onFabSetListener() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id = MainViewPager.getCurrentItem();
                if (id == 0) {
                    Intent intent = new Intent(MainActivity.this, GeneralPostForm.class);
                    startActivity(intent);
                } else if (id == 1) {
                    Intent intent = new Intent(MainActivity.this, AdsForm.class);
                    startActivity(intent);
                } else if (id == 2) {
                    String victim = MainActivity.this.getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("victim", "");
                    if (!victim.equals("true")) {
                        emergencyPresenter.addEmergency(MainActivity.this, true, countryCode);
                    } else {
                        if (numbersOfClicksEmergency == 0) {
                            Toast.makeText(MainActivity.this, "You Already In Emergency Mode , Click Two More Time To Cancel The current Emergency", Toast.LENGTH_SHORT).show();
                            numbersOfClicksEmergency++;
                        } else if (numbersOfClicksEmergency == 1) {
                            Toast.makeText(MainActivity.this, "You Already In Emergency Mode , Click One More Time To Cancel The current Emergency", Toast.LENGTH_SHORT).show();
                            numbersOfClicksEmergency++;
                        } else if (numbersOfClicksEmergency == 2) {
                            Toast.makeText(MainActivity.this, "Remove Pressed", Toast.LENGTH_SHORT).show();
                            MainActivity.this.main_progress_bar.setVisibility(View.VISIBLE);
                            emergencyPresenter.removeEmergency(MainActivity.this, new FunctionCallback() {
                                @Override
                                public void onSuccess(String result) {
                                    MainActivity.this.main_progress_bar.setVisibility(View.INVISIBLE);
                                    Toast.makeText(MainActivity.this, "Emergency Removed", Toast.LENGTH_SHORT).show();
                                    getSharedPreferences("currentUser", Context.MODE_PRIVATE).edit().putString("victim", "false").apply();
                                    fab.setImageResource(R.drawable.sos);
                                    numbersOfClicksEmergency = 0;
                                }

                                @Override
                                public void onError(String result) {
                                    MainActivity.this.main_progress_bar.setVisibility(View.INVISIBLE);
                                    Toast.makeText(MainActivity.this, "Emergency Dosen't Canceled", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                    //}
                    //else{emergencyPresenter.removeEmergency(MainActivity.this, true, countryCode);}
                    //Intent intent = new Intent(MainActivity.this, EmergencyActivate.class);
                    //startActivity(intent);
                }
            }
        });
    }

    // control the hardware back button action in this case if the side menu shown it will hide it.
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            moveTaskToBack(true);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            Intent intent = new Intent(MainActivity.this, UserProfile.class);
            startActivity(intent);
        } else if (id == R.id.nav_setting) {
            Intent intent = new Intent(MainActivity.this, Settings.class);
            startActivity(intent);
        } else if (id == R.id.nav_premium) {
            GoPremium.show();
        } else if (id == R.id.nav_sign_out) {
            Toast.makeText(MainActivity.this, "Bye Bye", Toast.LENGTH_SHORT).show();
            userPreseneter.signOut(this);
        } else if (id == R.id.mail_us) {
            Toast.makeText(MainActivity.this, "share", Toast.LENGTH_SHORT).show();
            intents.emailTo(constants.getMAIL(), MainActivity.this);
        } else if (id == R.id.nav_help) {
            Toast.makeText(MainActivity.this, "help", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_privacy) {
            Toast.makeText(MainActivity.this, "privacy", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_savedPosts) {
            Intent intent = new Intent(MainActivity.this, SavedPosts.class);
            startActivity(intent);
        } else if (id == R.id.nav_emergency_numbers) {
            Intent intent = new Intent(MainActivity.this, EmergencyNumbers.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public boolean CheckLocationPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 99);
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 99);
            }
            // MY_PERMISSIONS_REQUEST_FINE_LOCATION is an
            // app-defined int constant. The callback method gets the
            // result of the request.
            return false;
        }
        //startService(new Intent(this, LocationServices.class));
        return true;
    }

    private boolean isLocationServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                //TC
//                Toast.makeText(this, "Service is Running", Toast.LENGTH_LONG).show();
                return true;
            }
        }
        return false;
    }
}
