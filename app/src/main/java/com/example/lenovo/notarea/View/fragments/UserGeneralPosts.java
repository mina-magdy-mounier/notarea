package com.example.lenovo.notarea.View.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Presenter.JsonParser;
import com.example.lenovo.notarea.Presenter.PostPresenter;
import com.example.lenovo.notarea.Presenter.UserPreseneter;
import com.example.lenovo.notarea.Presenter.view_model_adapters.GeneralPostsAdapter;
import com.example.lenovo.notarea.R;


public class UserGeneralPosts extends Fragment {

    private UserPreseneter userPreseneter;
    private PostPresenter postPresenter;
    private SharedPreferences session;
    private JsonParser jsonParser = new JsonParser();
    private GeneralPostsAdapter generalPostsAdapter;
    private ListView generalListView;
    private LinearLayout my_posts;
    private LinearLayout has_posts;

    public UserGeneralPosts() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_general_posts, container, false);

        session = view.getContext().getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        userPreseneter = new UserPreseneter(session);
        postPresenter = new PostPresenter(session);
        generalListView = view.findViewById(R.id.general_list);
        my_posts = view.findViewById(R.id.my_posts);
        has_posts = view.findViewById(R.id.has_posts);
        init();
        return view;
    }

    public void RemoveListViewItems(final int postion, final String postID) {
        postPresenter.removePost(-1, postID, new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                if (result.equals("\"Post Deleted\"")) {
                    userPreseneter.getUser().getMyGPosts().remove(postion);
                    generalPostsAdapter.notifyDataSetChanged();
                }
            }
        });
    }


    private void init() {
        my_posts.setVisibility(View.VISIBLE);
        generalListView.setVisibility(View.GONE);
        has_posts.setVisibility(View.GONE);
        userPreseneter.getMyGPosts(new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                if (result != null) {
                    if (!result.equals("error") && !result.equals("php_error")) {
                        try {
                            my_posts.setVisibility(View.GONE);
                            generalListView.setVisibility(View.VISIBLE);
                            has_posts.setVisibility(View.GONE);
                            userPreseneter.getUser().setMyGPosts(jsonParser.decodeGlobalGPost(result));
                            generalPostsAdapter = new GeneralPostsAdapter(getActivity(), userPreseneter.getUser().getMyGPosts(), userPreseneter.getUser().getId());
                            generalPostsAdapter.setUserGeneralPosts(UserGeneralPosts.this);
                            generalListView.setAdapter(generalPostsAdapter);
                        } catch (Exception e) {

                        }
                    } else if (result.equals("error")) {
                        has_posts.setVisibility(View.VISIBLE);
                        my_posts.setVisibility(View.GONE);
                        generalListView.setVisibility(View.GONE);
                    } else {
                        Snackbar.make(generalListView, "connection error", Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });
    }
}