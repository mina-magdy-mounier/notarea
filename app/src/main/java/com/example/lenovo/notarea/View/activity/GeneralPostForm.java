package com.example.lenovo.notarea.View.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

import com.example.lenovo.notarea.CallBacks.FunctionCallback;
import com.example.lenovo.notarea.Constants;
import com.example.lenovo.notarea.Presenter.SessionHandler;
import com.example.lenovo.notarea.Presenter.UserPreseneter;
import com.example.lenovo.notarea.Presenter.view_model_adapters.BusinessListAdapter;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.notarea.Presenter.PostPresenter;
import com.example.lenovo.notarea.R;

import java.util.ArrayList;

public class GeneralPostForm extends AppCompatActivity {

    private Constants constants = new Constants();

    private int MAX_DISTANCE = constants.getG_MAX_DISTANCE();
    private int MIN_DISTANCE = constants.getG_MIN_DISTANCE();
    private int DISTANCE_STEP = constants.getG_DISTANCE_STEP();

    EditText postTilte, postBody, faceBook;
    Menu menu;
    TextView happy, sad, intrested, shocked;
    TextView faceBookAttachment, locationAttachment, phoneAttachment, number_wheel_title;
    LinearLayout userFeelingSection, contact_my_layout;
    ImageView arrow;
    NumberPicker number_wheel;
    Button closeAttachment, addFaceBookBtn, number_wheel_ok, number_wheel_cancel , cancelFaceBookBtn;
    public ProgressBar adding_in_progress;
    FloatingActionButton facebook, location, phone, distance;
    FloatingActionMenu floatingActionMenu;
    boolean isExpanded = true;
    int feelingEmoji = 0;
    String feelingDesc = "", FaceBook = null, shared_distance = String.valueOf(MIN_DISTANCE), temp_distance = null;
    Boolean addLocation = false;
    Dialog phoneListDialog, addFacebookUrlDialog, distanceDialog;
    ListView phoneList;
    ArrayList<String> attachedBusinessNumbersIDs = new ArrayList<>();
    UserPreseneter userPreseneter;
    PostPresenter postPresenter;
    FormHandler formHandler;
    SharedPreferences session;
    BusinessListAdapter businessListAdapter;
    SessionHandler sessionHandler = new SessionHandler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        if (sessionHandler.get_theme(session) != R.style.AppTheme) {
            setTheme(sessionHandler.get_theme(session));
        }
        setContentView(R.layout.general_post_form);
//        session = getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        userPreseneter = new UserPreseneter(session);
        postPresenter = new PostPresenter(session);
        formHandler = new FormHandler();
        addFacebookUrlDialog = new Dialog(this, R.style.CustomDialogTheme);
        phoneListDialog = new Dialog(this, R.style.CustomDialogTheme);
        phoneListDialog.setContentView(R.layout.phone_attachment);
        addFacebookUrlDialog.setContentView(R.layout.add_facebook_url);
        faceBook = addFacebookUrlDialog.findViewById(R.id.facebookUrl);
        cancelFaceBookBtn = addFacebookUrlDialog.findViewById(R.id.cancelFaceBookBtn);
        addFaceBookBtn = addFacebookUrlDialog.findViewById(R.id.addFaceBookBtn);
        phoneList = phoneListDialog.findViewById(R.id.phoneAttachment);
        contact_my_layout = findViewById(R.id.contact_my_layout);
        closeAttachment = phoneListDialog.findViewById(R.id.close_attachment);
        postBody = findViewById(R.id.post_body_input);
        happy = findViewById(R.id.happy);
        sad = findViewById(R.id.sad);
        intrested = findViewById(R.id.intrested);
        shocked = findViewById(R.id.shocked);
        userFeelingSection = findViewById(R.id.user_feeling_section);
        arrow = findViewById(R.id.userFeelingToggle);
        facebook = findViewById(R.id.add_facebook_url);
        location = findViewById(R.id.add_location_);
        phone = findViewById(R.id.add_phone_number);
        faceBookAttachment = findViewById(R.id.add_facebook);
        locationAttachment = findViewById(R.id.add_location);
        phoneAttachment = findViewById(R.id.add_phone);
        postTilte = findViewById(R.id.post_title);
        floatingActionMenu = findViewById(R.id.fob_menu);
        adding_in_progress = findViewById(R.id.adding_post_progress_bar);
        // new
        distanceDialog = new Dialog(this, R.style.CustomDialogTheme);
        distanceDialog.setContentView(R.layout.number_wheel);
        distance = findViewById(R.id.distance_picker);
        number_wheel = distanceDialog.findViewById(R.id.number_picker);
        number_wheel_ok = distanceDialog.findViewById(R.id.ok);
        number_wheel_cancel = distanceDialog.findViewById(R.id.cancel);
        number_wheel_title = distanceDialog.findViewById(R.id.number_wheel_title);
        number_wheel_title.setText("Choose distance in Meter");
        formHandler.cancel_dialog(number_wheel_cancel, distanceDialog);
        formHandler.set_wheel(MAX_DISTANCE, MIN_DISTANCE, DISTANCE_STEP, number_wheel);
        formHandler.show_dialog(distance, distanceDialog);
        get_distance();
        distance_listener();
        adding_in_progress.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.loading), PorterDuff.Mode.MULTIPLY);
        floatingActionMenu.setClosedOnTouchOutside(true);
        if (userPreseneter.getUser().getBusinessNumbers() != null) {
            businessListAdapter = new BusinessListAdapter(getApplicationContext(), userPreseneter.getUser().getBusinessNumbers());
            phoneList.setAdapter(businessListAdapter);
        }
        FormHandler.collapse(userFeelingSection);
        formHandler.showUserFeeling(arrow, isExpanded, userFeelingSection);
        formHandler.addLocation(location, locationAttachment, postBody, null);
        formHandler.addPhone(phoneList, phoneListDialog, phone, attachedBusinessNumbersIDs, contact_my_layout, this, null);
        formHandler.cancelLocationAttachment(locationAttachment, null, postBody);
        formHandler.closePhoneAttachment(closeAttachment, phoneListDialog);
        formHandler.onWritingPost(postBody);
        formHandler.onWritingTitle(postTilte);
        formHandler.addFaceBook(addFacebookUrlDialog, faceBook, facebook, addFaceBookBtn, faceBookAttachment, new FunctionCallback() {
            @Override
            public void onSuccess(String result) {
                if (result != null) {
                    FaceBook = result;
                }
            }

            @Override
            public void onError(String result) {
                if (result != null) {
                    FaceBook = null;
                    Toast.makeText(GeneralPostForm.this, result, Toast.LENGTH_LONG).show();
                }
            }
        });
        formHandler.cancel_dialog(cancelFaceBookBtn , addFacebookUrlDialog);
        formHandler.cancelFaceBookAttachment(faceBookAttachment, new FunctionCallback() {
            @Override
            public void onSuccess(String result) {
                if (result == null) {
                    FaceBook = null;
                    faceBook.setText("");
                }
            }

            @Override
            public void onError(String result) {
                if (result.equals("error"))
                    Toast.makeText(GeneralPostForm.this, "somthing wrong happened", Toast.LENGTH_LONG).show();
            }
        });

        feelingListener();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.post, menu);
        this.menu = menu;
        formHandler.setMenu(menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_name) {
            if (String.valueOf(locationAttachment.getVisibility()).equals(String.valueOf(View.VISIBLE))) {
                addLocation = true;
            }
            postPresenter.addPost(GeneralPostForm.this, postTilte.getText().toString(), postBody.getText().toString().trim(), feelingDesc, feelingEmoji, FaceBook, attachedBusinessNumbersIDs, addLocation, shared_distance);
        }
        return super.onOptionsItemSelected(item);
    }

    private void get_distance() {
        number_wheel_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shared_distance = temp_distance;
                distanceDialog.dismiss();
            }
        });
    }

    private void distance_listener() {
        number_wheel.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int old_number, int new_number) {
                temp_distance = numberPicker.getDisplayedValues()[new_number];
            }
        });
    }

    public void feelingListener() {
        happy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!feelingDesc.equals("Feeling Happy")) {
                    feelingEmoji = R.drawable.ic_emoji;
                    feelingDesc = "Feeling Happy";
                    formHandler.setBorder(happy, sad, intrested, shocked);
                } else {
                    feelingEmoji = 0;
                    feelingDesc = "";
                    formHandler.removeBorder(happy);
                }
            }
        });

        sad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!feelingDesc.equals("Feeling Sad")) {
                    feelingEmoji = R.drawable.ic_sad_emoji;
                    feelingDesc = "Feeling Sad";
                    formHandler.setBorder(sad, happy, intrested, shocked);
                } else {
                    feelingEmoji = 0;
                    feelingDesc = "";
                    formHandler.removeBorder(sad);
                }
            }
        });

        intrested.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!feelingDesc.equals("Feeling Intrested")) {
                    feelingEmoji = R.drawable.ic_intrested_emoji;
                    feelingDesc = "Feeling Intrested";
                    formHandler.setBorder(intrested, happy, sad, shocked);
                } else {
                    feelingEmoji = 0;
                    feelingDesc = "";
                    formHandler.removeBorder(intrested);
                }
            }
        });

        shocked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!feelingDesc.equals("Feeling Shocked")) {
                    feelingEmoji = R.drawable.ic_shocked_emoji;
                    feelingDesc = "Feeling Shocked";
                    formHandler.setBorder(shocked, happy, sad, intrested);
                } else {
                    feelingEmoji = 0;
                    feelingDesc = "";
                    formHandler.removeBorder(shocked);
                }
            }
        });
    }


}
