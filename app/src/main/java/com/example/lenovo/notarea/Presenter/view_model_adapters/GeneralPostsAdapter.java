package com.example.lenovo.notarea.Presenter.view_model_adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.notarea.CallBacks.FunctionCallback;
import com.example.lenovo.notarea.Intents;
import com.example.lenovo.notarea.Model.ExtraNumbers;
import com.example.lenovo.notarea.Model.Location;
import com.example.lenovo.notarea.Presenter.JsonParser;
import com.example.lenovo.notarea.Presenter.Maps.GeneralAdvMap;
import com.example.lenovo.notarea.R;
import com.example.lenovo.notarea.Model.Post;
import com.example.lenovo.notarea.View.activity.GPostEditor;
import com.example.lenovo.notarea.View.fragments.GeneralPageFragemnt;
import com.example.lenovo.notarea.View.fragments.UserGeneralPosts;
import com.example.lenovo.notarea.View.fragments.UserSavedGPosts;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class GeneralPostsAdapter extends ArrayAdapter<Post> {

    private String currentUserID;
    final int blue_color = Color.rgb(77, 136, 255);
    final int grey_color = Color.rgb(140, 140, 140);
    AdapterHandler adapterHandler = new AdapterHandler();
    ArrayList<Post> generalPostsBodies;
    GeneralPageFragemnt generalPageFragemnt;
    UserGeneralPosts userGeneralPosts;
    UserSavedGPosts userSavedGPosts;
    JsonParser jsonParser = new JsonParser();
    Intents intents = new Intents();

    public void setGeneralPageFragemnt(GeneralPageFragemnt generalPageFragemnt) {
        this.generalPageFragemnt = generalPageFragemnt;
    }

    public void setUserGeneralPosts(UserGeneralPosts userGeneralPosts) {
        this.userGeneralPosts = userGeneralPosts;
    }

    public void setUserSavedGPosts(UserSavedGPosts userSavedGPosts) {
        this.userSavedGPosts = userSavedGPosts;
    }

    public GeneralPostsAdapter(Context context, ArrayList<Post> generalPostsBodies, String currentUserID) {
        super(context, 0, generalPostsBodies);
        this.currentUserID = currentUserID;
        this.generalPostsBodies = generalPostsBodies;

    }

    public void facebookListiner(LinearLayout contactByfacebook, final String url, final Context context) {
        contactByfacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                intents.openFacebook(url, context);
            }
        });
    }

    public void phoneListiner(final Spinner spinner, final Context context) {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!spinner.getSelectedItem().toString().equals("Click to Call")) {
                    intents.call(spinner.getSelectedItem().toString(), context);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void locationListiner(LinearLayout contactByLocation, final Location location, final Context context) {
        contactByLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, GeneralAdvMap.class);
                intent.putExtra("LAT", String.valueOf(location.getLatitude()));
                intent.putExtra("LNG", String.valueOf(location.getLongitude()));
                context.startActivity(intent);
                Toast.makeText(context, location.getLongitude() + " " + location.getLatitude() + "", Toast.LENGTH_SHORT).show();
                Toast.makeText(getContext(), "post location ", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable final View convertView, @NonNull final ViewGroup parent) {
        View listViewItem = convertView;
        if (listViewItem == null) {
            listViewItem = LayoutInflater.from(getContext()).inflate(
                    R.layout.general_post_body, parent, false);
        }

        final Post post = getItem(position);
        final TextView userName = listViewItem.findViewById(R.id.user_name_id);
        TextView userFeeling = listViewItem.findViewById(R.id.user_feeling_id);
        TextView userPostBody = listViewItem.findViewById(R.id.user_post_body_id);
        ImageView userImage = listViewItem.findViewById(R.id.user_image_id);
        TextView dislike = listViewItem.findViewById(R.id.dislike);
        TextView like = listViewItem.findViewById(R.id.like);
        TextView likeCount = listViewItem.findViewById(R.id.likeCount);
        TextView dislikeCount = listViewItem.findViewById(R.id.disLikeCount);
        TextView save_post = listViewItem.findViewById(R.id.save_post);
        final TextView setting = listViewItem.findViewById(R.id.general_page_posts_setting);
        ImageView feelingEmoji = listViewItem.findViewById(R.id.emoji);
        LinearLayout contactByfacebook = listViewItem.findViewById(R.id.contact_me_by_face);
        LinearLayout contactByLocation = listViewItem.findViewById(R.id.contact_me_by_location);
        LinearLayout contactByPhone = listViewItem.findViewById(R.id.contact_me_by_phone);
        TextView facebook = listViewItem.findViewById(R.id.facebook_url);
        TextView location = listViewItem.findViewById(R.id.location);
        TextView phone = listViewItem.findViewById(R.id.phone);
        final Spinner phoneSpinner = listViewItem.findViewById(R.id.phoneSpinner);
        TextView contactMe = listViewItem.findViewById(R.id.contactMe);
        final LinearLayout gpost_body = listViewItem.findViewById(R.id.gpost_body);

        TextView title = listViewItem.findViewById(R.id.post_title);
        TextView date = listViewItem.findViewById(R.id.date);
        contactMe.setVisibility(View.GONE);

        userName.setText(post.getUser().getName());

        if (post.getTitle() == null || post.getTitle().toString().equals("")) {
            title.setVisibility(View.GONE);
        } else {
            title.setText(post.getTitle().toString());
            title.setVisibility(View.VISIBLE);
        }

        if (post.getDate() == null || post.getDate().toString().equals("")) {
            date.setVisibility(View.GONE);
        } else {
            date.setText(post.getDate().toString());
            date.setVisibility(View.VISIBLE);
        }


        if (post.getUser().getProfile_Img() != null) {
            Picasso.get().load(post.getUser().getProfile_Img()).into(userImage);
        } else {
            userImage.setImageResource(R.drawable.nobody);
        }

        if (post.getUserFeelingImg() > 0) {
            feelingEmoji.setVisibility(View.VISIBLE);
            feelingEmoji.setBackgroundResource(post.getUserFeelingImg());
        } else {
            feelingEmoji.setVisibility(View.GONE);
        }

        if (post.getUserFeeling() != null) {
            userFeeling.setVisibility(View.VISIBLE);
            userFeeling.setText(post.getUserFeeling());
        } else {
            userFeeling.setVisibility(View.GONE);
        }

        userPostBody.setText(post.getPostBody());

        if (post.getAttachment() != null) {
            contactMe.setVisibility(View.VISIBLE);
            contactMe.setText("Contact Me: ");
            if (post.getAttachment().getFacebook_url() != null && !post.getAttachment().getFacebook_url().equals("")) {
                contactByfacebook.setVisibility(View.VISIBLE);
                facebook.setText(post.getAttachment().getFacebook_url());
            } else {
                contactByfacebook.setVisibility(View.GONE);
            }

            if (post.getAttachment().getLocation() != null) {
                contactByLocation.setVisibility(View.VISIBLE);
                location.setText("user location");
            } else {
                contactByLocation.setVisibility(View.GONE);
            }


            if (post.getAttachment().getPhoneNumber() != null) {
                ArrayList<ExtraNumbers> phones;
                phones = post.getAttachment().getPhoneNumber();
                ArrayList<String> values = new ArrayList<String>();
                values.add("Click to Call");
                for (int i = 0; i < phones.size(); i++) {
                    values.add(phones.get(i).getNumber());
                }
                contactByPhone.setVisibility(View.VISIBLE);
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(listViewItem.getContext(), android.R.layout.simple_spinner_item, values);
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                phoneSpinner.setAdapter(spinnerArrayAdapter);
            } else {
                contactByPhone.setVisibility(View.GONE);
            }
        } else {
            contactByPhone.setVisibility(View.GONE);
            contactByLocation.setVisibility(View.GONE);
            contactByfacebook.setVisibility(View.GONE);
            contactMe.setVisibility(View.GONE);
            contactMe.setText("");
        }

        if (post.isLiked() == 1) {
            like.setText("liked");
            like.setTextColor(blue_color);
        } else {
            like.setText("like");
            like.setTextColor(grey_color);
        }

        if (post.isDisliked() == 1) {
            dislike.setText("disliked");
            dislike.setTextColor(blue_color);
        } else {
            dislike.setText("dislike");
            dislike.setTextColor(grey_color);
        }

        if (post.isSaved() == 1) {
            save_post.setText("saved");
            save_post.setTextColor(blue_color);
        } else {
            save_post.setText("save");
            save_post.setTextColor(grey_color);

        }

        dislikeCount.setText(String.valueOf(post.getNumberOfDislikes()));
        likeCount.setText(String.valueOf(post.getNumberOfLikes()));


        final View finalListViewItem = listViewItem;

        adapterHandler.likePost(like, likeCount, dislike, dislikeCount, post, this.currentUserID);
        adapterHandler.disLikePost(dislike, dislikeCount, like, likeCount, post, this.currentUserID);
        adapterHandler.savePost(save_post, post, this.currentUserID, new FunctionCallback() {
            @Override
            public void onSuccess(String result) {
                if (result.equals("1")) {
                    if (userSavedGPosts != null) {
                        userSavedGPosts.init();
                    }
                }
            }

            @Override
            public void onError(String result) {

            }
        });
        if (post.getAttachment() != null) {
            facebookListiner(contactByfacebook, post.getAttachment().getFacebook_url(), finalListViewItem.getContext());
            locationListiner(contactByLocation, post.getLocation(), finalListViewItem.getContext());
            phoneListiner(phoneSpinner, finalListViewItem.getContext());
        }

        final PopupMenu popup = new PopupMenu(getContext(), setting);
        final Menu p;


        popup.getMenuInflater().inflate(R.menu.post_popup_menu, popup.getMenu());
        setting.setVisibility(View.VISIBLE);
        p = popup.getMenu();

        if (post.getUser().getId().equals(currentUserID)) {
            p.findItem(R.id.edit).setVisible(true);
            p.findItem(R.id.delete).setVisible(true);
            p.findItem(R.id.report).setVisible(false);
            p.findItem(R.id.category).setVisible(false);
        } else {
            p.findItem(R.id.edit).setVisible(false);
            p.findItem(R.id.delete).setVisible(false);
            p.findItem(R.id.report).setVisible(true);
            p.findItem(R.id.category).setVisible(false);
        }

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.edit:
                        Intent intent = new Intent(getContext(), GPostEditor.class);
                        intent.putExtra("ePost", jsonParser.encodeGEditor(post).toString());
                        getContext().startActivity(intent);
                        return true;
                    case R.id.delete:
                        if (generalPageFragemnt != null) {
                            generalPageFragemnt.RemoveListViewItems(position, post.getPostID());
                        }
                        if (userGeneralPosts != null) {
                            userGeneralPosts.RemoveListViewItems(position, post.getPostID());
                        }
                        return true;
                    case R.id.report:
                        if (generalPageFragemnt != null) {
                            generalPageFragemnt.report(post.getPostID());
                        }
                        return true;
                }
                return true;
            }
        });


        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_setting);
                setting.startAnimation(animation);
                popup.show();
            }
        });


        return listViewItem;
    }


}
