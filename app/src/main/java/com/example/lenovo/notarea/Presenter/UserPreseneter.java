package com.example.lenovo.notarea.Presenter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import android.util.Patterns;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.notarea.CallBacks.FunctionCallback;
import com.example.lenovo.notarea.Model.Advertisement;
import com.example.lenovo.notarea.Model.ExtraNumbers;
import com.example.lenovo.notarea.Model.Location;
import com.example.lenovo.notarea.Model.Post;
import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Model.User;
import com.example.lenovo.notarea.Presenter.Validation.UserValidation;
import com.example.lenovo.notarea.Presenter.Verification.UserVerification;
import com.example.lenovo.notarea.R;
import com.example.lenovo.notarea.View.activity.MainActivity;
import com.example.lenovo.notarea.View.activity.ProfileEditor;
import com.example.lenovo.notarea.View.activity.Settings;
import com.example.lenovo.notarea.View.activity.SignIn;
import com.example.lenovo.notarea.View.activity.SignUp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static android.content.Context.LOCATION_SERVICE;


public class UserPreseneter {

    private final String TAG = "UserPresenter";

    private User user = new User();
    private Post post = new Post();
    private Advertisement advertisement = new Advertisement();
    private JsonParser jsonParser = new JsonParser();
    private SessionHandler sessionHandler = new SessionHandler();
    private SharedPreferences session;
    private Context context;

    public void setSession(SharedPreferences session) {
        this.session = session;
    }

    public UserPreseneter() {
    }

    public UserPreseneter(SharedPreferences session) {
        this.session = session;
        sessionHandler.getSession(session, this.user);
    }

    public User getUser() {
        return user;

    }

    public void signUp(final SignUp context, String profileImg, String name, String email, String password, String phone, String userAge, String userGender, HashMap<String, String> familyList, HashMap<String, String> businessList) {
        String check = checkMail_Passowrd(email, password);
        if (!check.equals("")) {
            Toast.makeText(context.getApplicationContext(), check, Toast.LENGTH_LONG).show();
            context.loading_dialog.dismiss();
            return;
        }
        FirebaseInstanceId.getInstance().getToken();
        FirebaseMessaging.getInstance().subscribeToTopic("test");
        SharedPreferences token1 = context.getSharedPreferences("device_token", Context.MODE_PRIVATE);
        String token = sessionHandler.get_token(token1);
        Location location = new Location();
        if (context.getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLng", "").equals("")) {
            android.location.Location lastKnownLocation = getLastKnownLocation(context);
            location = new Location(lastKnownLocation.getLongitude(), lastKnownLocation.getLatitude());
        } else {
            location = new Location(Double.parseDouble(context.getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLng", "")), Double.parseDouble(context.getApplicationContext().getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLat", "")));
        }
        User user = new User(profileImg, name, email, password, phone, userAge, userGender, familyList, businessList, location, 0);
        user.setToken(token);
        user.setContext(context);
        JSONObject userJson = jsonParser.encodeUser(user);
        user.addUser(userJson, new ServerCallback() {
                    @Override
                    public void onSuccess(String currentUserJson) {
                        if (currentUserJson.equals("Email is already taken")) {
                            Toast.makeText(context.getApplicationContext(), "Email is already taken", Toast.LENGTH_LONG).show();
                            context.loading_dialog.dismiss();
                        } else if (currentUserJson.equals("failed validation Error")) {
                            Toast.makeText(context.getApplicationContext(), "failed validation Error", Toast.LENGTH_LONG).show();
                            context.loading_dialog.dismiss();
                        } else {
                            context.loading_dialog.dismiss();
                            try {
                                JSONObject user = new JSONObject(currentUserJson);
                                sessionHandler.setSession(user, session);
                                Intent intent = new Intent(context.getApplicationContext(), MainActivity.class);
                                context.startActivity(intent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        );
    }

    public void signIn(String email, String password, final SignIn context, final CheckBox checkBox, final ServerCallback callback) {
        String check = checkMail_Passowrd(email, password);
        if (!check.equals("")) {
            Toast.makeText(context.getApplicationContext(), check, Toast.LENGTH_LONG).show();
            context.signInProgressBar.setVisibility(View.INVISIBLE);
            return;
        }
        final User currentUser = new User();
        currentUser.setContext(context);
        try {
            SharedPreferences token1 = context.getSharedPreferences("device_token", Context.MODE_PRIVATE);
            String token = sessionHandler.get_token(token1);
            currentUser.getUser(email, password, token, new ServerCallback() {
                        @Override
                        public void onSuccess(String currentUserJson) {
                            if (currentUserJson.equals("password is incorrect")) {
                                Toast.makeText(context.getApplicationContext(), "password is incorrect", Toast.LENGTH_LONG).show();
                                context.loading_dialog.dismiss();
                            } else if (currentUserJson.equals("Not Found")) {
                                Toast.makeText(context.getApplicationContext(), "Not Found", Toast.LENGTH_LONG).show();
                                context.loading_dialog.dismiss();
                            } else if (currentUserJson.equals("failed validation Error")) {
                                Toast.makeText(context.getApplicationContext(), "failed validation Error", Toast.LENGTH_LONG).show();
                                context.loading_dialog.dismiss();
                            } else if (currentUserJson.equals("token_error")) {
                                Toast.makeText(context.getApplicationContext(), "can't have 2 accounts on same device", Toast.LENGTH_LONG).show();
                                context.loading_dialog.dismiss();
                            } else {
                                JSONObject user = null;
                                context.loading_dialog.dismiss();
                                try {
                                    user = new JSONObject(currentUserJson);
                                    if (checkBox.isChecked()) {
                                        sessionHandler.remember_me(session);
                                    }
                                    sessionHandler.setSession(user, session);
                                    callback.onSuccess("Sign in Success");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
            );
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
    }

    public void signOut(final MainActivity context) {
        JSONObject sign_out = jsonParser.signOut(this.user.getId());
        user.sign_out(sign_out, new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                if (!result.equals("error") && !result.equals("php_error")) {
                    SharedPreferences.Editor editor = session.edit();
                    editor.clear();
                    editor.apply();
                    Intent intent = new Intent(context.getApplicationContext(), SignIn.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    context.finish();
                }
            }
        });
    }

    public void initUserProfile(ImageView userProfileImg, TextView userName, TextView userGender, TextView userAge, TextView userLocation, TextView userMail, TextView userPhone, Context myContext) {

        context = myContext;
        String myAddress = myContext.getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("address", "");
        try {
            userName.setText("Name : " + user.getName());
            userGender.setText("Gender : " + user.getGender());
            userAge.setText("Age : " + user.getAge());
            userLocation.setText(myAddress);
            userMail.setText(user.getEmail());
            userPhone.setText(user.getPhone());
            if (user.getProfile_Img() != null) {
                Picasso.get().load(user.getProfile_Img()).into(userProfileImg);
            } else {
                userProfileImg.setImageResource(R.drawable.nobody);
            }
        } catch (Exception e) {
            Log.d(TAG, " error in initUserProfile in UserPresenter ");
        }
    }

    public void update_user_name(final Settings context, final String name) {
        user.setContext(context);
        JSONObject json = jsonParser.encodeUserNameEditor(user.getId(), name);
        user.changeUserName(json, new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
                context.user_name_loader.setVisibility(View.GONE);
                if (result.equals("user name changed succesfully")) {
                    context.userNameDialog.dismiss();
                    sessionHandler.update_user_name(session, name);
                }
            }
        });
    }

    public void change_password(String old_password, String new_password, final FunctionCallback callback) {
        JSONObject json = jsonParser.encodePasswordEditor(old_password, new_password, this.user.getId());
        this.user.changePassword(json, new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                if (result.equals("changed")) {
                    callback.onSuccess("password changed");
                } else {
                    callback.onError(result);
                }
            }
        });
    }

    public void editProfileInfo(final ProfileEditor context, String profile_img, String phone, String age, String gender, ArrayList<ExtraNumbers> family_list, ArrayList<ExtraNumbers> business_list, ArrayList<String> deleted_list, final boolean edited_parts[]) {
        final User editedProfileUser = new User(user.getId(), profile_img, phone, age, gender, family_list, business_list);
        JSONObject json = jsonParser.encodeUserProfileEditor(editedProfileUser, deleted_list, edited_parts);
        user.updateUserProfile(json, new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                if (!result.equals("0")) {
                    try {
                        context.loading_dialog.dismiss();
                        sessionHandler.update_profile_info(context, session, editedProfileUser, new JSONObject(result), edited_parts);
                        context.finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    context.loading_dialog.dismiss();
                    Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void getMyGPosts(final ServerCallback callback) {
        user.setMyGPosts(new ArrayList<Post>());
        post.getUserPosts(user.getId(), new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                callback.onSuccess(result);
            }
        });
    }

    public void getMyADSPosts(final ServerCallback callback) {
        user.setMyAds(new ArrayList<Advertisement>());
        advertisement.getUserPosts(user.getId(), new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                Log.d(TAG, "onADSSuccess: " + result);
                callback.onSuccess(result);
            }
        });
    }

    public void getMySavedGPosts(final ServerCallback callback) {
        user.setSavedGPosts(new ArrayList<Post>());
        post.getUserSavedPosts(user.getId(), new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                callback.onSuccess(result);
            }
        });
    }

    public void getMySavedADSPosts(final ServerCallback callback) {
        user.setSavedAds(new ArrayList<Advertisement>());
        advertisement.getUserSavedPosts(user.getId(), new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                callback.onSuccess(result);
            }
        });
    }

    public String checkMail_Passowrd(String mail, String password) {
        String message = "";
        if (!Patterns.EMAIL_ADDRESS.matcher(mail).matches()) {
            message = "Email is not Valid";
        }
        if (password.length() < 6) {
            if (message.length() == 0) message = "Minmum password length is 6";
            else message += " , Minmum password length is 6";
        }
        return message;
    }
    private android.location.Location getLastKnownLocation(Context context) {
        LocationManager mLocationManager;
        mLocationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        android.location.Location bestLocation = null;
        for (String provider : providers) {
            android.location.Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        context.getApplicationContext().getSharedPreferences("currentUser",Context.MODE_MULTI_PROCESS).edit().putString("currentLat", bestLocation.getLatitude()+"").apply();
        context.getApplicationContext().getSharedPreferences("currentUser",Context.MODE_MULTI_PROCESS).edit().putString("currentLng", bestLocation.getLongitude()+"").apply();
        context.getApplicationContext().getSharedPreferences("currentUser",Context.MODE_MULTI_PROCESS).edit().putString("address", getCompleteAddressString(context , bestLocation.getLatitude(),bestLocation.getLongitude())).apply();
        return bestLocation;
    }

    public String getCompleteAddressString(Context context , double LATITUDE, double LONGITUDE) {
        // this.setContext(ma);
        String strAdd = "";
        StringBuilder strReturnedAddress = new StringBuilder("");
        try {

            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);

            if (addresses != null) {
                Address returnedAddress = addresses.get(0);

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            }

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            //Toast.makeText(this, "Address : " + address, Toast.LENGTH_SHORT).show();
            String city = addresses.get(0).getLocality();
            //Toast.makeText(this, "City : " + city, Toast.LENGTH_SHORT).show();
            String state = addresses.get(0).getAdminArea();
            //Toast.makeText(this, "State : " + state, Toast.LENGTH_SHORT).show();
            String country = addresses.get(0).getCountryName();
            //Toast.makeText(this, "Country : " + country, Toast.LENGTH_SHORT).show();
            //String postalCode = addresses.get(0).getPostalCode();
            //Toast.makeText(this,"Postal Code : " + postalCode,Toast.LENGTH_SHORT).show();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
            //Toast.makeText(this, "Known Name : " + knownName, Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
            //Log.w("My Current location address", "Cannot get Address!");
        }

        return strAdd;
    }
}
