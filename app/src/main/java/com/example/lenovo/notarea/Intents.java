package com.example.lenovo.notarea;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

public class Intents {

    Constants constants = new Constants();

    public void openFacebook(String id, Context context) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://facewebmodal/f?href=" + id));
            context.startActivity(intent);
        } catch (Exception e) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(id));
            context.startActivity(intent);
        }
    }

    public void call(String number, Context context) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + number));
        context.startActivity(intent);
    }


    public void emailTo(String mail, Context context) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + mail));
            intent.putExtra(Intent.EXTRA_SUBJECT, constants.getMAIL_SUBJECT());
            context.startActivity(intent);
        } catch (Exception e) {
            Log.d("Intents", "emailTo: " + e);
        }
    }

}
