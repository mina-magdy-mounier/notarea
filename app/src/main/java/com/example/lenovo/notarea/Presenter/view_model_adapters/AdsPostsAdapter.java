package com.example.lenovo.notarea.Presenter.view_model_adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.notarea.CallBacks.FunctionCallback;
import com.example.lenovo.notarea.Intents;
import com.example.lenovo.notarea.Model.Advertisement;
import com.example.lenovo.notarea.Model.Category;
import com.example.lenovo.notarea.Model.ExtraNumbers;
import com.example.lenovo.notarea.Model.Location;
import com.example.lenovo.notarea.Presenter.JsonParser;
import com.example.lenovo.notarea.Presenter.Maps.GeneralAdvMap;
import com.example.lenovo.notarea.R;
import com.example.lenovo.notarea.View.activity.AdsEditor;
import com.example.lenovo.notarea.View.activity.FormHandler;
import com.example.lenovo.notarea.View.fragments.AdsPageFragment;
import com.example.lenovo.notarea.View.fragments.UserAdsPosts;
import com.example.lenovo.notarea.View.fragments.UserSavedADSPosts;
import com.squareup.picasso.Picasso;

import java.text.Normalizer;
import java.util.ArrayList;


public class AdsPostsAdapter extends ArrayAdapter<Advertisement> {

    private String currentUserID;
    private final int blue_color = Color.rgb(77, 136, 255);
    private final int grey_color = Color.rgb(140, 140, 140);
    private AdapterHandler adapterHandler = new AdapterHandler();
    private AdsPageFragment adsPageFragment;
    private UserSavedADSPosts userSavedADSPosts;
    private UserAdsPosts userAdsPosts;
    private JsonParser jsonParser = new JsonParser();
    private Context context;
    private Intents intents = new Intents();
    private FormHandler formHandler = new FormHandler();


    public void setUserAdsPosts(UserAdsPosts userAdsPosts) {
        this.userAdsPosts = userAdsPosts;
    }

    public void setUserSavedADSPosts(UserSavedADSPosts userSavedADSPosts) {
        this.userSavedADSPosts = userSavedADSPosts;
    }

    public AdsPostsAdapter(Context context, ArrayList<Advertisement> adsPostsBodies, String currentUserID) {
        super(context, 0, adsPostsBodies);
        this.currentUserID = currentUserID;
        this.context = context;
    }

    public void setAdsPageFragment(AdsPageFragment adsPageFragment) {
        this.adsPageFragment = adsPageFragment;
    }

    public void facebookListiner(LinearLayout contactByfacebook, final String url, final Context context) {
        contactByfacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intents.openFacebook(url, context);
            }
        });
    }


    public void phoneListiner(final Spinner spinner, final Context context) {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!spinner.getSelectedItem().toString().equals("Click to Call")) {
                    intents.call(spinner.getSelectedItem().toString(), context);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void locationListiner(LinearLayout contactByLocation, final Location location, final Context context) {
        contactByLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, GeneralAdvMap.class);
                intent.putExtra("LAT", String.valueOf(location.getLatitude()));
                intent.putExtra("LNG", String.valueOf(location.getLongitude()));
                Toast.makeText(context, location.getLongitude() + " " + location.getLatitude() + "", Toast.LENGTH_SHORT).show();

                context.startActivity(intent);
                Toast.makeText(getContext(), "post location ", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listViewItem = convertView;
        if (listViewItem == null) {
            listViewItem = LayoutInflater.from(getContext()).inflate(
                    R.layout.ads_post_body, parent, false);
        }


        final Advertisement post = getItem(position);
        TextView userName = listViewItem.findViewById(R.id.user_name_id);
        TextView userFeeling = listViewItem.findViewById(R.id.user_feeling_id);
        TextView userPostBody = listViewItem.findViewById(R.id.user_post_body_id);
        ImageView userImage = listViewItem.findViewById(R.id.user_image_id);
        TextView dislike = listViewItem.findViewById(R.id.dislike);
        TextView like = listViewItem.findViewById(R.id.like);
        TextView save_post = listViewItem.findViewById(R.id.save_post);
        final TextView setting = listViewItem.findViewById(R.id.general_page_posts_setting);
        ImageView feelingEmoji = listViewItem.findViewById(R.id.emoji);
        ImageView adsImage = listViewItem.findViewById(R.id.adsImg);
        TextView likeCount = listViewItem.findViewById(R.id.likeCount);
        TextView disLikeCount = listViewItem.findViewById(R.id.disLikeCount);
        LinearLayout contactByfacebook = listViewItem.findViewById(R.id.contact_me_by_face);
        LinearLayout contactByLocation = listViewItem.findViewById(R.id.contact_me_by_location);
        LinearLayout contactByPhone = listViewItem.findViewById(R.id.contact_me_by_phone);
        TextView facebook = listViewItem.findViewById(R.id.facebook_url);
        TextView location = listViewItem.findViewById(R.id.location);
        TextView phone = listViewItem.findViewById(R.id.phone);
        final Spinner phoneSpinner = listViewItem.findViewById(R.id.phoneSpinner);
        TextView date = listViewItem.findViewById(R.id.date);
        TextView title = listViewItem.findViewById(R.id.post_title);
        TextView contactMe = listViewItem.findViewById(R.id.contactMe);
        final Dialog category = new Dialog(listViewItem.getContext(), R.style.CustomDialogTheme);
        category.setContentView(R.layout.category);
        final TextView category_title = category.findViewById(R.id.category_title);
        final ListView category_listview = category.findViewById(R.id.category_list_view);
        Button category_ok_btn = category.findViewById(R.id.cok);
        Button category_cancel_btn = category.findViewById(R.id.ccancel);
        category_cancel_btn.setVisibility(View.GONE);
        formHandler.cancel_dialog(category_ok_btn, category);
        category_title.setText("categories");


        userName.setText(post.getUser().getName());

        if (post.getUserFeeling() != null) {
            userFeeling.setText(post.getUserFeeling());
            userFeeling.setVisibility(View.VISIBLE);
        } else {
            userFeeling.setVisibility(View.GONE);
        }

        userPostBody.setText(post.getPostBody());

        if (post.getTitle() == null || post.getTitle().toString().equals("")) {
            title.setVisibility(View.GONE);
        } else {
            title.setText(post.getTitle().toString());
            title.setVisibility(View.VISIBLE);
        }

        if (post.getDate() == null || post.getDate().toString().equals("")) {
            date.setVisibility(View.GONE);
        } else {
            date.setText(post.getDate().toString());
            date.setVisibility(View.VISIBLE);
        }

        if (post.getUser().getProfile_Img() != null) {
            Picasso.get().load(post.getUser().getProfile_Img()).into(userImage);
        } else {
            userImage.setImageResource(R.drawable.nobody);
        }

        if (post.getUserFeelingImg() > 0) {
            feelingEmoji.setBackgroundResource(post.getUserFeelingImg());
            feelingEmoji.setVisibility(View.VISIBLE);
        } else {
            feelingEmoji.setVisibility(View.GONE);
        }

        if (post.getDistance() > 0) {
            Log.d("", "getView: " + String.valueOf(post.getDistance()));
        }

        disLikeCount.setText(String.valueOf(post.getNumberOfDislikes()));
        likeCount.setText(String.valueOf(post.getNumberOfLikes()));

        if (post.getAttachment() != null) {
            contactMe.setVisibility(View.VISIBLE);
            if (post.getAttachment().getFacebook_url() != null) {
                contactByfacebook.setVisibility(View.VISIBLE);
                facebook.setText(post.getAttachment().getFacebook_url());
            } else {
                contactByfacebook.setVisibility(View.GONE);
            }

            if (post.getAttachment().getLocation() != null) {
                contactByLocation.setVisibility(View.VISIBLE);
//            location.setText("user Location");
            } else {
                contactByLocation.setVisibility(View.GONE);
            }


            if (post.getAttachment().getPhoneNumber() != null) {
                ArrayList<ExtraNumbers> phones;
                phones = post.getAttachment().getPhoneNumber();
                ArrayList<String> values = new ArrayList<String>();
                values.add("Click to Call");
                for (int i = 0; i < phones.size(); i++) {
                    values.add(phones.get(i).getNumber());
                }
                contactByPhone.setVisibility(View.VISIBLE);
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(listViewItem.getContext(), android.R.layout.simple_spinner_item, values);
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                phoneSpinner.setAdapter(spinnerArrayAdapter);
            } else {
                contactByPhone.setVisibility(View.GONE);
            }
        } else {
            contactByfacebook.setVisibility(View.GONE);
            contactByPhone.setVisibility(View.GONE);
            contactByLocation.setVisibility(View.GONE);
            contactMe.setVisibility(View.GONE);
        }

        if (post.getAds_Img() != null) {
            if (!post.getAds_Img().equals("null")) {
                adsImage.setVisibility(View.VISIBLE);
                Picasso.get().load(post.getAds_Img()).into(adsImage);
            }
        } else {
            adsImage.setVisibility(View.GONE);
        }

        if (post.isLiked() == 1) {
            like.setText("liked");
            like.setTextColor(blue_color);
        } else {
            like.setText("like");
            like.setTextColor(grey_color);
        }

        if (post.isDisliked() == 1) {
            dislike.setText("disliked");
            dislike.setTextColor(blue_color);
        } else {
            dislike.setText("dislike");
            dislike.setTextColor(grey_color);
        }

        if (post.isSaved() == 1) {
            save_post.setText("saved");
            save_post.setTextColor(blue_color);
        } else {
            save_post.setText("save");
            save_post.setTextColor(grey_color);
        }

        final View finalListViewItem = listViewItem;
        adapterHandler.likePost(like, likeCount, dislike, disLikeCount, post, this.currentUserID);
        adapterHandler.disLikePost(dislike, disLikeCount, like, likeCount, post, this.currentUserID);
        adapterHandler.savePost(save_post, post, this.currentUserID, new FunctionCallback() {
            @Override
            public void onSuccess(String result) {
                if (userSavedADSPosts != null) {
                    userSavedADSPosts.init();
                }
            }

            @Override
            public void onError(String result) {

            }
        });
        if (post.getAttachment() != null) {
            facebookListiner(contactByfacebook, post.getAttachment().getFacebook_url(), finalListViewItem.getContext());
            locationListiner(contactByLocation, post.getLocation(), finalListViewItem.getContext());
            phoneListiner(phoneSpinner, finalListViewItem.getContext());
        }

        final PopupMenu popup = new PopupMenu(getContext(), setting);
        final Menu p;

        popup.getMenuInflater().inflate(R.menu.post_popup_menu, popup.getMenu());
        setting.setVisibility(View.VISIBLE);
        p = popup.getMenu();

        if (post.getUser().getId().equals(currentUserID)) {
            p.findItem(R.id.edit).setVisible(true);
            p.findItem(R.id.delete).setVisible(true);
            p.findItem(R.id.report).setVisible(false);
            p.findItem(R.id.category).setVisible(false);
        } else {
            p.findItem(R.id.edit).setVisible(false);
            p.findItem(R.id.delete).setVisible(false);
            p.findItem(R.id.report).setVisible(true);
            p.findItem(R.id.category).setVisible(true);
        }
        final View finalListViewItem1 = listViewItem;
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.edit:
                        Intent intent = new Intent(getContext(), AdsEditor.class);
                        intent.putExtra("eAds", jsonParser.encodeAdsEditor(post).toString());
                        getContext().startActivity(intent);
                        return true;
                    case R.id.delete:
                        if (adsPageFragment != null) {
                            adsPageFragment.RemoveListViewItems(position, post.getAdsID());
                        }
                        if (userAdsPosts != null) {
                            userAdsPosts.RemoveListViewItems(position, post.getAdsID());
                        }
                        if (userSavedADSPosts != null) {
                            userSavedADSPosts.RemoveListViewItems(position, post.getAdsID());
                        }
                        return true;
                    case R.id.report:
                        if (adsPageFragment != null) {
                            adsPageFragment.report(post.getPostID());
                        }
                        return true;

                    case R.id.category:
                        CategoryAdapter categoryAdapter = new CategoryAdapter(finalListViewItem1.getContext(), post.getCategories());
                        category_listview.setAdapter(categoryAdapter);
                        category.show();
                        return true;
                }
                return true;
            }
        });


        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_setting);
                setting.startAnimation(animation);
                popup.show();
            }
        });


        return listViewItem;
    }
}

