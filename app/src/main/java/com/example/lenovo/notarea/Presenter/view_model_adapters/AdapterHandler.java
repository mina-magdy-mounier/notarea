package com.example.lenovo.notarea.Presenter.view_model_adapters;


import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.lenovo.notarea.CallBacks.FunctionCallback;
import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Model.Post;

public class AdapterHandler {

    String TAG = "AdapterHandler";
    final int blue_color = Color.rgb(77, 136, 255);
    final int grey_color = Color.rgb(140, 140, 140);

    public AdapterHandler() {
    }

    public void likePost(final TextView like, final TextView likesCount, final TextView dislike, final TextView dislikeCount, final Post posts, final String currentUserID) {
        final Post post = new Post();
        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (like.getText().toString().equals("liked")) {
                    post.likeStateChange(currentUserID, posts.getPostID(), new ServerCallback() {
                        @Override
                        public void onSuccess(String result) {
                            Log.d(TAG, "onLikeSuccess: " + result);
                            if (result.equals("1")) {
                                like.setText("like");
                                like.setTextColor(grey_color);
                                posts.setIsLiked(0);
                                likesCount.setText(String.valueOf(posts.getNumberOfLikes() - 1));
                                posts.setNumberOfLikes(posts.getNumberOfLikes() - 1);
                            }
                        }
                    });
                } else {
                    post.likeStateChange(currentUserID, posts.getPostID(), new ServerCallback() {
                        @Override
                        public void onSuccess(String result) {
                            if (result.equals("1")) {
                                like.setText("liked");
                                like.setTextColor(blue_color);
                                posts.setIsLiked(1);
                                likesCount.setText(String.valueOf(posts.getNumberOfLikes() + 1));
                                posts.setNumberOfLikes(posts.getNumberOfLikes() + 1);
                                if (dislike.getText().equals("disliked")) {
                                    dislike.setText("dislike");
                                    dislike.setTextColor(grey_color);
                                    posts.setIsDisliked(0);
                                    dislikeCount.setText(String.valueOf(posts.getNumberOfDislikes() - 1));
                                    posts.setNumberOfDislikes(posts.getNumberOfDislikes() - 1);
                                }

                            }
                        }
                    });
                }
            }
        });
    }

    public void disLikePost(final TextView dislike, final TextView dislikesCount, final TextView like, final TextView likeCount, final Post posts, final String currentUserID) {
        final Post post = new Post();
        dislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dislike.getText().toString().equals("disliked")) {
                    post.disLikeStateChange(currentUserID, posts.getPostID(), new ServerCallback() {
                        @Override
                        public void onSuccess(String result) {
                            Log.d(TAG, "ondisLikeSuccess: " + result);
                            if (result.equals("1")) {
                                dislike.setText("dislike");
                                dislike.setTextColor(grey_color);
                                posts.setIsDisliked(0);
                                dislikesCount.setText(String.valueOf(posts.getNumberOfDislikes() - 1));
                                posts.setNumberOfDislikes(posts.getNumberOfDislikes() - 1);

                            }
                        }
                    });
                } else {
                    post.disLikeStateChange(currentUserID, posts.getPostID(), new ServerCallback() {
                        @Override
                        public void onSuccess(String result) {
                            if (result.equals("1")) {
                                dislike.setText("disliked");
                                dislike.setTextColor(blue_color);
                                posts.setIsDisliked(1);
                                dislikesCount.setText(String.valueOf(posts.getNumberOfDislikes() + 1));
                                posts.setNumberOfDislikes(posts.getNumberOfDislikes() + 1);
                                if (like.getText().toString().equals("liked")) {
                                    like.setText("like");
                                    like.setTextColor(grey_color);
                                    posts.setIsLiked(0);
                                    likeCount.setText(String.valueOf(posts.getNumberOfLikes() - 1));
                                    posts.setNumberOfLikes(posts.getNumberOfLikes() - 1);
                                }
                            }
                        }
                    });
                }
            }
        });
    }

    public void savePost(final TextView textView, final Post posts, final String currentUserID, final FunctionCallback callback) {
        final Post post = new Post();
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textView.getText().toString().equals("saved")) {
                    post.savePostStateChange(currentUserID, posts.getPostID(), new ServerCallback() {
                        @Override
                        public void onSuccess(String result) {
                            if (result.equals("\"Unsaved\"")) {
                                textView.setText("save");
                                callback.onSuccess("1");
                                textView.setTextColor(grey_color);
                                posts.setIsSaved(0);
                            }
                        }
                    });
                } else {
                    post.savePostStateChange(currentUserID, posts.getPostID(), new ServerCallback() {
                        @Override
                        public void onSuccess(String result) {
                            if (result.equals("\"1\"")) {
                                textView.setText("saved");
                                textView.setTextColor(blue_color);
                                posts.setIsSaved(1);
                            }
                        }
                    });
                }
            }
        });
    }
}
