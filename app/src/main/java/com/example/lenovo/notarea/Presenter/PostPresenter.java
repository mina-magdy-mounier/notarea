package com.example.lenovo.notarea.Presenter;


import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.lenovo.notarea.CallBacks.FunctionCallback;
import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Model.Attachment;
import com.example.lenovo.notarea.Model.Location;
import com.example.lenovo.notarea.Model.Post;
import com.example.lenovo.notarea.Model.User;
import com.example.lenovo.notarea.View.activity.GPostEditor;
import com.example.lenovo.notarea.View.activity.GeneralPostForm;
import com.example.lenovo.notarea.View.activity.MainActivity;
import com.example.lenovo.notarea.View.fragments.GeneralPageFragemnt;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.content.Context.LOCATION_SERVICE;

public class PostPresenter {

    final String TAG = "PostPresenter";
    User user = new User();
    JsonParser jsonParser = new JsonParser();
    private Post post = new Post();
    private ArrayList<Post> posts = new ArrayList<>();
    private GeneralPageFragemnt generalPageFragemnt;
    private MainActivity mainActivity;
    private SessionHandler sessionHandler = new SessionHandler();

    public void setGeneralPageFragemnt(GeneralPageFragemnt generalPageFragemnt) {
        this.generalPageFragemnt = generalPageFragemnt;
    }

    public void refreshPostsArray() {
        this.posts = new ArrayList<>();
        init();
    }

    public Post getPost() {
        return post;
    }

    public User getUser() {
        return user;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public ArrayList<Post> getPosts() {
        return posts;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public MainActivity getMainActivity() {
        return mainActivity;
    }

    public PostPresenter() {
    }

    public PostPresenter(SharedPreferences session) {
        sessionHandler.getSession(session, this.user);
    }

    public boolean allDone(String b[]) {
        for (int i = 0; i < b.length; i++) {
            if (b[i].equals("load")) {
                return false;
            }
        }
        return true;
    }

    public void init() {
        mainActivity.main_progress_bar.setVisibility(View.VISIBLE);
        mainActivity.getAllIsDone()[0] = "load";
        Location location = new Location();
        if (mainActivity.getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLat", "").equals("")) {
            android.location.Location lastKnownLocation = getLastKnownLocation(mainActivity.getApplicationContext());
            location = new Location(lastKnownLocation.getLongitude(), lastKnownLocation.getLatitude());
        } else {
            location = new Location(Double.parseDouble(mainActivity.getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLng", "")), Double.parseDouble(mainActivity.getApplicationContext().getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLat", "")));
        }
        JSONObject jsonObject = jsonParser.encodePostsNearest(location, this.user.getId());
        post.getPosts(jsonObject, new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                if (!result.equals("error") && !result.equals("php_error")) {
                    JsonParser jp = new JsonParser();
                    posts = new ArrayList<>();
                    posts = jp.decodeNearestGPost(result);
                    mainActivity.getAllIsDone()[0] = "done";
                    generalPageFragemnt.generalListView.setVisibility(View.VISIBLE);
                    generalPageFragemnt.no_general.setVisibility(View.GONE);
                    generalPageFragemnt.postsReady();
                } else if (result.equals("error")) {
                    mainActivity.getAllIsDone()[0] = "error";
                    generalPageFragemnt.no_general.setVisibility(View.VISIBLE);
                    generalPageFragemnt.generalListView.setVisibility(View.GONE);
                } else {
                    mainActivity.getAllIsDone()[0] = "error";

                    Snackbar.make(generalPageFragemnt.generalListView, "connection error", Snackbar.LENGTH_SHORT).show();
                }
                if (allDone(mainActivity.getAllIsDone())) {
                    mainActivity.main_progress_bar.setVisibility(View.GONE);
                }
            }
        });
    }

    public void initEditor(Bundle bundle) {
        this.post = jsonParser.decodeNearestGPost(bundle.get("ePost").toString()).get(0);
    }

    public Attachment addAttachment(String FaceBook, boolean addLocation, Location location, ArrayList<String> phones) {
        if (FaceBook != null || addLocation || phones != null) {
            Attachment attachment = new Attachment();
            if (FaceBook != null) {
                attachment.setFacebook_url(FaceBook);
            } else {
                attachment.setFacebook_url(null);
            }
            if (addLocation) {
                attachment.setLocation(location);
            } else {
                attachment.setLocation(null);
            }
            if (phones != null) {
                attachment.setPhoneNumbersIDs(phones);
            } else {
                attachment.setPhoneNumbersIDs(null);
            }
            return attachment;
        } else {
            return null;
        }
    }

    public Boolean addPost(final GeneralPostForm GeneralPostFormContext, String title, String postBody, String userFeeling, int userFeelingImg, String FaceBook, ArrayList<String> phones, boolean addLocation, String shared_distance) {
        try {
            GeneralPostFormContext.adding_in_progress.setVisibility(View.VISIBLE);
            if (phones.size() == 0) phones = null;
            if (title.equals("")) title = null;
            Location location = new Location();
            if (GeneralPostFormContext.getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLng", "").equals("")) {
                android.location.Location lastKnownLocation = getLastKnownLocation(mainActivity.getApplicationContext());
                location = new Location(lastKnownLocation.getLongitude(), lastKnownLocation.getLatitude());
            } else {
                location = new Location(Double.parseDouble(GeneralPostFormContext.getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLng", "")), Double.parseDouble(GeneralPostFormContext.getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLat", "")));
            }
            Attachment attachment = addAttachment(FaceBook, addLocation, location, phones);
            final Post post = new Post(title, postBody, userFeeling, userFeelingImg, 0, 0, 0, 0, 0, attachment, user.getId(), user, location, Integer.parseInt(shared_distance));
            post.setContext(GeneralPostFormContext);
            JSONObject postJson = jsonParser.encodeGPost(post);
            post.addPost(postJson, this.user.getId(), new ServerCallback() {
                @Override
                public void onSuccess(String result) {
                    if (result != null) {
                        GeneralPostFormContext.adding_in_progress.setVisibility(View.GONE);
                        GeneralPostFormContext.finish();
                    } else {
                        GeneralPostFormContext.adding_in_progress.setVisibility(View.GONE);
                        Toast.makeText(GeneralPostFormContext.getApplicationContext(), "Failure in adding Post", Toast.LENGTH_LONG).show();
                    }
                }
            });
            return true;
        } catch (Exception e) {
            Log.d(TAG, e.toString());
            return false;
        }
    }

    // title-0 , body-1 , face-2 , numbers-3 , location-4 , feelings-5
    public void editPost(final GPostEditor gPostEditor, String postID, String title, String postBody, String userFeeling, int userFeelingImg, String FaceBook, ArrayList<String> phones, boolean addLocation, String distance, Boolean editedParts[]) {
        final String DELETED_CODE = "";
        try {
            Attachment attachment = new Attachment();
            if (editedParts[2] || editedParts[3] || editedParts[4]) {
                attachment = addAttachment(FaceBook, addLocation, null, phones);
            } else {
                attachment = null;
            }

            if (!editedParts[0]) {
                title = null;
            } else {
                if (title.equals("")) {
                    title = DELETED_CODE;
                }
            }
            if (!editedParts[1]) {
                postBody = null;
            } else {
                if (postBody.equals("")) {
                    postBody = DELETED_CODE;
                }
            }

            Location location = new Location(0, 0);
            final Post post = new Post(title, postBody, userFeeling, userFeelingImg, 0, 0, 0, 0, 0, attachment, user.getId(), user, location, Integer.parseInt(distance));
            post.setContext(gPostEditor);
            JSONObject postJson = jsonParser.encodeEditedGPost(post, editedParts);
            post.editPost(user.getId(), postID, postJson, new ServerCallback() {
                @Override
                public void onSuccess(String result) {
                    gPostEditor.adding_in_progress.setVisibility(View.GONE);
                    gPostEditor.finish();
                }
            });

        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
    }

    public void removePost(int position, String postID, final ServerCallback callback) {
        post.deletePost(user.getId(), postID, new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                callback.onSuccess(result);
            }
        });
    }

    public void report(final String report, String post_id, final FunctionCallback callback) {
        JSONObject report_json = jsonParser.encodeReport(this.user.getId(), post_id, report);
        post.report(report_json, new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                if (!result.equals("error")) {
                    callback.onSuccess("done");
                } else {
                    callback.onError(result);

                }
            }
        });
    }

    private android.location.Location getLastKnownLocation(Context context) {
        LocationManager mLocationManager;
        mLocationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        android.location.Location bestLocation = null;
        for (String provider : providers) {
            android.location.Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        context.getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).edit().putString("currentLat", bestLocation.getLatitude() + "").apply();
        context.getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).edit().putString("currentLng", bestLocation.getLongitude() + "").apply();
        context.getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).edit().putString("address", getCompleteAddressString(context, bestLocation.getLatitude(), bestLocation.getLongitude())).apply();
        return bestLocation;
    }

    public String getCompleteAddressString(Context context, double LATITUDE, double LONGITUDE) {
        // this.setContext(ma);
        String strAdd = "";
        StringBuilder strReturnedAddress = new StringBuilder("");
        try {

            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);

            if (addresses != null) {
                Address returnedAddress = addresses.get(0);

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            }

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            //Toast.makeText(this, "Address : " + address, Toast.LENGTH_SHORT).show();
            String city = addresses.get(0).getLocality();
            //Toast.makeText(this, "City : " + city, Toast.LENGTH_SHORT).show();
            String state = addresses.get(0).getAdminArea();
            //Toast.makeText(this, "State : " + state, Toast.LENGTH_SHORT).show();
            String country = addresses.get(0).getCountryName();
            //Toast.makeText(this, "Country : " + country, Toast.LENGTH_SHORT).show();
            //String postalCode = addresses.get(0).getPostalCode();
            //Toast.makeText(this,"Postal Code : " + postalCode,Toast.LENGTH_SHORT).show();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
            //Toast.makeText(this, "Known Name : " + knownName, Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
            //Log.w("My Current location address", "Cannot get Address!");
        }

        return strAdd;
    }
}

