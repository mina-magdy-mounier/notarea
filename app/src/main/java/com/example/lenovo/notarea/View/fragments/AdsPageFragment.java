package com.example.lenovo.notarea.View.fragments;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.lenovo.notarea.CallBacks.FunctionCallback;
import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Presenter.AdsPresenter;
import com.example.lenovo.notarea.Presenter.JsonParser;
import com.example.lenovo.notarea.R;
import com.example.lenovo.notarea.Presenter.view_model_adapters.AdsPostsAdapter;
import com.example.lenovo.notarea.View.activity.MainActivity;

public class AdsPageFragment extends Fragment {

    public ListView adsListView;
    public LinearLayout no_ads_normal, no_ads_premium;
    static AdsPresenter adsPresenter;
    static AdsPostsAdapter adsPostsAdapter;
    SharedPreferences session;
    MainActivity mainActivity;
    ProgressBar report_progress;
    JsonParser jsonParser = new JsonParser();
    Dialog reportDialog;
    EditText eReport;
    Button dSubmit, dCancel;
    String reportBody = null;
    String postID;


    public AdsPageFragment() {
        // Required empty public constructor
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View ads = inflater.inflate(R.layout.ads_page_fragment, container, false);
        adsListView = ads.findViewById(R.id.ads_list_view);
        setHasOptionsMenu(true);
        session = ads.getContext().getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, new IntentFilter("AdsPageFragment"));
        reportDialog = new Dialog(getActivity(), R.style.CustomDialogTheme);
        reportDialog.setContentView(R.layout.report);
        eReport = reportDialog.findViewById(R.id.report);
        dSubmit = reportDialog.findViewById(R.id.report_submit);
        dCancel = reportDialog.findViewById(R.id.report_cancel);
        report_progress = reportDialog.findViewById(R.id.report_progress);
        no_ads_normal = ads.findViewById(R.id.no_ads_normal);
        no_ads_premium = ads.findViewById(R.id.no_ads_premium);
        report_progress.setVisibility(View.GONE);
        submitReport();
        cancelReport();
        adsPresenter = new AdsPresenter(session);
        adsPresenter.setMainActivity(mainActivity);
        adsPresenter.getAdvertisement().setContext(getContext());
        init();

        return ads;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    public void init() {
        mainActivity.main_progress_bar.setVisibility(View.VISIBLE);
        adsPresenter.initAds(AdsPageFragment.this, new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                if (result != null) {
                    adsPresenter.setAdvertisements(jsonParser.decodeNearestAdsPost(result));
                }
                adsPostsAdapter = new AdsPostsAdapter(getContext(), adsPresenter.getAdvertisements(), adsPresenter.getUser().getId());
                adsPostsAdapter.setAdsPageFragment(AdsPageFragment.this);
                adsListView.setAdapter(adsPostsAdapter);
            }
        });
    }

    public void RemoveListViewItems(final int postion, final String postID) {
        adsPresenter.removePost(postion, postID, new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                if (result.equals("\"Advertisment Deleted\"")) {
                    adsPresenter.getAdvertisements().remove(postion);
                    adsPostsAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.refresh, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menuRefresh) {
            init();
        }
        return super.onOptionsItemSelected(item);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String postJson = intent.getStringExtra("json");

            if (adsPostsAdapter != null) {
                if (adsPresenter.getAdvertisements() != null) {
                    adsPresenter.getAdvertisements().add(0, jsonParser.decodeAdsNotification(postJson));
                    adsPostsAdapter.notifyDataSetChanged();
                }
            } else {
                init();
            }
        }
    };

    public void submitReport() {
        dSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                report_progress.setVisibility(View.VISIBLE);
                if (!eReport.getText().toString().equals("")) {
                    reportBody = eReport.getText().toString();
                    adsPresenter.report(reportBody, postID, new FunctionCallback() {
                        @Override
                        public void onSuccess(String result) {
                            if (result.equals("done")) {
                                reportDialog.dismiss();
                                eReport.setText("");
                                reportBody = null;
                                report_progress.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "your report submitted succsefully", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onError(String result) {
                            Toast.makeText(getActivity(), result, Toast.LENGTH_LONG).show();
                            report_progress.setVisibility(View.GONE);
                        }
                    });
                } else {
                    Toast.makeText(getActivity(), "report can't be empty", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void cancelReport() {
        dCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reportDialog.dismiss();
                eReport.setText("");
                reportBody = null;
            }
        });
    }

    public void report(String postID) {
        this.postID = postID;
        eReport.setText("");
        reportDialog.show();
    }
}
