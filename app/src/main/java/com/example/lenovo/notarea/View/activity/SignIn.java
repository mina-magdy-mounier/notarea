package com.example.lenovo.notarea.View.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Model.Location;
import com.example.lenovo.notarea.Presenter.SessionHandler;
import com.example.lenovo.notarea.Presenter.UserPreseneter;
import com.example.lenovo.notarea.Presenter.Validation.UserValidation;
import com.example.lenovo.notarea.R;

public class SignIn extends AppCompatActivity {

    Button signIn;
    LinearLayout loginInputs, LRemeberMe;
    EditText email, password;
    TextView signUp, forgetPassword;
    CheckBox cRememberMe;
    public ProgressBar signInProgressBar;
    Boolean isCollapsed = true;
    Boolean validUser = false, validPassword = false;
    UserPreseneter userPreseneter;
    SharedPreferences session;
    UserValidation validation = new UserValidation();
    LinearLayout logo, logo_bar;
    SessionHandler sessionHandler = new SessionHandler();
    //loading
    public Dialog loading_dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getSharedPreferences("currentUser", Context.MODE_PRIVATE).getString("remeberMe", "no").equals("yes")) {
            Intent intent = new Intent(SignIn.this, MainActivity.class);
            startActivity(intent);
        } else {
            session = getSharedPreferences("currentUser", Context.MODE_PRIVATE);
            if (sessionHandler.get_theme(session) != R.style.AppTheme) {
                setTheme(sessionHandler.get_theme(session));
            }
            setContentView(R.layout.sign_in);
            userPreseneter = new UserPreseneter();
            userPreseneter.setSession(session);
            signIn = findViewById(R.id.sign_in_button);
            loginInputs = findViewById(R.id.login_inputs);
            email = findViewById(R.id.email);
            password = findViewById(R.id.password);
            signInProgressBar = findViewById(R.id.sign_in_progressbar);
            signInProgressBar.setVisibility(View.GONE);
            signUp = findViewById(R.id.signUp);
            forgetPassword = findViewById(R.id.forgetPassword);
            LRemeberMe = findViewById(R.id.remeberMELayout);
            cRememberMe = findViewById(R.id.remeberMECheckbox);
            FormHandler.collapse(loginInputs);
            cRememberMe.setClickable(false);
            signInProgressBar.setVisibility(View.GONE);
            logo = findViewById(R.id.logo);
            logo_bar = findViewById(R.id.logo_bar);
            loading_dialog = new Dialog(this, R.style.CustomDialogTheme);
            loading_dialog.setContentView(R.layout.loading);
            loading_dialog.setCanceledOnTouchOutside(false);

            signInButtonListener();
            emailInputListener();
            passwordInputListener();
            setSignUpListener();
            forgetPasswordListener();
            logo.startAnimation(inFromLeftAnimation());


            LRemeberMe.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!cRememberMe.isChecked()) {
                        cRememberMe.setChecked(true);
                    } else {
                        cRememberMe.setChecked(false);
                    }
                }
            });
        }

    }


    public void emailInputListener() {
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                email.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String error = validation.validateUserEmail(email.getText().toString());
                if (error != null) {
                    email.setError(error);
                    validUser = false;
                    colorSignInButton(false, false);
                } else {
                    validUser = true;
                    email.setError(null);
                    colorSignInButton(true, validPassword);
                }
            }
        });
    }

    public void passwordInputListener() {
        password.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                password.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String error = validation.validateUserPassword(password.getText().toString());
                if (error != null) {
                    password.setError(error);
                    colorSignInButton(false, false);
                    validPassword = false;
                } else {
                    password.setError(null);
                    colorSignInButton(true, validUser);
                    validPassword = true;
                }
            }
        });
    }

    private Animation inFromLeftAnimation() {
        Animation inFromLeft = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromLeft.setDuration(700);
        inFromLeft.setInterpolator(new AccelerateInterpolator());
        return inFromLeft;
    }

    public void colorSignInButton(Boolean x, Boolean y) {
        if (x && y) {
            signIn.setBackgroundColor(Color.rgb(77, 136, 255));
        } else {
            signIn.setBackgroundResource(R.drawable.blue_border);
        }
    }

    public void signInButtonListener() {
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validUser && validPassword) {
                    loading_dialog.show();
                    userPreseneter.signIn(email.getText().toString(), password.getText().toString(), SignIn.this, cRememberMe, new ServerCallback() {
                        @Override
                        public void onSuccess(String result) {
                            if (result.equals("Sign in Success")) {
                                finish();
                                loading_dialog.dismiss();
                                Intent intent = new Intent(getApplication(), MainActivity.class);
                                startActivity(intent);
                            } else if (result.equals("Sign in Failed")) {
                                Toast.makeText(getApplicationContext(), "in-valid email or password", Toast.LENGTH_LONG).show();
                                loading_dialog.dismiss();
                            }
                        }
                    });

                } else {
                    if (!isCollapsed) {
                        FormHandler.collapse(loginInputs);
                        isCollapsed = true;
                    } else {
                        FormHandler.expand(loginInputs);
                        isCollapsed = false;
                    }
                }
            }
        });
    }

    public void setSignUpListener() {
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignIn.this, SignUp.class);
                startActivity(intent);
            }
        });
    }

    public void forgetPasswordListener() {
        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(SignIn.this, "Forget Passsword", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
