package com.example.lenovo.notarea.Presenter;


import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.lenovo.notarea.CallBacks.FunctionCallback;
import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Model.Emergency;
import com.example.lenovo.notarea.Model.EmergencyNumber;
import com.example.lenovo.notarea.Model.Location;
import com.example.lenovo.notarea.Model.User;
import com.example.lenovo.notarea.Presenter.Maps.MapsActivity;
import com.example.lenovo.notarea.R;
import com.example.lenovo.notarea.View.activity.EmergencyActivate;
import com.example.lenovo.notarea.View.activity.MainActivity;
import com.example.lenovo.notarea.View.fragments.EmergencyPageFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.prefs.PreferenceChangeEvent;

import static android.content.Context.LOCATION_SERVICE;

public class EmergencyPresenter extends PostPresenter {

    static String TAG = "EmergencyPresenter";
    User user = new User();
    JsonParser jsonParser = new JsonParser();
    private Emergency emergency = new Emergency();
    private static ArrayList<Emergency> Emergencies;
    private EmergencyActivate emergencyActivate;
    private MainActivity mainActivity;
    private EmergencyPageFragment emergencyPageFragment;
    SharedPreferences session;
    SessionHandler sessionHandler = new SessionHandler();
    //    ArrayList<String> phoneNo = new ArrayList<>();
    String message;
    ArrayList<EmergencyNumber> emerNums = new ArrayList<EmergencyNumber>();

    public EmergencyPresenter() {
    }

    public MainActivity getMainActivity() {
        return mainActivity;
    }

    public EmergencyPageFragment getEmergencyPageFragment() {
        return emergencyPageFragment;
    }

    public void setEmergencyPageFragment(EmergencyPageFragment emergencyPageFragment) {
        this.emergencyPageFragment = emergencyPageFragment;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public EmergencyPresenter(SharedPreferences session) {
        sessionHandler.getSession(session, this.user);
    }

    /////////////////////////////////Seter and getter/////////////////////////////////////
    public static String getTAG() {
        return TAG;
    }

    public static void setTAG(String TAG) {
        EmergencyPresenter.TAG = TAG;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public JsonParser getJsonParser() {
        return jsonParser;
    }

    public void setJsonParser(JsonParser jsonParser) {
        this.jsonParser = jsonParser;
    }

    public Emergency getEmergency() {
        return emergency;
    }

    public void setEmergency(Emergency emergency) {
        this.emergency = emergency;
    }

    public ArrayList<Emergency> getEmergencies() {
        return Emergencies;
    }

    public void setEmergencies(ArrayList<Emergency> emergencies) {
        Emergencies = emergencies;
    }

    public EmergencyActivate getEmergencyActivate() {
        return emergencyActivate;
    }

    public void setEmergencyActivate(EmergencyActivate emergencyActivate) {
        this.emergencyActivate = emergencyActivate;
    }

    public SharedPreferences getSession() {
        return session;
    }

    public void setSession(SharedPreferences session) {
        this.session = session;
    }
/////////////////////////////////////////////////////////////////////////////////////////

    public Boolean addEmergency(final MainActivity mainActivity, boolean addLocation, String countryid) {
        {
            sendSMSMessage(mainActivity);
            mainActivity.fab.setEnabled(false);
            mainActivity.main_progress_bar.setVisibility(View.VISIBLE);
            Location location = new Location();
            if (mainActivity.getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLng", "").equals("")) {
                android.location.Location lastKnownLocation = getLastKnownLocation(mainActivity.getApplicationContext());
                location = new Location(lastKnownLocation.getLongitude(), lastKnownLocation.getLatitude());
            } else {
                location = new Location(Double.parseDouble(mainActivity.getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLng", "")), Double.parseDouble(mainActivity.getApplicationContext().getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLat", "")));
            }
            final Emergency emergency = new Emergency(user, location, countryid);
            emergency.setContext(mainActivity);
            emergency.addEmergency(emergency, new ServerCallback() {
                @Override
                public void onSuccess(String result) {
                    if (result != null) {
                        mainActivity.fab.setEnabled(true);
                        mainActivity.main_progress_bar.setVisibility(View.INVISIBLE);
                        mainActivity.fab.setImageResource(R.drawable.ic_close_white_24dp);
                        Toast.makeText(mainActivity, "Emergency added", Toast.LENGTH_LONG).show();
                        mainActivity.getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).edit().putString("victim", "true").apply();
                        Log.d(TAG, "addEmergency: " + result);
                        JSONObject addedEmergency = null;
                        try {
                            addedEmergency = new JSONObject(result);
                            mainActivity.getSharedPreferences("currentUser", Context.MODE_PRIVATE).edit().putString("emergencyID", String.valueOf(addedEmergency.optInt("id"))).apply();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //mainActivity.getSharedPreferences("currentUser",Context.MODE_MULTI_PROCESS).edit().putString("victim", "true").apply();
                    } else {
                        mainActivity.fab.setEnabled(true);
                        mainActivity.main_progress_bar.setVisibility(View.INVISIBLE);
                        Toast.makeText(mainActivity, "Failure in adding Emergency", Toast.LENGTH_LONG).show();
                    }
                }
            });
            String victim = mainActivity.getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("victim", "");
            return true;
        }
    }

    public void emergencyRefresh() {
        this.Emergencies = new ArrayList<>();
        init();
    }

    public Boolean removeEmergency(final MainActivity mainActivity, final FunctionCallback functionCallback) {
        emergency.setContext(mainActivity);
        emergency.removeEmergency(this.user.getId(), new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                if (result != null) {
                    Log.d(TAG, "onSuccess Emergency respond: " + result);
                    functionCallback.onSuccess("removed");
                } else {
                    functionCallback.onSuccess("not_removed");
                    Toast.makeText(mainActivity, "Failure in removing Emergency", Toast.LENGTH_LONG).show();
                }
            }
        });
        return true;
    }


    public void init() {
        mainActivity.main_progress_bar.setVisibility(View.VISIBLE);
        String countryCode = mainActivity.getApplicationContext().getSharedPreferences("currentUser", Context.MODE_PRIVATE).getString("countryCode", "");
        Location location = new Location();
        if (mainActivity.getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLng", "").equals("")) {
            android.location.Location lastKnownLocation = getLastKnownLocation(mainActivity.getApplicationContext());
            location = new Location(lastKnownLocation.getLongitude(), lastKnownLocation.getLatitude());
        } else {
            location = new Location(Double.parseDouble(mainActivity.getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLng", "")), Double.parseDouble(mainActivity.getApplicationContext().getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLat", "")));
        }
        getMainActivity().getAllIsDone()[2] = "load";
        emergency.getEmergencies(this.user.getId(), countryCode, location, new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                JsonParser jp = new JsonParser();
                if (result != null) {
                    if (!result.equals("not_found") && !result.equals("php_error")) {
                        getMainActivity().getAllIsDone()[2] = "done";
                        Emergencies = jp.decodeEmergency(result);
                        emergencyPageFragment.emergenciesReady();
                        emergencyPageFragment.no_emergency_normal.setVisibility(View.GONE);
                        emergencyPageFragment.emergencyViewList.setVisibility(View.VISIBLE);
                    } else if (result.equals("not_found")) {
                        getMainActivity().getAllIsDone()[2] = "error";
                        emergencyPageFragment.no_emergency_normal.setVisibility(View.VISIBLE);
                        emergencyPageFragment.emergencyViewList.setVisibility(View.GONE);
                    } else if (result.equals("php_error")) {
                        getMainActivity().getAllIsDone()[2] = "error";
                        emergencyPageFragment.emergencyViewList.setVisibility(View.GONE);
                        emergencyPageFragment.no_emergency_normal.setVisibility(View.GONE);
                        Snackbar.make(emergencyPageFragment.emergencyViewList, "connection error", Snackbar.LENGTH_SHORT).show();
                    }

                }
                if (allDone(mainActivity.getAllIsDone())) {
                    mainActivity.main_progress_bar.setVisibility(View.GONE);
                }

            }
        });
    }

    protected void sendSMSMessage(final MainActivity mainActivity) {
        Location location = new Location();
        if (mainActivity.getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLng", "").equals("")) {
            android.location.Location lastKnownLocation = getLastKnownLocation(mainActivity.getApplicationContext());
            location = new Location(lastKnownLocation.getLongitude(), lastKnownLocation.getLatitude());
        } else {
            location = new Location(Double.parseDouble(mainActivity.getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLng", "")), Double.parseDouble(mainActivity.getApplicationContext().getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLat", "")));
        }
        message = "HELP Hazem Fouda PLEASE!\n Current Location: https://www.google.com/maps/?q=" + String.valueOf(location.getLatitude()) + "," + String.valueOf(location.getLongitude()) + "\nLast Online Address: " + mainActivity.getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("address", "") + "\nBy Notarea";
        SmsManager smsManager = SmsManager.getDefault();
        ArrayList<String> messageParts = SmsManager.getDefault().divideMessage(message);
//        SmsManager.getDefault().sendMultipartTextMessage(phoneNo.get(0), null, messageParts, null, null);
//        for (int i = 0; i < phoneNo.size(); i++)
//           smsManager.sendTextMessage(user.getFamilyNumbers().get(i).getNumber(), null, message, null, null);
    }

    public Boolean rateEmergency(final MapsActivity mapsActivity, String emergencyID, float rate) {
        {
            final Emergency emergency = new Emergency();
            emergency.setContext(mapsActivity);
            emergency.setRate(rate);
            emergency.setId(emergencyID);
            emergency.setUser(user);
            Toast.makeText(mapsActivity, "Enter to add rate from presenter", Toast.LENGTH_LONG).show();
            emergency.rateEmergency(emergency, new ServerCallback() {
                @Override
                public void onSuccess(String result) {
                    if (result != null) {
                        Toast.makeText(mapsActivity, "Rate", Toast.LENGTH_LONG).show();
                        Log.d(TAG, "onSuccess Rate response: " + result);
                    } else {
                        Log.d(TAG, "Failure in rating Emergency");
                    }
                }
            });
            return true;
        }
    }

    public ArrayList<EmergencyNumber> getEmergencyNumbers(final MapsActivity mapsActivity) {
        String countryCode = mapsActivity.getApplicationContext().getSharedPreferences("currentUser", Context.MODE_PRIVATE).getString("countryCode", "");
        emergency.setContext(mapsActivity);
        emergency.setCountryId(countryCode);
        emergency.getEmerNums(emergency, new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                if (result != null) {
                    emerNums = jsonParser.decodeEmergencyNumbers(result);
                    Toast.makeText(mapsActivity, "Emergency Numbers", Toast.LENGTH_LONG).show();
                    Log.d(TAG, "onSuccess Emergency Numbers response: " + result);
                } else {
                    Toast.makeText(mapsActivity, "Failure in Emergency Numbers", Toast.LENGTH_LONG).show();
                }
            }
        });
        return emerNums;
    }

    private android.location.Location getLastKnownLocation(Context context) {
        LocationManager mLocationManager;
        mLocationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        android.location.Location bestLocation = null;
        for (String provider : providers) {
            android.location.Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        context.getApplicationContext().getSharedPreferences("currentUser",Context.MODE_MULTI_PROCESS).edit().putString("currentLat", bestLocation.getLatitude()+"").apply();
        context.getApplicationContext().getSharedPreferences("currentUser",Context.MODE_MULTI_PROCESS).edit().putString("currentLng", bestLocation.getLongitude()+"").apply();
        context.getApplicationContext().getSharedPreferences("currentUser",Context.MODE_MULTI_PROCESS).edit().putString("address", getCompleteAddressString(context , bestLocation.getLatitude(),bestLocation.getLongitude())).apply();
        return bestLocation;
    }

    public String getCompleteAddressString(Context context , double LATITUDE, double LONGITUDE) {
        // this.setContext(ma);
        String strAdd = "";
        StringBuilder strReturnedAddress = new StringBuilder("");
        try {

            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);

            if (addresses != null) {
                Address returnedAddress = addresses.get(0);

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            }

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            //Toast.makeText(this, "Address : " + address, Toast.LENGTH_SHORT).show();
            String city = addresses.get(0).getLocality();
            //Toast.makeText(this, "City : " + city, Toast.LENGTH_SHORT).show();
            String state = addresses.get(0).getAdminArea();
            //Toast.makeText(this, "State : " + state, Toast.LENGTH_SHORT).show();
            String country = addresses.get(0).getCountryName();
            //Toast.makeText(this, "Country : " + country, Toast.LENGTH_SHORT).show();
            //String postalCode = addresses.get(0).getPostalCode();
            //Toast.makeText(this,"Postal Code : " + postalCode,Toast.LENGTH_SHORT).show();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
            //Toast.makeText(this, "Known Name : " + knownName, Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
            //Log.w("My Current location address", "Cannot get Address!");
        }

        return strAdd;
    }
}

