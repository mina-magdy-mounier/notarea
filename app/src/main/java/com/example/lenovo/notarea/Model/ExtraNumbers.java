package com.example.lenovo.notarea.Model;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Constants;

import org.json.JSONObject;

public class ExtraNumbers {

    final String TAG = "ExtraNumbers";
    private String id;
    private String name;
    private String number;
    private String type;
    private Bitmap image;
    private Constants constants = new Constants();
    private String URL = constants.getURL();
    private Context context;

    public ExtraNumbers() {
    }

    public ExtraNumbers(String name, String number, String type) {
        this.name = name;
        this.number = number;
        this.type = type;
    }

    public ExtraNumbers(String id, String name, String number, String type) {
        this.id = id;
        this.name = name;
        this.number = number;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void getEmergencyNumbers(String country_code, final ServerCallback callback) {
        try {
            String url = URL + "api/notarea/emergencynumbers/" + country_code;
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                    (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject responsee) {
                            Log.d(TAG, "getEmergencyNumbers response: " + responsee.toString());
                            callback.onSuccess(responsee.toString());
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            NetworkResponse response = error.networkResponse;
                            if (response != null && response.data != null) {
                                switch (response.statusCode) {
                                    case 404:
                                        callback.onSuccess("error");
                                        break;
                                    case 500:
                                        callback.onSuccess("php_error");
                                        break;
                                }
                            }
                        }
                    });
            Singleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
    }
}
