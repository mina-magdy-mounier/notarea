package com.example.lenovo.notarea.Model;

import android.content.Context;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Presenter.JsonParser;

import org.json.JSONObject;

import java.util.ArrayList;

public class Emergency extends Post {

    final private String TAG = "Emergency";
    private double rate;
    private String countryId;
    private boolean active;
    private ArrayList<EmergencyNumber> emeNumbers = new ArrayList<EmergencyNumber>();
    private Context context;
    private String id;

    public Emergency() {
    }

    public Emergency(User user, Location location, String countryid) {
        this.setUser(user);
        this.setLocation(location);
        this.setTitle("Emergency !!");
        this.setPostBody("Help " + user.getName() + " Please !");
        this.setDistance(500);
        this.countryId = countryid;
    }

    public Emergency(String id, User user, String content, double rate, int distance, boolean active, String date, String countryid) {
        this.setId(id);
        this.setUser(user);
        this.setPostBody(content);
        this.rate = rate;
        this.setDistance(distance);
        this.countryId = countryid;
        this.active = active;
        this.setDate(date);
    }

    public ArrayList<EmergencyNumber> getEmeNumbers() {
        return emeNumbers;
    }

    public void setEmeNumbers(ArrayList<EmergencyNumber> emeNumbers) {
        this.emeNumbers = emeNumbers;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public void addEmergency(Emergency emergency, final ServerCallback callback) {
        try {
            jsonParser = new JsonParser();
            JSONObject emergencyJason = jsonParser.encodeEmergency(emergency);
            String url = URL + "api/notarea/users/" + emergency.getUser().getId() + "/emergencies";
            Log.d(TAG, "addEmergency1: " + emergencyJason);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                    (Request.Method.POST, url, emergencyJason, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject responsee) {
                            Log.d(TAG, "addEmergency2: " + responsee.toString());
                            callback.onSuccess(responsee.toString());
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d(TAG, error.toString());
                        }
                    });
            // Access the RequestQueue through your singleton class.
            Singleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
    }

    public void removeEmergency(String userID, final ServerCallback callback) {
        try {
            String emergencyID = context.getSharedPreferences("currentUser", Context.MODE_PRIVATE).getString("emergencyID", "");
            String url = URL + "api/notarea/users/" + userID + "/emergencies/" + emergencyID;
            Log.d(TAG, "cc " + url);
            StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d(TAG, "Remove Emergency1: " + response.toString());
                    callback.onSuccess(response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, error.toString());
                }
            });
            // Access the RequestQueue through your singleton class.
            Singleton.getInstance(context).addToRequestQueue(stringRequest);
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void getEmergencies(String currentUserID, String countryCode, Location location, final ServerCallback callback) {
        JSONObject locationJson = jsonParser.encodeLocation(location, countryCode);
        String url = URL + "api/notarea/users/nearestemergencies";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, locationJson, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject responsee) {
                        Log.d(TAG, "getEmergencies response: " + responsee.toString());
                        callback.onSuccess(responsee.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse response = error.networkResponse;
                        if (response != null && response.data != null) {
                            switch (response.statusCode) {
                                case 404:
                                    callback.onSuccess("not_found");
                                    break;
                                case 500:
                                    callback.onSuccess("php_error");
                                    break;
                            }
                        }
                    }
                });
        Singleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public boolean rateEmergency(Emergency emergency, final ServerCallback callback) {
        try {
            jsonParser = new JsonParser();
            JSONObject emergencyJason = jsonParser.encodeRateEmergency(emergency);
            String url = URL + "api/notarea/users/emergencies/rate";
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                    (Request.Method.POST, url, emergencyJason, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject responsee) {
                            Log.d(TAG, "rate Emergency response : " + responsee.toString());
                            callback.onSuccess(responsee.toString());
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d(TAG, error.toString());
                        }
                    });
            // Access the RequestQueue through your singleton class.
            Singleton.getInstance(this.getContext()).addToRequestQueue(jsonObjectRequest);

        } catch (Exception e) {
            Log.d(TAG, e.toString());
            return false;
        }
        return true;
    }

    public boolean getEmerNums(Emergency emergency, final ServerCallback callback) {
        try {
            jsonParser = new JsonParser();
            String url = URL + "api/notarea/emergencynumbers/" + emergency.getCountryId();
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                    (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject responsee) {
                            Log.d(TAG, "get Emergency Numbers: " + responsee.toString());
                            callback.onSuccess(responsee.toString());
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d(TAG + "**", error.toString());
                        }
                    });
            Singleton.getInstance(this.getContext()).addToRequestQueue(jsonObjectRequest);
        } catch (Exception e) {
            Log.d(TAG, e.toString());
            return false;
        }
        return true;
    }

    public boolean updateLocationWithOutNotification() {
        jsonParser = new JsonParser();
        JSONObject locationUpdateJson = jsonParser.encodeLocationUpdater(this.getContext());
        String url = URL + "api/notarea/users/changelocation";
        Log.d(TAG, "update locatoin: " + locationUpdateJson.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, locationUpdateJson, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject responsee) {
                        Log.d(TAG, "updateLocationWithOutNotification: " + responsee.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                });
        // Access the RequestQueue through your singleton class.
        Singleton.getInstance(this.getContext()).addToRequestQueue(jsonObjectRequest);
        return true;
    }

    public boolean updateLocationNotification() {
        jsonParser = new JsonParser();
        JSONObject locationUpdateJson = jsonParser.encodeLocationNotificationUpdater(this.getContext());
        String url = URL + "api/notarea/emergencies/notifications/changeUserLocation";
        Log.d(TAG, "update locatoin emergency : " + locationUpdateJson.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, locationUpdateJson, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject responsee) {
                        Log.d(TAG, "updateLocationNotification: " + responsee.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                });
        // Access the RequestQueue through your singleton class.
        Singleton.getInstance(this.getContext()).addToRequestQueue(jsonObjectRequest);
        return true;
    }

    public void updateLocation() {
        String victim = this.getContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("victim", "");
        if (victim.equals("true")) updateLocationNotification();
        else {
            updateLocationWithOutNotification();
        }
    }

}
