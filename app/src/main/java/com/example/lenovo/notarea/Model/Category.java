package com.example.lenovo.notarea.Model;


import android.content.Context;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Constants;

import org.json.JSONArray;

public class Category {
    private String TAG = "Category";
    private String id;
    private String name;
    private boolean checked;
    private Constants constants = new Constants();
    private Context context;

    public Category() {
    }

    public Category(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void getCategories(final ServerCallback callback) {
        String url = constants.getURL() + "api/notarea/categories";
        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray responsee) {
                        Log.d(TAG, "getCategories response: ");
                        callback.onSuccess(responsee.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse response = error.networkResponse;
                        if (response != null && response.data != null) {
                            switch (response.statusCode) {
                                case 404:
                                    callback.onSuccess("error");
                                    break;
                                case 500:
                                    callback.onSuccess("php_error");
                                    break;
                            }
                        }
                    }
                });
        Singleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

}
