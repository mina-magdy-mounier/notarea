package com.example.lenovo.notarea.Presenter;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Model.Advertisement;
import com.example.lenovo.notarea.Model.Attachment;
import com.example.lenovo.notarea.Model.Category;
import com.example.lenovo.notarea.Model.Location;
import com.example.lenovo.notarea.Presenter.view_model_adapters.CategoryAdapter;
import com.example.lenovo.notarea.View.activity.AdsEditor;
import com.example.lenovo.notarea.View.activity.AdsForm;
import com.example.lenovo.notarea.View.fragments.AdsPageFragment;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.content.Context.LOCATION_SERVICE;

public class AdsPresenter extends PostPresenter {
    private Advertisement advertisement;
    private ArrayList<Advertisement> advertisements;
    private AdsPageFragment adsPageFragment;
    private ArrayList<Category> categories;
    private Category category;

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public AdsPresenter(SharedPreferences session) {
        super(session);
        this.advertisements = new ArrayList<>();
        this.advertisement = new Advertisement();
    }

    public void setAdvertisements(ArrayList<Advertisement> advertisements) {
        this.advertisements = advertisements;
    }

    public Advertisement getAdvertisement() {
        return advertisement;
    }

    public ArrayList<Advertisement> getAdvertisements() {
        return advertisements;
    }

    public void initAds(final AdsPageFragment adsPageFragment, final ServerCallback callback) {
        advertisements = new ArrayList<>();
        getMainActivity().getAllIsDone()[1] = "load";
        getMainActivity().main_progress_bar.setVisibility(View.VISIBLE);
        Location location = new Location();
        if (adsPageFragment.getContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLng", "").equals("")) {
            android.location.Location lastKnownLocation = getLastKnownLocation(adsPageFragment.getContext());
            location = new Location(lastKnownLocation.getLongitude(), lastKnownLocation.getLatitude());
        } else {
            location = new Location(Double.parseDouble(adsPageFragment.getContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLng", "")), Double.parseDouble(adsPageFragment.getContext().getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLat", "")));
        }
        JSONObject jsonObject = jsonParser.encodePostsNearest(location, this.getUser().getId());
        advertisement.getADS(jsonObject, new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                if (!result.equals("error") && !result.equals("php_error")) {
                    getMainActivity().getAllIsDone()[1] = "done";
                    callback.onSuccess(result);
                    adsPageFragment.adsListView.setVisibility(View.VISIBLE);
                    adsPageFragment.no_ads_normal.setVisibility(View.GONE);
                    adsPageFragment.no_ads_premium.setVisibility(View.GONE);
                } else if (result.equals("error")) {
                    adsPageFragment.adsListView.setVisibility(View.VISIBLE);
                    getMainActivity().getAllIsDone()[1] = "error";
                    if (user.getPremium() == 0) {
                        adsPageFragment.no_ads_normal.setVisibility(View.VISIBLE);
                        adsPageFragment.no_ads_premium.setVisibility(View.GONE);
                    } else {
                        adsPageFragment.no_ads_normal.setVisibility(View.GONE);
                        adsPageFragment.no_ads_premium.setVisibility(View.VISIBLE);
                    }
                    callback.onSuccess(null);
                } else {
                    getMainActivity().getAllIsDone()[1] = "error";
                    Snackbar.make(adsPageFragment.adsListView, "connection error", Snackbar.LENGTH_SHORT).show();

                }
                if (allDone(getMainActivity().getAllIsDone())) {
                    getMainActivity().main_progress_bar.setVisibility(View.GONE);
                }
            }
        });
    }

    public void initEditor(Bundle bundle) {
        this.advertisement = jsonParser.decodeNearestAdsPost(bundle.get("eAds").toString()).get(0);
    }

    @Override
    public Attachment addAttachment(String FaceBook, boolean addLocation , Location location, ArrayList<String> phones) {
        return super.addAttachment(FaceBook, addLocation , location, phones);
    }

    public void addAdvertisment(final AdsForm adsForm, String title, String postBody, String userFeeling, int userFeelingImg, String FaceBook, ArrayList<String> phones, boolean addLocation, String AdsImg, String shared_distance, String days_of_ads, ArrayList<Category> categories) {
        try {
            if (phones.size() == 0) {
                phones = null;
            }
            if (title.equals("")) {
                title = null;
            }
            Location location = new Location();
            if (adsForm.getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLng", "").equals("")) {
                android.location.Location lastKnownLocation = getLastKnownLocation(adsForm.getApplicationContext());
                location = new Location(lastKnownLocation.getLongitude(), lastKnownLocation.getLatitude());
            } else {
                location = new Location(Double.parseDouble(adsForm.getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLng", "")), Double.parseDouble(adsForm.getApplicationContext().getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLat", "")));
            }
            Attachment attachment = addAttachment(FaceBook, addLocation , location, phones);
            Advertisement advertisement = new Advertisement(title, postBody, userFeeling, userFeelingImg, 0, 0, 0, 0, 0, attachment, user.getId(), user, location, AdsImg, Integer.parseInt(shared_distance), Integer.parseInt(days_of_ads), categories);
            advertisement.setContext(adsForm);
            JSONObject postJson = jsonParser.encodeAdsPost(advertisement);
            advertisement.addAds(postJson, this.getUser().getId(), new ServerCallback() {
                @Override
                public void onSuccess(String result) {
                    if (result != null) {
                        adsForm.adding_in_progress.setVisibility(View.GONE);
                        adsForm.finish();
                    } else {
                        adsForm.adding_in_progress.setVisibility(View.GONE);
                        Toast.makeText(adsForm.getApplicationContext(), "Failure in adding advertisement", Toast.LENGTH_LONG).show();
                    }
                }
            });
        } catch (Exception e) {
            Log.d("AdsPresenter", e.toString());
        }
    }

    // title-0 , body-1 , face-2 , numbers-3 , location-4 , feelings-5
    public void editAds(final AdsEditor adsPostEditor, String postID, String title, String postBody, String userFeeling, int userFeelingImg, String FaceBook, ArrayList<String> phones, boolean addLocation, String image, String distance, Boolean editedParts[]) {
        final String DELETED_CODE = "";
        try {
            Attachment attachment;
            if (editedParts[2] || editedParts[3] || editedParts[4]) {
                attachment = addAttachment(FaceBook, addLocation , null, phones);
            } else {
                attachment = null;
            }

            if (!editedParts[0]) {
                title = null;
            } else {
                if (title.equals("")) {
                    title = DELETED_CODE;
                }
            }
            if (!editedParts[1]) {
                postBody = null;
            } else {
                if (postBody.equals("")) {
                    postBody = DELETED_CODE;
                }
            }
            Location location = new Location(0, 0);
            final Advertisement post = new Advertisement(title, postBody, userFeeling, userFeelingImg, 0, 0, 0, 0, 0, attachment, user.getId(), user, location, image, Integer.parseInt(distance));
            post.setContext(adsPostEditor);
            JSONObject postJson = jsonParser.encodeEditedAdsPost(post, editedParts);
            Log.d(TAG, "editAds: " + postJson);
            post.editAds(this.getUser().getId(), postID, postJson, new ServerCallback() {
                @Override
                public void onSuccess(String result) {
                    adsPostEditor.adding_in_progress.setVisibility(View.GONE);
                    Toast.makeText(adsPostEditor.getApplicationContext(), "ads edited", Toast.LENGTH_SHORT).show();
                    adsPostEditor.finish();
                }
            });
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
    }

    @Override
    public void removePost(int position, String postID, final ServerCallback callback) {
        advertisement.deleteAds(user.getId(), postID, new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                callback.onSuccess(result);
            }
        });
    }

    public void initCategory(final AdsForm adsForm) {
        categories = new ArrayList<>();
        category = new Category();
        category.setContext(adsForm);
        category.getCategories(new ServerCallback() {
            @Override
            public void onSuccess(String result) {

                if (!result.equals("error") && !result.equals("php_error")) {
                    categories = jsonParser.encodeCategory(result);
                    if (categories.size() > 0) {
                        adsForm.categoryAdapter = new CategoryAdapter(adsForm, categories);
                        adsForm.categoryList.setAdapter(adsForm.categoryAdapter);
                        adsForm.categoryList.setVisibility(View.VISIBLE);
                        adsForm.no_category.setVisibility(View.GONE);
                    } else {
                        adsForm.no_category.setVisibility(View.VISIBLE);
                        adsForm.categoryList.setVisibility(View.GONE);
                    }
                }
            }
        });

    }
    private android.location.Location getLastKnownLocation(Context context) {
        LocationManager mLocationManager;
        mLocationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        android.location.Location bestLocation = null;
        for (String provider : providers) {
            android.location.Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        context.getApplicationContext().getSharedPreferences("currentUser",Context.MODE_MULTI_PROCESS).edit().putString("currentLat", bestLocation.getLatitude()+"").apply();
        context.getApplicationContext().getSharedPreferences("currentUser",Context.MODE_MULTI_PROCESS).edit().putString("currentLng", bestLocation.getLongitude()+"").apply();
        context.getApplicationContext().getSharedPreferences("currentUser",Context.MODE_MULTI_PROCESS).edit().putString("address", getCompleteAddressString(context , bestLocation.getLatitude(),bestLocation.getLongitude())).apply();
        return bestLocation;
    }

    public String getCompleteAddressString(Context context , double LATITUDE, double LONGITUDE) {
        // this.setContext(ma);
        String strAdd = "";
        StringBuilder strReturnedAddress = new StringBuilder("");
        try {

            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);

            if (addresses != null) {
                Address returnedAddress = addresses.get(0);

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            }

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            //Toast.makeText(this, "Address : " + address, Toast.LENGTH_SHORT).show();
            String city = addresses.get(0).getLocality();
            //Toast.makeText(this, "City : " + city, Toast.LENGTH_SHORT).show();
            String state = addresses.get(0).getAdminArea();
            //Toast.makeText(this, "State : " + state, Toast.LENGTH_SHORT).show();
            String country = addresses.get(0).getCountryName();
            //Toast.makeText(this, "Country : " + country, Toast.LENGTH_SHORT).show();
            //String postalCode = addresses.get(0).getPostalCode();
            //Toast.makeText(this,"Postal Code : " + postalCode,Toast.LENGTH_SHORT).show();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
            //Toast.makeText(this, "Known Name : " + knownName, Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
            //Log.w("My Current location address", "Cannot get Address!");
        }

        return strAdd;
    }
}
