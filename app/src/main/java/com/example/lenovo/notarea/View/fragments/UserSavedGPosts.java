package com.example.lenovo.notarea.View.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Presenter.JsonParser;
import com.example.lenovo.notarea.Presenter.PostPresenter;
import com.example.lenovo.notarea.Presenter.UserPreseneter;
import com.example.lenovo.notarea.Presenter.view_model_adapters.GeneralPostsAdapter;
import com.example.lenovo.notarea.R;

public class UserSavedGPosts extends Fragment {

    static GeneralPostsAdapter generalPostsAdapter;
    ListView listView;
    LinearLayout my_posts;
    LinearLayout has_posts;
    JsonParser jsonParser = new JsonParser();
    UserPreseneter userPreseneter;
    PostPresenter postPresenter;
    SharedPreferences session;

    public UserSavedGPosts() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_user_saved_gposts, container, false);
        session = view.getContext().getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        userPreseneter = new UserPreseneter(session);
        postPresenter = new PostPresenter();
        listView = view.findViewById(R.id.savedGpostsList);
        my_posts = view.findViewById(R.id.my_posts);
        has_posts = view.findViewById(R.id.has_posts);
        init();

        return view;
    }

    public void init() {
        my_posts.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);
        has_posts.setVisibility(View.GONE);
        userPreseneter.getMySavedGPosts(new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                if (result != null) {
                    if (!result.equals("error") && !result.equals("php_error")) {
                        try {
                            my_posts.setVisibility(View.GONE);
                            listView.setVisibility(View.VISIBLE);
                            has_posts.setVisibility(View.GONE);
                            userPreseneter.getUser().setSavedGPosts(jsonParser.decodeGlobalGPost(result));
                            generalPostsAdapter = new GeneralPostsAdapter(getActivity(), userPreseneter.getUser().getSavedGPosts(), userPreseneter.getUser().getId());
                            generalPostsAdapter.setUserSavedGPosts(UserSavedGPosts.this);
                            listView.setAdapter(generalPostsAdapter);
                        } catch (Exception e) {

                        }
                    } else if (result.equals("error")) {
                        has_posts.setVisibility(View.VISIBLE);
                        my_posts.setVisibility(View.GONE);
                        listView.setVisibility(View.GONE);
                    } else {
                        Snackbar.make(listView, "connection error", Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

}
