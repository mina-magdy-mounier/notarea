package com.example.lenovo.notarea.View.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.example.lenovo.notarea.Model.Emergency;
import com.example.lenovo.notarea.Presenter.EmergencyPresenter;
import com.example.lenovo.notarea.Presenter.JsonParser;
import com.example.lenovo.notarea.Presenter.Maps.MapsActivity;
import com.example.lenovo.notarea.Presenter.view_model_adapters.EmergencyAdapter;
import com.example.lenovo.notarea.R;
import com.example.lenovo.notarea.View.activity.MainActivity;

public class EmergencyPageFragment extends Fragment {

    public LinearLayout no_emergency_normal;
    public static ListView emergencyViewList;
    Button emergencyButton;
    MainActivity mainActivity;
    EmergencyPresenter emergencyPresenter;
    SharedPreferences session;
    Boolean allIsDone[];
    static EmergencyAdapter emergencyAdapter;
    JsonParser jsonParser = new JsonParser();
    boolean emergency_found = false;

    public EmergencyPageFragment() {
        // Required empty public constructor
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View emergencyPage = inflater.inflate(R.layout.emergency_page_fragment, container, false);
        session = emergencyPage.getContext().getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, new IntentFilter("EmergencyPageFragment"));
        emergencyPresenter = new EmergencyPresenter(session);
        emergencyPresenter.setMainActivity(mainActivity);
        no_emergency_normal = emergencyPage.findViewById(R.id.no_emergency);
        //emergencyPresenter.setAllIsDone(allIsDone);
        emergencyPresenter.getEmergency().setContext(getContext());
        emergencyPresenter.setEmergencyPageFragment(this);
        emergencyPresenter.init();
        /////////////////////////////////////////////////////////////////////
        //emergencyButton = emergencyPage.findViewById(R.id.sign_in_progressbar);
        // emergencyPresenter.init();
        emergencyViewList = emergencyPage.findViewById(R.id.emergency_list_view);
        // EmergencyAdapter emergencyAdapter = new EmergencyAdapter(getActivity() , emergencyPresenter.getEmergencies());
        //emergencyListView.setAdapter(emergencyAdapter);
        setHasOptionsMenu(true);


        emergencyViewList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Emergency emergency = (Emergency) adapterView.getItemAtPosition(i);
                Intent intent = new Intent(getActivity(), MapsActivity.class);
                intent.putExtra("LAT", String.valueOf(emergency.getLocation().getLatitude()));
                intent.putExtra("LNG", String.valueOf(emergency.getLocation().getLongitude()));
                intent.putExtra("ID", String.valueOf(emergency.getId()));
                intent.putExtra("NAME", String.valueOf(emergency.getUser().getName()));
                intent.putExtra("PHOTO", String.valueOf(emergency.getUser().getProfile_Img()));
                startActivity(intent);
            }
        });

        return emergencyPage;
    }

    public void emergenciesReady() {
        emergencyAdapter = new EmergencyAdapter(getActivity(), emergencyPresenter.getEmergencies(), emergencyPresenter.getUser().getId());
        emergencyViewList.setAdapter(emergencyAdapter);
        emergencyAdapter.setEmergencyPageFragment(this);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String json = intent.getStringExtra("json");
            Emergency emergency = jsonParser.decodeEmergencyNotification(json);
            if (emergencyAdapter != null) {
                if (emergencyPresenter.getEmergencies() != null) {
                    if (emergencyPresenter.getEmergencies().size() != 0) {
                        int x = emergencyPresenter.getEmergencies().size();
                        for (int i = 0; i < x; i++) {
                            if (emergency.getId().equals(emergencyPresenter.getEmergencies().get(i).getId())) {
                                Toast.makeText(mainActivity, "Emergency already found", Toast.LENGTH_SHORT).show();
                                emergencyPresenter.getEmergencies().set(i, emergency);
                                emergencyAdapter.notifyDataSetChanged();
                                emergency_found = true;
                                return;
                            }
                            emergency_found = false;
                        }
                    } else {
                        emergencyPresenter.emergencyRefresh();
                    }

                    if (!emergency_found) {
                        emergencyPresenter.getEmergencies().add(0, jsonParser.decodeEmergencyNotification(json));
                        emergencyAdapter.notifyDataSetChanged();
                    }

                } else {
                    Toast.makeText(getContext(), "is null", Toast.LENGTH_LONG).show();
                }
            } else {
                emergencyPresenter.emergencyRefresh();
            }
        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.refresh, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menuRefresh) {
            emergencyPresenter.emergencyRefresh();
        }
        return super.onOptionsItemSelected(item);
    }

    public SharedPreferences getSession() {
        return session;
    }

    public void setSession(SharedPreferences session) {
        this.session = session;
    }

    public Button getEmergencyButton() {
        return emergencyButton;
    }

    public void setEmergencyButton(Button emergencyButton) {
        this.emergencyButton = emergencyButton;
    }

    public MainActivity getMainActivity() {
        return mainActivity;
    }

    public EmergencyPresenter getEmergencyPresenter() {
        return emergencyPresenter;
    }

    public void setEmergencyPresenter(EmergencyPresenter emergencyPresenter) {
        this.emergencyPresenter = emergencyPresenter;
    }

    public Boolean[] getAllIsDone() {
        return allIsDone;
    }

    public EmergencyAdapter getEmergencyAdapter() {
        return emergencyAdapter;
    }

    public void setEmergencyAdapter(EmergencyAdapter emergencyAdapter) {
        this.emergencyAdapter = emergencyAdapter;
    }

}
