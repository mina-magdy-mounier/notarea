package com.example.lenovo.notarea.Model;

public class EmergencyNumber {

    final String TAG = "EmergencyNumber";
    String emergencyService;
    String PhoneNumber;

    public EmergencyNumber(String emergencyService, String phoneNumber) {
        this.emergencyService = emergencyService;
        PhoneNumber = phoneNumber;
    }

    public EmergencyNumber() {
        emergencyService = "";
        PhoneNumber = "";
    }

    public String getEmergencyService() {
        return emergencyService;
    }

    public void setEmergencyService(String emergencyService) {
        this.emergencyService = emergencyService;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

}
