package com.example.lenovo.notarea.View.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.FireBase.FirebaseStorageService;
import com.example.lenovo.notarea.Presenter.SessionHandler;
import com.example.lenovo.notarea.Presenter.UserPreseneter;
import com.example.lenovo.notarea.Presenter.Validation.UserValidation;
import com.example.lenovo.notarea.Presenter.Verification.UserVerification;
import com.example.lenovo.notarea.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class SignUp extends AppCompatActivity {

    public ProgressBar signUpProgressBar;
    Bitmap userProfileIMG = null;
    ImageView userProfileImg;
    FloatingActionButton profile_img_btn;
    Button signUp, addFamilyListButton, addBusinessListButton, addNewFamilyPhoneNumber, addNewBusinessPhoneNumber;
    Spinner ageSpinner, genderSpinner;
    Dialog familyListDialog, businessListDialog;
    ListView familyPhoneListView, businessPhoneListView;
    EditText userMail, userName, userPassword, userPasswordr, userPhone, newFamilyPhoneNumber, newBusinessPhoneNumber;
    String userAgeValue = null, userGenderValue = null, img_link = null;
    boolean mail = false, name = false, password = false, passwordr = false, phone = false, bUserAge = false, bUserGender = false;
    HashMap<String, String> familyMap = new HashMap<>();
    HashMap<String, String> businessMap = new HashMap<>();
    ArrayAdapter<String> familyListAdapter, businessListAdapter;
    ArrayList<String> familyArrayList, businessArrayList;
    SharedPreferences userSession;
    UserPreseneter userPreseneter;
    UserValidation validation = new UserValidation();
    UserVerification userVerification = new UserVerification();
    FirebaseStorageService firebaseStorageService = new FirebaseStorageService();
    Uri imgURI = null;
    public Dialog loading_dialog;
    SessionHandler sessionHandler = new SessionHandler();

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == 101) {
                Uri imgUri = data.getData();
                try {
                    userProfileIMG = MediaStore.Images.Media.getBitmap(getContentResolver(), imgUri);
                    userProfileImg.setImageBitmap(userProfileIMG);
                    userProfileImg.setVisibility(View.VISIBLE);
                    this.imgURI = imgUri;
                    profile_img_btn.setImageResource(R.drawable.ic_close_white_24dp);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void cancelImg(final FloatingActionButton profile_img_btn) {
        profile_img_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userProfileIMG != null) {
                    userProfileImg.setImageBitmap(null);
                    userProfileImg.setImageResource(R.drawable.nobody);
                    profile_img_btn.setImageResource(R.drawable.ic_camera_alt_black_24dp);
                    userProfileIMG = null;
                    imgURI = null;
                } else {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("image/jpeg");
                    intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                    startActivityForResult(intent, 101);
                }
            }
        });
    }

    private void getUserName(final EditText userName) {
        userName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    String error = validation.validateUserName(userName.getText().toString());
                    if (error != null) {
                        userName.setError(error);
                        name = false;
                    } else {
                        String verified = userVerification.verifyUserName(userName.getText().toString());
                        if (verified != null) {
                            userName.setError(verified);
                            name = false;
                        } else {
                            name = true;
                        }

                    }
                }
            }
        });
    }

    private void getUserEmail(final EditText userMail) {
        userMail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    String error = validation.validateUserEmail(userMail.getText().toString());
                    if (error != null) {
                        userMail.setError(error);
                        mail = false;
                    } else {
                        String verified = userVerification.verifyUserMail(userMail.getText().toString());
                        if (verified != null) {
                            userMail.setError(verified);
                            mail = false;
                        } else {
                            mail = true;
                        }
                    }
                }
            }
        });
    }

    private void getUserPassword(final EditText userPassword) {
        userPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    String error = validation.validateUserPassword(userPassword.getText().toString());
                    if (error != null) {
                        userPassword.setError(error);
                        password = false;
                    } else {
                        password = true;
                    }
                }
            }
        });
    }

    public void getUserPhone(final EditText userPhone) {
        userPhone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    String error = validation.validateUserPhone(userPhone.getText().toString());
                    if (error != null) {
                        userPhone.setError(error);
                        phone = false;
                    } else {
                        phone = true;
                    }
                }
            }
        });
    }

    private void fillUserAgeSpinner(Spinner ageSpinner) {
        ArrayList<Integer> values = new ArrayList<>();
        for (int i = 16; i < 100; i++) {
            values.add(i);
        }
        ArrayAdapter<Integer> spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, values);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ageSpinner.setAdapter(spinnerArrayAdapter);
    }

    public void getUserAge(final Spinner userAge) {
        userAge.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                userAgeValue = userAge.getSelectedItem().toString();
                bUserAge = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                userAgeValue = "16";
            }
        });
    }

    public void getUserGender(final Spinner userGender) {
        userGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                userGenderValue = userGender.getSelectedItem().toString();
                bUserGender = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                userGenderValue = "Male";
            }
        });
    }

    public void familyListBtnListnere(Button addFamilyListButton, final Dialog d) {
        addFamilyListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.show();
            }
        });
    }

    public void businessListBtnListnere(Button addBusinessListButton, final Dialog d) {
        addBusinessListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.show();
            }
        });
    }

    public void addNewFamilyPhoneBtnListner(final Button add, final EditText phone, final ArrayList<String> familyArrayList, final HashMap<String, String> familyMap, final ArrayAdapter<String> familyListAdapter) {
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation.validateExtraNumberInput(phone, SignUp.this)) {
                    String name = phone.getText().toString().split("-")[0];
                    String number = phone.getText().toString().split("-")[1];
                    familyArrayList.add(phone.getText().toString());
                    familyMap.put(number, name);
                    familyListAdapter.notifyDataSetChanged();
                    phone.setText("");
                }
            }
        });
    }

    public void addNewBusinessPhoneBtnListner(final Button add, final EditText phone, final ArrayList<String> businessArrayList, final HashMap<String, String> businessMap, final ArrayAdapter<String> businessListAdapter) {
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation.validateExtraNumberInput(phone, SignUp.this)) {
                    String name = phone.getText().toString().split("-")[0];
                    String number = phone.getText().toString().split("-")[1];
                    businessArrayList.add(phone.getText().toString());
                    businessMap.put(number, name);
                    businessListAdapter.notifyDataSetChanged();
                    phone.setText("");
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userSession = getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        if (sessionHandler.get_theme(userSession) != R.style.AppTheme) {
            setTheme(sessionHandler.get_theme(userSession));
        }
        setContentView(R.layout.sign_up);
        userPreseneter = new UserPreseneter();
        userPreseneter.setSession(userSession);
        firebaseStorageService.setActivity(SignUp.this);
        signUpProgressBar = findViewById(R.id.signUpProgressBar);
        userProfileImg = findViewById(R.id.userProfileImg);
        userName = findViewById(R.id.userName);
        userMail = findViewById(R.id.userMail);
        userPassword = findViewById(R.id.userPassword);
        userPasswordr = findViewById(R.id.userPasswordr);
        userPhone = findViewById(R.id.userPhone);
        ageSpinner = findViewById(R.id.userAgeSpinner);
        genderSpinner = findViewById(R.id.userGenderSpinner);
        signUp = findViewById(R.id.signUpButton);
        profile_img_btn = findViewById(R.id.profile_img_btn);
        signUpProgressBar.setVisibility(View.GONE);
        addFamilyListButton = findViewById(R.id.addFamilyList);
        addBusinessListButton = findViewById(R.id.addBusinessList);
        familyListDialog = new Dialog(this, R.style.CustomDialogTheme);
        businessListDialog = new Dialog(this, R.style.CustomDialogTheme);
        familyListDialog.setContentView(R.layout.add_user_extra_phones_form);
        businessListDialog.setContentView(R.layout.add_user_extra_phones_form);
        familyPhoneListView = familyListDialog.findViewById(R.id.phoneList);
        addNewFamilyPhoneNumber = familyListDialog.findViewById(R.id.addPhoneNumber);
        newFamilyPhoneNumber = familyListDialog.findViewById(R.id.phoneNumber);
        businessPhoneListView = businessListDialog.findViewById(R.id.phoneList);
        addNewBusinessPhoneNumber = businessListDialog.findViewById(R.id.addPhoneNumber);
        newBusinessPhoneNumber = businessListDialog.findViewById(R.id.phoneNumber);
        loading_dialog = new Dialog(this, R.style.CustomDialogTheme);
        loading_dialog.setContentView(R.layout.loading);
        loading_dialog.setCanceledOnTouchOutside(false);
        familyArrayList = new ArrayList<>();
        familyListAdapter = new ArrayAdapter<>(SignUp.this, android.R.layout.simple_list_item_1, familyArrayList);
        familyPhoneListView.setAdapter(familyListAdapter);
        businessArrayList = new ArrayList<>();
        businessListAdapter = new ArrayAdapter<>(SignUp.this, android.R.layout.simple_list_item_1, businessArrayList);
        businessPhoneListView.setAdapter(businessListAdapter);
        fillUserAgeSpinner(ageSpinner);
        cancelImg(profile_img_btn);
        getUserName(userName);
        getUserEmail(userMail);
        getUserPassword(userPassword);
        getUserPhone(userPhone);
        getUserAge(ageSpinner);
        getUserGender(genderSpinner);
        signUpButtonListener(signUp);
        familyListBtnListnere(addFamilyListButton, familyListDialog);
        businessListBtnListnere(addBusinessListButton, businessListDialog);
        addNewFamilyPhoneBtnListner(addNewFamilyPhoneNumber, newFamilyPhoneNumber, familyArrayList, familyMap, familyListAdapter);
        addNewBusinessPhoneBtnListner(addNewBusinessPhoneNumber, newBusinessPhoneNumber, businessArrayList, businessMap, businessListAdapter);


        familyPhoneListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String name = adapterView.getItemAtPosition(i).toString();
                familyMap.remove(name.split("-")[1]);
                familyArrayList.remove(i);
                familyListAdapter.notifyDataSetChanged();
            }
        });

        businessPhoneListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String name = adapterView.getItemAtPosition(i).toString();
                businessMap.remove(name.split("-")[1]);
                businessArrayList.remove(i);
                businessListAdapter.notifyDataSetChanged();
            }
        });

    }

    public boolean isEmpty() {
        return !userName.getText().toString().equals("") && !userPassword.getText().toString().equals("") && !userPhone.getText().toString().equals("") && !userMail.getText().toString().equals("");
    }

    public void signUpButtonListener(final Button signUp) {
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userMail.clearFocus();
                userName.clearFocus();
                userPassword.clearFocus();
                userPhone.clearFocus();
                String error = validation.passwordMatcher(userPassword.getText().toString(), userPasswordr.getText().toString());
                if (error != null) {
                    Toast.makeText(SignUp.this, error, Toast.LENGTH_LONG).show();
                } else {
                    if (mail && name && password && phone && bUserAge && bUserGender && isEmpty()) {
                        loading_dialog.show();
                        if (imgURI != null) {
                            firebaseStorageService.get_photo_local_uri(imgURI, new ServerCallback() {
                                @Override
                                public void onSuccess(String result) {
                                    userPreseneter.signUp(SignUp.this, result, userName.getText().toString(), userMail.getText().toString(), userPassword.getText().toString(), userPhone.getText().toString(), userAgeValue, userGenderValue, familyMap, businessMap);
                                }
                            });
                        } else {
                            userPreseneter.signUp(SignUp.this, null, userName.getText().toString(), userMail.getText().toString(), userPassword.getText().toString(), userPhone.getText().toString(), userAgeValue, userGenderValue, familyMap, businessMap);
                        }
                    } else {
                        loading_dialog.dismiss();
                        Toast.makeText(getApplicationContext(), "All data Must be filled and vaild", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

}



