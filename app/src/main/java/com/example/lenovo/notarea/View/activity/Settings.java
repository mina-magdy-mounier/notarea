package com.example.lenovo.notarea.View.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.notarea.CallBacks.FunctionCallback;
import com.example.lenovo.notarea.Presenter.SessionHandler;
import com.example.lenovo.notarea.Presenter.UserPreseneter;
import com.example.lenovo.notarea.Presenter.Validation.UserValidation;
import com.example.lenovo.notarea.R;

public class Settings extends AppCompatActivity {

    public ProgressBar user_name_loader, change_password_loader;
    public EditText user_name, old_password, new_password, confirm_password;
    public Dialog userNameDialog, passwordChangeDialog;
    Button profile_editor, user_name_editor, password_editor, de_activate_btn, delete_btn, change_color, nok, ncancel, pok, pcancel, color_ok, color_cancel;
    Switch notification;
    TextView default_theme, blue_theme, green_theme, notify;
    SharedPreferences session;
    Dialog color_picker;
    UserPreseneter userPreseneter;
    UserValidation userValidation;
    FormHandler formHandler = new FormHandler();
    SessionHandler sessionHandler = new SessionHandler();

    private int theme_index = R.style.AppTheme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        if (sessionHandler.get_theme(session) != R.style.AppTheme) {
            setTheme(sessionHandler.get_theme(session));
        }
        setContentView(R.layout.settings);

        userPreseneter = new UserPreseneter(session);
        userValidation = new UserValidation();

        profile_editor = findViewById(R.id.changeProfile);
        user_name_editor = findViewById(R.id.changeUserName);
        password_editor = findViewById(R.id.changePassword);
        de_activate_btn = findViewById(R.id.deActivateAccount);
        delete_btn = findViewById(R.id.deleteAccount);
        notification = findViewById(R.id.notification);
        change_color = findViewById(R.id.changeColor);
        notify = findViewById(R.id.notify);

        passwordChangeDialog = new Dialog(this, R.style.CustomDialogTheme);
        userNameDialog = new Dialog(this, R.style.CustomDialogTheme);

        passwordChangeDialog.setContentView(R.layout.change_password);
        userNameDialog.setContentView(R.layout.change_user_name);

        user_name = userNameDialog.findViewById(R.id.user_name);
        user_name_loader = userNameDialog.findViewById(R.id.user_name_loading);
        nok = userNameDialog.findViewById(R.id.nok);
        ncancel = userNameDialog.findViewById(R.id.ncancel);

        old_password = passwordChangeDialog.findViewById(R.id.current_password);
        new_password = passwordChangeDialog.findViewById(R.id.new_password);
        confirm_password = passwordChangeDialog.findViewById(R.id.confirm_password);
        pok = passwordChangeDialog.findViewById(R.id.pok);
        pcancel = passwordChangeDialog.findViewById(R.id.pcancel);
        change_password_loader = passwordChangeDialog.findViewById(R.id.change_password_loader);

        color_picker = new Dialog(this, R.style.CustomDialogTheme);
        color_picker.setContentView(R.layout.color_picker);
        color_ok = color_picker.findViewById(R.id.color_ok);
        color_cancel = color_picker.findViewById(R.id.color_cancel);
        default_theme = color_picker.findViewById(R.id.red);
        green_theme = color_picker.findViewById(R.id.green);
        blue_theme = color_picker.findViewById(R.id.blue);

        toProfileEditor();
        update_user_name();
        update_password();
        theme_changer();
        Color_ok();
        control_notification();
        init_notification_status();
        formHandler.show_dialog(change_color, color_picker);
        formHandler.cancel_dialog(color_cancel, color_picker);
        formHandler.show_dialog(password_editor, passwordChangeDialog);
        formHandler.show_dialog(user_name_editor, userNameDialog);
        formHandler.cancel_dialog(pcancel, passwordChangeDialog);
        formHandler.cancel_dialog(ncancel, userNameDialog);
    }

    private void init_notification_status() {
        if (sessionHandler.get_notification_state(session) == 1) {
            notification.setChecked(true);
        } else {
            notification.setChecked(false);
        }
    }

    private void control_notification() {
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notification.isChecked()) {
                    sessionHandler.update_notification_state(session, 1);
                    Snackbar.make(notify, "Notification on", Snackbar.LENGTH_LONG).show();
                } else {
                    sessionHandler.update_notification_state(session, 0);
                    Snackbar.make(notify, "Notification off", Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    private void color_picker_indecator(TextView t1, TextView t2, TextView t3) {
        t1.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_brush_black_24dp, 0, 0);
        t2.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        t3.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
    }

    private void theme_changer() {
        default_theme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                color_picker_indecator(default_theme, green_theme, blue_theme);
                theme_index = R.style.AppTheme;
            }
        });

        blue_theme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                color_picker_indecator(blue_theme, default_theme, green_theme);
                theme_index = R.style.blue;
            }
        });

        green_theme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                color_picker_indecator(green_theme, blue_theme, default_theme);
                theme_index = R.style.green;
            }
        });
    }

    private void Color_ok() {
        color_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionHandler.update_theme(session, theme_index);
                Intent intent = new Intent(Settings.this, Settings.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void toProfileEditor() {
        profile_editor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Settings.this, ProfileEditor.class);
                startActivity(intent);
            }
        });
    }

    private void update_user_name() {
        nok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userName = user_name.getText().toString();
                String error = userValidation.validateUserName(userName);
                user_name_loader.setVisibility(View.VISIBLE);
                if (error != null) {
                    user_name.setError(error);
                    user_name_loader.setVisibility(View.GONE);
                } else {
                    userPreseneter.update_user_name(Settings.this, userName);
                }
            }
        });
    }

    private void update_password() {
        pok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String oldPassword = old_password.getText().toString();
                String newPassword = new_password.getText().toString();
                String confirmPassowrd = confirm_password.getText().toString();
                change_password_loader.setVisibility(View.VISIBLE);
                if (!oldPassword.equals("")) {
                    String error = userValidation.passwordMatcher(newPassword, confirmPassowrd);
                    if (error != null) {
                        Toast.makeText(Settings.this, error, Toast.LENGTH_LONG).show();
                        change_password_loader.setVisibility(View.GONE);
                    } else {
                        userPreseneter.change_password(oldPassword, newPassword, new FunctionCallback() {
                            @Override
                            public void onSuccess(String result) {
                                change_password_loader.setVisibility(View.GONE);
                                passwordChangeDialog.dismiss();
                                Toast.makeText(Settings.this, result, Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onError(String result) {
                                change_password_loader.setVisibility(View.GONE);
                                Toast.makeText(Settings.this, result, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                } else {
                    old_password.setError("can't be empty");
                    change_password_loader.setVisibility(View.GONE);
                }
            }
        });
    }

}
