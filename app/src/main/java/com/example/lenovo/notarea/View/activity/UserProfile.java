package com.example.lenovo.notarea.View.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.lenovo.notarea.Presenter.SessionHandler;
import com.example.lenovo.notarea.R;
import com.example.lenovo.notarea.View.adapters.UserProfileAdapter;

public class UserProfile extends AppCompatActivity {

    ViewPager userProfileViewPager;
    TabLayout userProfileTabs;
    UserProfileAdapter userProfileAdapter;
    SessionHandler sessionHandler = new SessionHandler();
    SharedPreferences session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        if (sessionHandler.get_theme(session) != R.style.AppTheme) {
            setTheme(sessionHandler.get_theme(session));
        }
        setContentView(R.layout.user_profile);

        userProfileViewPager = findViewById(R.id.profile);
        userProfileTabs = findViewById(R.id.userProfile);

        userProfileAdapter = new UserProfileAdapter(getSupportFragmentManager());
        userProfileViewPager.setAdapter(userProfileAdapter);

        userProfileViewPager.setOffscreenPageLimit(2);
        userProfileTabs.setTabMode(TabLayout.MODE_FIXED);
        userProfileTabs.setTabGravity(TabLayout.GRAVITY_FILL);
        userProfileTabs.setupWithViewPager(userProfileViewPager);


    }
}
