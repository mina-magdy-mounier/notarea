package com.example.lenovo.notarea.Presenter.Maps;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.SharedPreferencesCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.notarea.CallBacks.FunctionCallback;
import com.example.lenovo.notarea.Constants;
import com.example.lenovo.notarea.Intents;
import com.example.lenovo.notarea.Model.Emergency;
import com.example.lenovo.notarea.Model.EmergencyNumber;
import com.example.lenovo.notarea.Model.ExtraNumbers;
import com.example.lenovo.notarea.Presenter.EmergencyNumbers;
import com.example.lenovo.notarea.Presenter.EmergencyPresenter;
import com.example.lenovo.notarea.Presenter.JsonParser;
import com.example.lenovo.notarea.Presenter.SessionHandler;
import com.example.lenovo.notarea.Presenter.view_model_adapters.EmergencyNumbersAdapter;
import com.example.lenovo.notarea.R;
import com.example.lenovo.notarea.View.activity.FormHandler;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener,
        GoogleMap.OnMarkerClickListener,
        LocationListener,
        View.OnClickListener {

    private static final String TAG = "MapsActivity";
    private GoogleMap mMap;
    private LocationRequest locationRequest;
    private double longitude;
    private double latitude;
    private final int REQ_LOC_CODE = 99;
    private GoogleApiClient googleApiClient;
    private Marker currentLocationMarker;
    private Marker victimLocationMarker;
    private Location location;
    private double endLatitude, endLongitude;
    private int proximityRadius = 10000;
    private String Address;
    private Button police;
    private Button ambulance;
    private Button fire;
    private Button rate;
    NumberPicker Rate;
    GetDirectionsData getDirectionsData = new GetDirectionsData();
    Dialog rateDialog;
    NumberPicker rateWheel;
    Button rateOk,rateCancel;
    TextView rateTitle;
    FormHandler formHandler = new FormHandler();
    private Constants constants = new Constants();
    private int RATE_MIN = constants.getRATE_MIN();
    private int RATE_MAX = constants.getRATE_MAX();
    private int RATE_STEP = constants.getRATE_STEP();
    EmergencyPresenter emergencyPresenter;
    SharedPreferences session;
    String emergencyID;
    String name;
    String sharedRate = String.valueOf(RATE_MIN), tempRate = null;
    String photo;
    ArrayList<ExtraNumbers> emerNums = new ArrayList<ExtraNumbers>();
    LatLng myLoc;
    MarkerOptions markerOpts;
    private RatingBar ratingBar;
    FloatingActionMenu floatingActionMenu;
    public LocationManager locationManager;
    public MyLocationListener listener;
    public Location previousBestLocation = null;
    boolean gps_enabled = false;
    boolean network_enabled = false;
    Intents intent =  new Intents();
    SessionHandler sessionHandler = new SessionHandler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        session = getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        if (sessionHandler.get_theme(session) != R.style.AppTheme) {
            setTheme(sessionHandler.get_theme(session));
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        endLatitude= Double.parseDouble(getIntent().getStringExtra("LAT"));
        endLongitude= Double.parseDouble(getIntent().getStringExtra("LNG"));
        emergencyID=  getIntent().getStringExtra("ID");
        name =   getIntent().getStringExtra("NAME");
        photo = getIntent().getStringExtra("PHOTO");
        ratingBar = findViewById(R.id.ratingBar);
        session = this.getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        floatingActionMenu = findViewById(R.id.fob_menu);
        floatingActionMenu.setClosedOnTouchOutside(true);
        emergencyPresenter = new EmergencyPresenter(session);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter("MapsActivity"));
        rateWithStars();

        SOS();
        //if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        //{
            //Initializing googleApiClient
            buildGoogleApiClient();
            CheckLocationPermission();
        //}
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

//        locationRequest = LocationRequest.create();
//        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//        locationRequest.setInterval(5 * 1000);
//        locationRequest.setFastestInterval(2 * 1000);
//        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
//        //**************************
//        builder.setAlwaysShow(true); //this is the key ingredient
//        //**************************
//        //Initializing googleApiClient
//        PendingResult<LocationSettingsResult> result =
//                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
//        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
//            @Override
//            public void onResult(@NonNull LocationSettingsResult result) {
//                final Status status = result.getStatus();
////                final LocationSettingsStates state = result.getLocationSettingsStates();
//                switch (status.getStatusCode()) {
//                    case LocationSettingsStatusCodes.SUCCESS:
//
//
//                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                        // Location settings are not satisfied. But could be fixed by showing the user
//                        // a dialog.
//                        try {
//                            // Show the dialog by calling startResolutionForResult(),
//                            // and check the result in onActivityResult().
//                            status.startResolutionForResult(
//                                    MapsActivity.this, 1000);
//                        } catch (IntentSender.SendIntentException e) {
//                            // Ignore the error.
//                        }
//                        break;
//                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//                        break;
//                }
//            }
//        });
//        //getNeabyPlaces("restaurant");
//       /* Intent SearchIntent = new Intent(MapsActivity.this, MainActivity.class);
//        SearchIntent.putExtra("LAT",latitude);
//        SearchIntent.putExtra("LNG",longitude);
//        startActivity(SearchIntent);*/
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        listener =new  MyLocationListener();
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        //don't start listeners if no provider is enabled
        while (!gps_enabled && !network_enabled) {
        }
        if (gps_enabled ) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10f, listener);
           // Toast.makeText( getApplicationContext(), "GPS Get location", Toast.LENGTH_SHORT ).show();
        }
        else if(network_enabled) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 10f, listener);
            //Toast.makeText( getApplicationContext(), "Network Get location", Toast.LENGTH_SHORT ).show();

        }
    }

    //SOS
    FloatingActionButton sos;
    FloatingActionButton ENums;
    private Dialog sos_dialog;
    private ListView sos_numbers;
    private Button sos_cancel;
    private EmergencyNumbersAdapter emergencyNumbersAdapter;
    private EmergencyNumbers emergencyNumbers = new EmergencyNumbers();
    private JsonParser jsonParser = new JsonParser();

    public void SOS() {
        // SOS
        Log.d(TAG,"SOS");
        sos = findViewById(R.id.rateFAB);
        sos_dialog = new Dialog(this, R.style.CustomDialogTheme);
        sos_dialog.setContentView(R.layout.sos);
        sos_numbers = sos_dialog.findViewById(R.id.sos_list);
        sos_cancel = sos_dialog.findViewById(R.id.sos_cancel);
        formHandler.show_dialog(sos, sos_dialog);
        formHandler.cancel_dialog(sos_cancel, sos_dialog);
        emergencyNumbers.getEmergencyNumbers(new FunctionCallback() {
            @Override
            public void onSuccess(String result) {
                emerNums = jsonParser.decodeEmergenciesNumbers(result);
                emergencyNumbersAdapter = new EmergencyNumbersAdapter(MapsActivity.this, emerNums);
                sos_numbers.setAdapter(emergencyNumbersAdapter);
            }

            @Override
            public void onError(String result) {
                Log.d(TAG, "Error: " + result);
            }
        });
        sos_numbers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final ExtraNumbers eNum =(ExtraNumbers) parent.getItemAtPosition(position);
                intent.call(eNum.getNumber().toString(),MapsActivity.this);

            }
        });

    }

    public void rateWithStars() {
        Log.d(TAG,"" + "Rating");
        // Initialize RatingBar
        ENums = findViewById(R.id.emergencyNumbers);
        rateDialog = new Dialog(MapsActivity.this, R.style.CustomDialogTheme);
        rateDialog.setContentView(R.layout.rating_bar);
        rateOk = rateDialog.findViewById(R.id.ok);
        rateCancel = rateDialog.findViewById(R.id.cancel);
        rateTitle = rateDialog.findViewById(R.id.number_wheel_title);
        rateTitle.setText("Rate Emergency Situation");
        formHandler.cancel_dialog(rateCancel, rateDialog);
        ratingBar = (RatingBar) rateDialog.findViewById(R.id.ratingBar);
        ratingBar.setStepSize((float)0.1);// to show to stars
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                Log.d("RateChange",ratingBar.getRating()+"");
                tempRate =  ratingBar.getRating()+"";
                //Toast.makeText(MapsActivity.this, String.valueOf(ratingBar.getRating()), Toast.LENGTH_SHORT).show();
            }
        });
        ENums.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rateDialog.show();
            }
        });
        getRate();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if(googleApiClient == null) {
                //Initializing googleApiClient
                buildGoogleApiClient();
            }
            mMap.setMyLocationEnabled(true);
        }
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMarkerDragListener(this);
    }

    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    public boolean CheckLocationPermission() {
        // Here, thisActivity is the current activity
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            // Should we show an explanation?
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION))
            {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION} , REQ_LOC_CODE);
            }
            else
            {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION} , REQ_LOC_CODE);
            }
            // MY_PERMISSIONS_REQUEST_FINE_LOCATION is an
            // app-defined int constant. The callback method gets the
            // result of the request.
            return false;
        }
        return true;
    }

    //Getting current location
    private void getCurrentLocation() {
        mMap.clear();
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            //Getting longitude and latitude
            longitude = location.getLongitude();
            latitude = location.getLatitude();
            //moving the map to location
            moveMap(longitude,latitude);
            //Toast.makeText(MapsActivity.this, longitude + " " + latitude, Toast.LENGTH_SHORT).show();
            //ReverseGecoding rg = new ReverseGecoding(this.getApplicationContext());
            Address = getCompleteAddressString(latitude, longitude);
        }
        getRoad(longitude,latitude,endLongitude,endLatitude);
    }

    public String getNeabyPlaces(String type) {
        mMap.clear();
        String url = getNearByUrl(longitude, latitude, type);
        Object dataTransfer[] = new Object[2];
        dataTransfer[0] = mMap;
        dataTransfer[1] = url;
        NearByPlacesData neabyPlaces = new NearByPlacesData();
        neabyPlaces.execute(dataTransfer);
        return type;
    }

    public String getNearByUrl(double longitude, double latitude, String type) {
        StringBuilder googlePlaceUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlaceUrl.append("location=" + latitude + "," + longitude);
        googlePlaceUrl.append("&radius=" + proximityRadius);
        googlePlaceUrl.append("&type=" + type);
        googlePlaceUrl.append("&sensor=true");
        googlePlaceUrl.append("&key=AIzaSyCKtLb0onHO0xTteMtXiMHmj9CCuBN_UFg");
        return googlePlaceUrl.toString();
    }

    public String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        // this.setContext(ma);
        String strAdd = "";
        StringBuilder strReturnedAddress = new StringBuilder("");
        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);

            if (addresses != null) {
                Address returnedAddress = addresses.get(0);

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Toast.makeText(this, "My Current location address : " + strAdd, Toast.LENGTH_SHORT).show();
                //  Log.w("My Current location address", strReturnedAddress.toString());
            } else {
                Toast.makeText(this, "My Current location address" + "No Address returned!", Toast.LENGTH_SHORT).show();
                // Log.w("My Current location address", "No Address returned!");
            }

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            //Toast.makeText(this, "Address : " + address, Toast.LENGTH_SHORT).show();
            String city = addresses.get(0).getLocality();
            //Toast.makeText(this, "City : " + city, Toast.LENGTH_SHORT).show();
            String state = addresses.get(0).getAdminArea();
            //Toast.makeText(this, "State : " + state, Toast.LENGTH_SHORT).show();
            String country = addresses.get(0).getCountryName();
            //Toast.makeText(this, "Country : " + country, Toast.LENGTH_SHORT).show();
            //String postalCode = addresses.get(0).getPostalCode();
            //Toast.makeText(this,"Postal Code : " + postalCode,Toast.LENGTH_SHORT).show();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
            //Toast.makeText(this, "Known Name : " + knownName, Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "My Current location address" + "Cannot get Address!", Toast.LENGTH_SHORT).show();
            //Log.w("My Current location address", "Cannot get Address!");
        }

        return strAdd;
    }

    public void getRoad(double longitude, double latitude,double newLongitude, double newLatitude) {
        //move to current position
        //moveMap(newLongitude,newLatitude);
        getDistance(longitude , latitude,newLongitude,newLatitude);
    }

    public float getDistance(double longitude, double latitude,final double newLongitude,final double newLatitude) {
//        final Button button = findViewById(R.id.search_button);
//        button.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(new LatLng(newLatitude,newLongitude));
        markerOptions.title(getCompleteAddressString(newLatitude,newLongitude));
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        float[] results = new float[10];
        Location.distanceBetween(longitude,latitude,newLongitude,newLatitude,results);
        markerOptions.snippet("Distance = "+results[0]/1000 + "KMs");
       // Toast.makeText(MapsActivity.this, "Distance = "+results[0]/1000 + " KMs" , Toast.LENGTH_SHORT).show();
        if(victimLocationMarker != null)
        {
            victimLocationMarker.remove();
        }
        victimLocationMarker = mMap.addMarker(markerOptions);
//            }
//        });
        getDirection(longitude,latitude,newLongitude,newLatitude);
        //getWalkingDirections();
        return results[0]/1000;
    }

    public void getDirection(double longitude, double latitude,double newLongitude, double newLatitude) {
        Object[] dataTransfer = new Object[3];
        String url = getDirectionUrl(longitude,latitude,newLongitude,newLatitude);
        dataTransfer[0] = mMap;
        dataTransfer[1] = url;
        dataTransfer[2] = new LatLng(newLatitude,newLongitude);
        getDirectionsData.removePolyLine();
        getDirectionsData = new GetDirectionsData();
        getDirectionsData.execute(dataTransfer);
        //Toast.makeText(this,getDirectionsData.polyLine + "***",  Toast.LENGTH_SHORT).show();
        //Toast.makeText(this,getDirectionsData.getDistance() + " " + getDirectionsData.getDuration(),  Toast.LENGTH_SHORT).show();
    }

    public String getDirectionUrl(double longitude, double latitude,double newLongitude, double newLatitude) {
        StringBuilder googleDirectionsUrl = new StringBuilder("https://maps.googleapis.com/maps/api/directions/json?");
        googleDirectionsUrl.append("origin="+latitude+","+longitude);
        googleDirectionsUrl.append("&destination="+newLatitude+","+newLongitude);
        googleDirectionsUrl.append("&key=AIzaSyC0fz9lVvVgPgnZbvHcqWvBjKmPKPuEBFA");//
        return googleDirectionsUrl.toString();
    }

    public void getWalkingDirections() {
        // Origin of route
        String str_origin = "origin=" + latitude + "," + longitude;

        // Destination of route
        String str_dest = "destination=" + endLatitude + "," + endLongitude;

        // Sensor enabled
        String sensor = "sensor=false";
        // Travelling mode enable
        // <strong>String mode = "mode = driving"</strong>
        String mode = "mode=walking";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&"+ mode;

        // Output format
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        try {
            String JSONdata = new DownloadURL().readURL(url);
           // Toast.makeText(MapsActivity.this, JSONdata, Toast.LENGTH_SHORT).show();
//            DataParser dataParser =  new DataParser();
//            dataParser.parse(JSONdata);
//            HashMap<String, String> data = new HashMap<>();
//            Toast.makeText(MapsActivity.this, data.get("distance"), Toast.LENGTH_SHORT).show();
//            Toast.makeText(MapsActivity.this, data.get("duration"), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void moveMap(double longi,double lat) {
        /**
         * Creating the latlng object to store lat, long coordinates
         * adding marker to map
         * move the camera with animation
         */

        myLoc = new LatLng(lat, longi);
        if(currentLocationMarker == null)
        {
        markerOpts = new MarkerOptions();
        markerOpts.position(myLoc);
        markerOpts.draggable(true);
        markerOpts.title(getCompleteAddressString(lat,longi));
        markerOpts.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
        currentLocationMarker = mMap.addMarker(markerOpts);
        }
        else
        {
            //currentLocationMarker.remove();
            currentLocationMarker.setPosition(myLoc);
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLng(myLoc));
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setRotateGesturesEnabled(true);
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        //new ReverseGeocodingTask(getBaseContext()).execute(myLoc);
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMapLongClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Log.v(TAG, "view click event");
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        // mMap.clear();
        //mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
       // Toast.makeText(MapsActivity.this, "onMarkerDragStart", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMarkerDrag(Marker marker) {
        //Toast.makeText(MapsActivity.this, "onMarkerDrag", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        // getting the Co-ordinates
        endLatitude = marker.getPosition().latitude;
        endLongitude = marker.getPosition().longitude;
        //Toast.makeText(MapsActivity.this, "onMarkerDragEnd  --> " + getCompleteAddressString(endLatitude,endLongitude), Toast.LENGTH_SHORT).show();

        //move to current position

        getRoad(longitude,latitude,endLongitude,endLatitude);
//        PolylineOptions options = new PolylineOptions();
//        options.color(Color.CYAN);
//        options.width(10);
//        options.add(new LatLng(endLatitude,endLongitude));
//        mMap.addPolyline(options);
    }

    @Override
    protected void onStart() {
        googleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.setDraggable(true);
        marker.showInfoWindow();
        //currentLocationMarker = marker;
        //Toast.makeText(MapsActivity.this, "onMarkerClick", Toast.LENGTH_SHORT).show();
        return true;
    }

    @Override
    public void onLocationChanged(Location location) {
        //Toast.makeText(this, "LocationChanged", Toast.LENGTH_SHORT).show();
        //       if(!locationFlag)
//       {
//           getCurrentLocation();
//           previousLocation = new LatLng(latitude,longitude);
//           locationFlag = true;
//           getRoad(endLongitude,endLatitude);
//       }
        //getCurrentLocation();
        currentLocationMarker.setPosition(new LatLng(location.getLatitude(),location.getLongitude()));
        //moveMap(location.getLongitude(),location.getLatitude());
        //Address = getCompleteAddressString(latitude, longitude);
        getRoad(location.getLongitude(),location.getLatitude(),endLongitude,endLatitude);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode)
        {
            case REQ_LOC_CODE:
                if(grantResults.length > 0 && grantResults[0] ==  PackageManager.PERMISSION_GRANTED) {
                    //permission is granted
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                    {
                        if(googleApiClient == null)
                        {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }
                }
                else
                {
                    Toast.makeText(this,"Permission Denied " , Toast.LENGTH_LONG).show();
                    //this is to display a msg-->permission is denied
                }
                return;
        }

    }

    private void getRate() {
        rateOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharedRate = tempRate;
                //Toast.makeText(MapsActivity.this, "Shared Rate : " + sharedRate, Toast.LENGTH_SHORT).show();
                if(sharedRate == null) sharedRate ="0";
                emergencyPresenter.rateEmergency(MapsActivity.this, emergencyID , Float.parseFloat(sharedRate));
                rateDialog.dismiss();
            }
        });
    }

    private void rateListener() {
        rateWheel.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int old_number, int new_number) {
                tempRate = numberPicker.getDisplayedValues()[new_number];
            }
        });
    }

    public class MyLocationListener implements android.location.LocationListener
    {

        public void onLocationChanged(final Location loc)
        {
            currentLocationMarker.setPosition(new LatLng(loc.getLatitude(),loc.getLongitude()));
            location=loc;
            getRoad(loc.getLongitude(),loc.getLatitude(),endLongitude,endLatitude);
            getApplicationContext().getSharedPreferences("currentUser",Context.MODE_MULTI_PROCESS).edit().putString("currentLat", loc.getLatitude()+"").apply();
            getApplicationContext().getSharedPreferences("currentUser",Context.MODE_MULTI_PROCESS).edit().putString("currentLng", loc.getLongitude()+"").apply();
            getApplicationContext().getSharedPreferences("currentUser",Context.MODE_MULTI_PROCESS).edit().putString("address", getCompleteAddressString(loc.getLatitude(),loc.getLongitude())).apply();
        }

        public void onProviderDisabled(String provider)
        {
            //Toast.makeText( getApplicationContext(), "Gps Disabled", Toast.LENGTH_SHORT ).show();
        }


        public void onProviderEnabled(String provider)
        {
            //Toast.makeText( getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();
        }


        public void onStatusChanged(String provider, int status, Bundle extras)
        {

        }

    }
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String json = intent.getStringExtra("json");
            Emergency emergency = jsonParser.decodeEmergencyNotification(json);
            Toast.makeText(getApplicationContext(), "Location Changed", Toast.LENGTH_SHORT).show();
            if(emergency.getId().equals(emergencyID)){
                //Toast.makeText(getApplicationContext(), "Victim Location Chnaged: "+emergency.getLocation().getLongitude()+" " +emergency.getLocation().getLatitude()+"" ,Toast.LENGTH_SHORT).show();
                victimLocationMarker.setPosition(new LatLng(emergency.getLocation().getLatitude(),emergency.getLocation().getLongitude()));
                getRoad(location.getLongitude(),location.getLatitude(),emergency.getLocation().getLongitude(),emergency.getLocation().getLatitude());
            }
        }
    };

}

