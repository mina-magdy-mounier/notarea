package com.example.lenovo.notarea.Model;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Presenter.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class Advertisement extends Post {

    final private String TAG = "Advertisement";

    private String ads_Img;
    private String adsID;
    private int daysOfAD;
    private ArrayList<Category> categories;

    public Advertisement() {
    }

    public Advertisement(String title, String postBody, String userFeeling, int userFeelingImg, int numberOfLikes, int numberOfDislikes, int isLiked, int isDisliked, int isSaved, Attachment attachment, String userID, User user, Location location, String adsImg, int distance) {
        super(title, postBody, userFeeling, userFeelingImg, numberOfLikes, numberOfDislikes, isLiked, isDisliked, isSaved, attachment, userID, user, location, distance);
        this.ads_Img = adsImg;
    }

    public Advertisement(String title, String postBody, String userFeeling, int userFeelingImg, int numberOfLikes, int numberOfDislikes, int isLiked, int isDisliked, int isSaved, Attachment attachment, String userID, User user, Location location, String adsImg, int distance, int daysOfAD, ArrayList<Category> categories) {
        super(title, postBody, userFeeling, userFeelingImg, numberOfLikes, numberOfDislikes, isLiked, isDisliked, isSaved, attachment, userID, user, location, distance);
        this.ads_Img = adsImg;
        this.categories = categories;
        this.daysOfAD = daysOfAD;
    }

    // ===========================  Setters & Getters ============================================//

    public String getAdsID() {
        return adsID;
    }

    public void setAdsID(String id) {
        this.adsID = id;
    }

    public int getDaysOfAD() {
        return daysOfAD;
    }

    public void setDaysOfAD(int daysOfAD) {
        this.daysOfAD = daysOfAD;
    }

    public String getAds_Img() {
        return ads_Img;
    }

    public void setAds_Img(String ads_Img) {
        this.ads_Img = ads_Img;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }

    public ArrayList<Category> getCategories() {
        return categories;
    }

    // ===========================  CRUD ============================================//

    public void addAds(JSONObject postJson, String currentUserId, final ServerCallback callback) {
        try {
            String url = URL + "api/notarea/users/" + currentUserId + "/advertisments";
            Log.d(TAG, "addAds: " + postJson);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                    (Request.Method.POST, url, postJson, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject responsee) {
                            Log.d(TAG, "addAds response: " + responsee.toString());
                            callback.onSuccess(responsee.toString());
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d(TAG, error.toString());
                        }
                    });
            Singleton.getInstance(super.getContext()).addToRequestQueue(jsonObjectRequest);
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
    }

    public void getADS(JSONObject jsonObject, final ServerCallback callback) {
        String url = URL + "api/notarea/users/nearestadvertisments";
        Log.d(TAG, "getADS: " + jsonObject);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject responsee) {
                        Log.d(TAG, "getADS response: " + responsee);
                        callback.onSuccess(responsee.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse response = error.networkResponse;
                        if (response != null && response.data != null) {
                            switch (response.statusCode) {
                                case 404:
                                    callback.onSuccess("error");
                                    break;
                                case 500:
                                    callback.onSuccess("php_error");
                                    break;
                            }
                        }
                    }
                });
        Singleton.getInstance(super.getContext()).addToRequestQueue(jsonObjectRequest);
    }

    public void editAds(String userID, String postID, JSONObject postJson, final ServerCallback callback) {
        try {
            String url = URL + "api/notarea/users/" + userID + "/advertisments/" + postID;
            Log.d(TAG, "editAds: " + postJson);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                    (Request.Method.PUT, url, postJson, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject responsee) {
                            Log.d(TAG, "editAds response: " + responsee.toString());
                            callback.onSuccess(responsee.toString());
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            NetworkResponse response = error.networkResponse;
                            if (response != null && response.data != null) {
                                switch (response.statusCode) {
                                    case 404:
                                        callback.onSuccess("error");
                                        break;
                                    case 500:
                                        callback.onSuccess("php_error");
                                        break;
                                }
                            }
                        }
                    });
            Singleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
    }

    @Override
    public void getUserPosts(String currentUserID, final ServerCallback callback) {
        String url = URL + "api/notarea/users/" + currentUserID + "/useradvertisments";
        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray responsee) {
                        Log.d(TAG, "getUserPosts response: " + responsee.toString());
                        callback.onSuccess(responsee.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse response = error.networkResponse;
                        if (response != null && response.data != null) {
                            switch (response.statusCode) {
                                case 404:
                                    callback.onSuccess("error");
                                    break;

                                case 500:
                                    callback.onSuccess("php_error");
                                    break;
                            }
                        }
                    }
                });
        Singleton.getInstance(super.getContext()).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    public void getUserSavedPosts(String currentUserID, final ServerCallback callback) {
        String url = URL + "api/notarea/users/" + currentUserID + "/savedads";
        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray responsee) {
                        Log.d(TAG, "getUserSavedPosts response: " + responsee.toString());
                        callback.onSuccess(responsee.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse response = error.networkResponse;
                        if (response != null && response.data != null) {
                            switch (response.statusCode) {
                                case 404:
                                    callback.onSuccess("error");
                                    break;

                                case 500:
                                    callback.onSuccess("php_error");
                                    break;
                            }
                        }
                    }
                });
        Singleton.getInstance(super.getContext()).addToRequestQueue(jsonObjectRequest);
    }

    public void deleteAds(String currentUserID, String advertismentID, final ServerCallback callback) {
        try {
            String url = URL + "api/notarea/users/" + currentUserID + "/advertisments/" + advertismentID;
            StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d(TAG, "deleteAds response: " + response);
                    callback.onSuccess(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, error.toString());
                }
            });
            Singleton.getInstance(super.getContext()).addToRequestQueue(stringRequest);
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
    }

}
