package com.example.lenovo.notarea.View.fragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.notarea.Model.Location;
import com.example.lenovo.notarea.Presenter.Maps.MyLocationActivity;
import com.example.lenovo.notarea.Presenter.UserPreseneter;
import com.example.lenovo.notarea.Presenter.view_model_adapters.FamilyListAdapter;
import com.example.lenovo.notarea.R;

import java.util.List;
import java.util.Locale;


public class UserInfoFragment extends Fragment {

    ListView familyList;
    TextView userName, userGender, userAge, userLocation, userMail, userPhone, noFamilyNumbers, setting;
    SharedPreferences session;
    UserPreseneter userPreseneter;
    ImageView userProfileImg;
    FamilyListAdapter familyListAdapter;
    Animation animation;
    PopupMenu popupMenu;

    Boolean isPopUpMenuShown = false;

    public UserInfoFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_info, container, false);

        session = view.getContext().getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        userPreseneter = new UserPreseneter(session);
        userProfileImg = view.findViewById(R.id.userProfileImg);
        familyList = view.findViewById(R.id.family_list);
        userName = view.findViewById(R.id.user_name);
        userGender = view.findViewById(R.id.user_gender);
        userAge = view.findViewById(R.id.user_age);
        userLocation = view.findViewById(R.id.user_location);
        final TextView button = view.findViewById(R.id.user_location);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent myLocIntent = new Intent(getActivity().getApplicationContext(), MyLocationActivity.class);
                startActivity(myLocIntent);
            }
        });
        userMail = view.findViewById(R.id.user_mail);
        userPhone = view.findViewById(R.id.user_phone);
        noFamilyNumbers = view.findViewById(R.id.noFamilyNumbers);
        setting = view.findViewById(R.id.user_info_settings);
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_setting);


        userPreseneter.initUserProfile(userProfileImg, userName, userGender, userAge, userLocation, userMail, userPhone , getActivity().getApplicationContext());

        if (userPreseneter.getUser().getFamilyNumbers() != null) {
            familyListAdapter = new FamilyListAdapter(getActivity(), userPreseneter.getUser().getFamilyNumbers());
            familyList.setAdapter(familyListAdapter);
            familyList.setVisibility(View.VISIBLE);
            noFamilyNumbers.setVisibility(View.GONE);
        } else {
            familyList.setVisibility(View.GONE);
            noFamilyNumbers.setVisibility(View.VISIBLE);
        }
        setting();

        return view;
    }

    private void setting() {
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setting.startAnimation(animation);
                if (!isPopUpMenuShown) {
                    popupMenu.show();
                } else {
                    popupMenu.dismiss();
                }
            }
        });
    }


}
