package com.example.lenovo.notarea.FireBase;


import android.app.Activity;
import android.net.Uri;
import android.util.Log;

import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Constants;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;


public class FirebaseStorageService {
    private final String TAG = "FirebaseStorageService";
    private FirebaseStorage firebaseStorage;
    private StorageReference storageReference;
    private Activity activity;
    private String link = null;
    private Constants constants = new Constants();


    public FirebaseStorageService() {
        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference(constants.getFIREBASE_STORAGE());
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }


    public void get_photo_local_uri(Uri uri, final ServerCallback serverCallback) {
        final StorageReference temp = storageReference.child(uri.getLastPathSegment());
        temp.putFile(uri).addOnSuccessListener(activity, new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(final UploadTask.TaskSnapshot taskSnapshot) {
                temp.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        serverCallback.onSuccess(uri.toString());
                    }
                });
            }
        });
    }
}
