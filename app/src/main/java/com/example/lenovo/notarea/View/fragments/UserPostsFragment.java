package com.example.lenovo.notarea.View.fragments;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.lenovo.notarea.R;
import com.example.lenovo.notarea.View.adapters.UserPostsAdapter;

public class UserPostsFragment extends Fragment {

    ViewPager userPosts;
    TabLayout postsType;
    UserPostsAdapter userPostsAdapter;

    public UserPostsFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_posts, container, false);

        userPosts = view.findViewById(R.id.userPostsViewPager);
        postsType = view.findViewById(R.id.userPosts);

        userPostsAdapter = new UserPostsAdapter(getFragmentManager());
        userPosts.setAdapter(userPostsAdapter);

        userPosts.setOffscreenPageLimit(2);
        postsType.setTabMode(TabLayout.MODE_FIXED);
        postsType.setTabGravity(TabLayout.GRAVITY_FILL);
        postsType.setupWithViewPager(userPosts);

        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.refresh, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
}
