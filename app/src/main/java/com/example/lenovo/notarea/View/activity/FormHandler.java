package com.example.lenovo.notarea.View.activity;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.notarea.CallBacks.FunctionCallback;
import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Presenter.Validation.UserValidation;
import com.example.lenovo.notarea.R;
import com.github.clans.fab.FloatingActionButton;

import java.util.ArrayList;


public class FormHandler {

    final int black = Color.rgb(77, 136, 255);
    private Menu menu;
    private UserValidation userValidation = new UserValidation();


    public FormHandler() {
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public static void expand(final View view) {
        view.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = view.getMeasuredHeight();
        view.getLayoutParams().height = 1;
        view.setVisibility(View.VISIBLE);
        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                view.getLayoutParams().height = interpolatedTime == 1 ? LinearLayout.LayoutParams.WRAP_CONTENT : (int) (targetHeight * interpolatedTime);
                view.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        animation.setDuration(200);
        view.startAnimation(animation);
    }

    public static void collapse(final View view) {
        final int initialHeight = view.getMeasuredHeight();
        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    view.setVisibility(View.GONE);
                } else {
                    view.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    view.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        animation.setDuration(200);
        view.startAnimation(animation);
    }

    public void onWritingTitle(final EditText postTilte) {
        final float textSize = postTilte.getTextSize();
        postTilte.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                postTilte.setTypeface(Typeface.DEFAULT);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                postTilte.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (postTilte.getText().toString().equals("")) {
                    postTilte.setTypeface(Typeface.DEFAULT);
                } else {
                    postTilte.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                }
            }
        });
    }


    public void onEditingTitle(final EditText postTilte, final EditText postBody, final Boolean[] isEdited) {
        final float textSize = postTilte.getTextSize();
        postTilte.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                postTilte.setTypeface(Typeface.DEFAULT);
                disableOption(R.id.action_name);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                postTilte.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (postTilte.getText().toString().equals("")) {
                    postTilte.setTypeface(Typeface.DEFAULT);
                } else {
                    postTilte.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                }
                if (!postBody.getText().toString().equals("")) {
                    isEdited[0] = true;
                    enableOption(R.id.action_name);
                }
            }
        });
    }

    // track user writing in the editView.
    public void onWritingPost(final EditText postBody) {

        postBody.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                disableOption(R.id.action_name);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!postBody.getText().toString().equals("")) {
                    enableOption(R.id.action_name);
                } else {
                    disableOption(R.id.action_name);
                }
            }
        });

    }


    public void onEditingPost(final EditText postBody, final Boolean isEdited[]) {
        postBody.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                disableOption(R.id.action_name);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                isEdited[1] = true;
                if (!postBody.getText().toString().equals("")) {
                    enableOption(R.id.action_name);
                } else {
                    disableOption(R.id.action_name);
                }
            }
        });

    }

    // enable post button on the nav bar
    void enableOption(int id) {
        MenuItem item = menu.findItem(id);
        item.setEnabled(true);
    }

    // disable post button on the nav bar
    void disableOption(int id) {
        MenuItem item = menu.findItem(id);
        item.setEnabled(false);
    }


    public void show_dialog(FloatingActionButton f, final Dialog d) {
        f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.show();
            }
        });
    }

    public void show_dialog(android.support.design.widget.FloatingActionButton f, final Dialog d) {
        f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.show();
            }
        });
    }

    public void canEdit(EditText postBody) {
        if (!postBody.getText().toString().equals("")) {
            enableOption(R.id.action_name);
        }
    }


    public void showUserFeeling(final ImageView arrow, boolean isExpanded, final LinearLayout userFeelingSection) {
        final boolean[] finalExpaned = {isExpanded};
        arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!finalExpaned[0]) {
                    expand(userFeelingSection);
                    finalExpaned[0] = true;
                } else {
                    collapse(userFeelingSection);
                    finalExpaned[0] = false;
                }
            }
        });
    }

    public void set_wheel(int MAX_DISTANCE, int MIN_DISTANCE, int DISTANCE_STEP, NumberPicker number_wheel) {
        String DISTANCES[] = new String[((MAX_DISTANCE - MIN_DISTANCE) / DISTANCE_STEP) + 1];
        int tmp = MIN_DISTANCE;
        for (int i = 0; i <= ((MAX_DISTANCE - MIN_DISTANCE) / DISTANCE_STEP); i++) {
            DISTANCES[i] = Integer.toString(tmp);
            tmp += DISTANCE_STEP;
        }
        number_wheel.setMinValue(0);
        number_wheel.setMaxValue(((MAX_DISTANCE - MIN_DISTANCE) / DISTANCE_STEP));
        number_wheel.setDisplayedValues(DISTANCES);
    }

    public void cancel_dialog(Button b, final Dialog d) {
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.dismiss();
            }
        });
    }

    public void show_dialog(Button b, final Dialog d) {
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.show();
            }
        });
    }

    public void addLocation(FloatingActionButton location, final TextView locationAttachment, final EditText postBody, final Boolean isEdited[]) {
        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (locationAttachment.getVisibility() != View.VISIBLE) {
                    locationAttachment.setVisibility(View.VISIBLE);
                    if (isEdited != null) isEdited[4] = true;
                }
                canEdit(postBody);
            }
        });
    }

    public void cancelLocationAttachment(final TextView locationAttachment, final Boolean isEdited[], final EditText postBody) {
        locationAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                locationAttachment.setVisibility(View.GONE);
                if (isEdited != null) isEdited[4] = true;
                canEdit(postBody);
            }
        });
    }

    public void showAttachmentPopUp(Dialog phoneListDialog) {
        phoneListDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        phoneListDialog.show();
    }

    public void closePhoneAttachment(Button closeAttachment, final Dialog phoneListDialog) {
        closeAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                phoneListDialog.dismiss();
            }
        });
    }

    public void addPhone(final ListView phoneList, final Dialog phoneListDialog, FloatingActionButton phone, final ArrayList<String> attachedBusinessNumbersIDs, final LinearLayout contact_my_layout, final Context context, final Boolean isEdited[]) {
        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAttachmentPopUp(phoneListDialog);
                attachedPhonesListiner(phoneList, attachedBusinessNumbersIDs, contact_my_layout, context, isEdited);
            }
        });
    }

    public void attachedPhonesListiner(ListView phoneList, final ArrayList<String> attachedBusinessNumbersIDs, final LinearLayout contact_my_layout, final Context context, final Boolean isEdited[]) {
        phoneList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView id = view.findViewById(R.id.id);
                TextView name = view.findViewById(R.id.name);
                TextView number = view.findViewById(R.id.number);
                if (!attachedBusinessNumbersIDs.contains(id.getText().toString())) {
                    attachedBusinessNumbersIDs.add(id.getText().toString());
                    if (isEdited != null) {
                        enableOption(R.id.action_name);
                        isEdited[3] = true;
                    }
                    addPhoneView(id.getText().toString(), name.getText().toString(), number.getText().toString(), attachedBusinessNumbersIDs, contact_my_layout, context, isEdited);
                } else {
                    Toast.makeText(context.getApplicationContext(), name.getText().toString() + " already added.", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void addPhoneView(final String id, final String name, final String number, final ArrayList<String> attachedBusinessNumbersIDs, LinearLayout contact_my_layout, Context context, final Boolean isEdited[]) {
        final TextView phone = new TextView(context.getApplicationContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        phone.setLayoutParams(layoutParams);
        phone.setBackgroundResource(R.drawable.custome_shadow);
        phone.setCompoundDrawablePadding(5);
        phone.setTextColor(black);
        phone.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close_black_15dp, 0);
        phone.setPadding(15, 15, 10, 15);
        layoutParams.setMargins(4, 0, 4, 0);
        phone.setText(name);
        phone.setShadowLayer(0f, -1, 1, Color.BLUE);
        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                phone.setVisibility(View.GONE);
                attachedBusinessNumbersIDs.remove(id);
                if (isEdited != null) {
                    isEdited[3] = true;
                    enableOption(R.id.action_name);
                }
            }
        });
        contact_my_layout.addView(phone);
    }

    public void addFaceBook(final Dialog dFaceBook, final EditText eFacebook, FloatingActionButton facebook, final Button addFaceBookBtn, final TextView faceBookAttachment, final FunctionCallback callback) {
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dFaceBook.show();
                addFaceBookBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String error = userValidation.validate_facebook_url(eFacebook.getText().toString());
                        if (error == null) {
                            callback.onSuccess(eFacebook.getText().toString());
                            faceBookAttachment.setVisibility(View.VISIBLE);
                            dFaceBook.dismiss();
                        } else {
                            callback.onError(error);
                        }
                    }
                });
            }
        });
    }

    public void cancelFaceBookAttachment(final TextView faceBookAttachment, final FunctionCallback callback) {
        faceBookAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    faceBookAttachment.setVisibility(View.GONE);
                    callback.onSuccess(null);
                } catch (Exception e) {
                    callback.onError("error");
                }

            }
        });
    }

    public void setBorder(TextView t1, TextView t2, TextView t3, TextView t4) {
        t1.setBackgroundResource(R.drawable.blue_border);
        t2.setBackgroundResource(0);
        t3.setBackgroundResource(0);
        t4.setBackgroundResource(0);
    }
    public void removeBorder(TextView t1) {
        t1.setBackgroundResource(0);
    }

}
