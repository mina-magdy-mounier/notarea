package com.example.lenovo.notarea.View.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.lenovo.notarea.CallBacks.FunctionCallback;
import com.example.lenovo.notarea.Intents;
import com.example.lenovo.notarea.Model.ExtraNumbers;
import com.example.lenovo.notarea.Presenter.EmergencyPresenter;
import com.example.lenovo.notarea.Presenter.JsonParser;
import com.example.lenovo.notarea.Presenter.SessionHandler;
import com.example.lenovo.notarea.Presenter.view_model_adapters.EmergencyNumbersAdapter;
import com.example.lenovo.notarea.R;

import java.util.ArrayList;

public class EmergencyNumbers extends AppCompatActivity {

    private final String TAG = "EmergencyNumbers";
    public ListView emergency_list;
    public EmergencyNumbersAdapter emergencyNumbersAdapter;
    private Intents intents = new Intents();
    private com.example.lenovo.notarea.Presenter.EmergencyNumbers emergencyNumbers = new com.example.lenovo.notarea.Presenter.EmergencyNumbers();
    private JsonParser jsonParser = new JsonParser();
    private SessionHandler sessionHandler = new SessionHandler();
    private SharedPreferences session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        if (sessionHandler.get_theme(session) != R.style.AppTheme) {
            setTheme(sessionHandler.get_theme(session));
        }
        setContentView(R.layout.emergency_numbers);

        emergency_list = findViewById(R.id.emergency_numbers_list);
        emergencyNumbers.setContext(EmergencyNumbers.this);

        init();
        emergency_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ExtraNumbers extraNumbers1 = (ExtraNumbers) adapterView.getItemAtPosition(i);
                intents.call(extraNumbers1.getNumber(), EmergencyNumbers.this);
            }
        });
    }

    private void init() {
        emergencyNumbers.getEmergencyNumbers(new FunctionCallback() {
            @Override
            public void onSuccess(String result) {
                emergencyNumbersAdapter = new EmergencyNumbersAdapter(EmergencyNumbers.this, jsonParser.decodeEmergenciesNumbers(result));
                emergency_list.setAdapter(emergencyNumbersAdapter);
            }

            @Override
            public void onError(String result) {
                switch (result) {
                    case "error":
                        break;

                    case "php_error":
                        break;
                }

            }
        });
    }


}
