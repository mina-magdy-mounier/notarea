package com.example.lenovo.notarea.View.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.lenovo.notarea.View.fragments.UserSavedADSPosts;
import com.example.lenovo.notarea.View.fragments.UserSavedGPosts;

public class SavedPostsAdpater extends FragmentPagerAdapter {


    public SavedPostsAdpater(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0)
            return new UserSavedGPosts();
        else return new UserSavedADSPosts();
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 0)
            return "General";
        else return "Ads";

    }
}
