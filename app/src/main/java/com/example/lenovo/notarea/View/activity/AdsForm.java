package com.example.lenovo.notarea.View.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;

import com.example.lenovo.notarea.CallBacks.FunctionCallback;
import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Constants;
import com.example.lenovo.notarea.FireBase.FirebaseStorageService;
import com.example.lenovo.notarea.Model.Category;
import com.example.lenovo.notarea.Presenter.SessionHandler;
import com.example.lenovo.notarea.Presenter.UserPreseneter;
import com.example.lenovo.notarea.Presenter.view_model_adapters.BusinessListAdapter;
import com.example.lenovo.notarea.Presenter.view_model_adapters.CategoryAdapter;
import com.github.clans.fab.FloatingActionButton;

import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.notarea.Presenter.AdsPresenter;
import com.example.lenovo.notarea.R;
import com.github.clans.fab.FloatingActionMenu;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;

public class AdsForm extends AppCompatActivity {

    private Constants constants = new Constants();

    private int MAX_DISTANCE = constants.getADS_MAX_DISTANCE();
    private int MIN_DISTANCE = constants.getADS_MIN_DISTANCE();
    private int DISTANCE_STEP = constants.getADS_DISTANCE_STEP();

    // Days of ADS
    private int MAX_DAYS = constants.getADS_MAX_DAYS();
    private int MIN_DAYS = constants.getADS_MIN_DAYS();
    private int DAYS_STEP = constants.getADS_DAYS_STEP();

    Uri ADS_IMG = null;
    FirebaseStorageService firebaseStorageService = new FirebaseStorageService();

    EditText postTitle, postBody, faceBook;
    Menu menu;
    TextView happy, sad, intrested, shocked;
    public TextView faceBookAttachment, locationAttachment, phoneAttachment, distance_wheel_title, days_wheel_title, no_category;
    FloatingActionMenu fabMenu;
    // category
    FloatingActionButton image_picker, facebook, location, phone, cancelImg, distance, daysOFAdsBtn, categoryBtn;
    ImageView adsImg, arrow;
    // Days of ADS
    NumberPicker distance_wheel, days_wheel;
    // category
    Button closeAttachment, addFaceBookBtn, distance_wheel_ok, distance_wheel_cancel, days_wheel_ok, days_wheel_cancel, cancelFaceBookBtn, category_wheel_ok, category_wheel_cancel;
    // category
    LinearLayout userFeelingSection, contact_my_layout, category_parent;
    public ProgressBar adding_in_progress;
    int feelingEmoji = 0;
    String feelingDesc = "", FaceBook = null, shared_distance = String.valueOf(MIN_DISTANCE), temp_distance = null, temp_days = null, days_of_ads = String.valueOf(MIN_DAYS);
    Bitmap AdsImgBitMap = null;
    boolean isExpanded = true,
            addLocation = false;
    // Days of ADS
    Dialog phoneListDialog, addFacebookUrlDialog, distanceDialog, daysOfAdsDialog, categoryDialog;
    public ListView phoneList, categoryList;
    ArrayList<String> attachedBusinessNumbersIDs = new ArrayList<>();
    UserPreseneter userPreseneter;
    FormHandler formHandler;
    AdsPresenter adsPresenter;
    SharedPreferences session;
    BusinessListAdapter businessListAdapter;
    public CategoryAdapter categoryAdapter;
    SessionHandler sessionHandler = new SessionHandler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        if (sessionHandler.get_theme(session) != R.style.AppTheme) {
            setTheme(sessionHandler.get_theme(session));
        }
        firebaseStorageService.setActivity(AdsForm.this);
        setContentView(R.layout.ads_post_form);
//        session = getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        userPreseneter = new UserPreseneter(session);
        formHandler = new FormHandler();
        adsPresenter = new AdsPresenter(session);
        phoneListDialog = new Dialog(this, R.style.CustomDialogTheme);
        phoneListDialog.setContentView(R.layout.phone_attachment);
        phoneList = phoneListDialog.findViewById(R.id.phoneAttachment);
        contact_my_layout = findViewById(R.id.contact_my_layout);
        closeAttachment = phoneListDialog.findViewById(R.id.close_attachment);
        addFacebookUrlDialog = new Dialog(this, R.style.CustomDialogTheme);
        addFacebookUrlDialog.setContentView(R.layout.add_facebook_url);
        cancelFaceBookBtn = addFacebookUrlDialog.findViewById(R.id.cancelFaceBookBtn);
        faceBook = addFacebookUrlDialog.findViewById(R.id.facebookUrl);
        addFaceBookBtn = addFacebookUrlDialog.findViewById(R.id.addFaceBookBtn);
        postBody = findViewById(R.id.post_body_input);
        happy = findViewById(R.id.happy);
        sad = findViewById(R.id.sad);
        intrested = findViewById(R.id.intrested);
        shocked = findViewById(R.id.shocked);
        adsImg = findViewById(R.id.adsImg);
        userFeelingSection = findViewById(R.id.user_feeling_section);
        arrow = findViewById(R.id.userFeelingToggle);
        image_picker = findViewById(R.id.image_picker);
        facebook = findViewById(R.id.facebook);
        location = findViewById(R.id.location);
        phone = findViewById(R.id.phoneNumber);
        faceBookAttachment = findViewById(R.id.add_facebook);
        locationAttachment = findViewById(R.id.add_location);
        phoneAttachment = findViewById(R.id.add_phone);
        postTitle = findViewById(R.id.post_title);
        fabMenu = findViewById(R.id.fob_menu);
        adding_in_progress = findViewById(R.id.adding_post_progress_bar);

        adding_in_progress.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.loading), PorterDuff.Mode.MULTIPLY);

        if (userPreseneter.getUser().getBusinessNumbers() != null || userPreseneter.getUser().getBusinessNumbers().size() != 0) {
            businessListAdapter = new BusinessListAdapter(getApplicationContext(), userPreseneter.getUser().getBusinessNumbers());
            phoneList.setAdapter(businessListAdapter);
        }

        PickImage();
        FormHandler.collapse(userFeelingSection);
        formHandler.showUserFeeling(arrow, isExpanded, userFeelingSection);
        formHandler.addLocation(location, locationAttachment, postBody, null);
        formHandler.addPhone(phoneList, phoneListDialog, phone, attachedBusinessNumbersIDs, contact_my_layout, this, null);
        formHandler.cancelLocationAttachment(locationAttachment, null, postBody);
        formHandler.closePhoneAttachment(closeAttachment, phoneListDialog);
        formHandler.onWritingPost(postBody);
        formHandler.onWritingTitle(postTitle);
        formHandler.addFaceBook(addFacebookUrlDialog, faceBook, facebook, addFaceBookBtn, faceBookAttachment, new FunctionCallback() {
            @Override
            public void onSuccess(String result) {
                if (result != null) {
                    FaceBook = result;
                }
            }

            @Override
            public void onError(String result) {
                if (result != null) {
                    FaceBook = null;
                    Toast.makeText(AdsForm.this, result, Toast.LENGTH_LONG).show();
                }
            }
        });
        formHandler.cancel_dialog(cancelFaceBookBtn, addFacebookUrlDialog);
        formHandler.cancelFaceBookAttachment(faceBookAttachment, new FunctionCallback() {
            @Override
            public void onSuccess(String result) {
                if (result == null) {
                    FaceBook = null;
                    faceBook.setText("");
                }
            }

            @Override
            public void onError(String result) {
                if (result.equals("error"))
                    Toast.makeText(AdsForm.this, "somthing wrong happened", Toast.LENGTH_LONG).show();
            }
        });
        feelingListener();
        fabMenu.setClosedOnTouchOutside(true);

        // Distance Wheel
        distanceDialog = new Dialog(this, R.style.CustomDialogTheme);
        distanceDialog.setContentView(R.layout.number_wheel);
        distance = findViewById(R.id.distance_picker);
        distance_wheel = distanceDialog.findViewById(R.id.number_picker);
        distance_wheel_ok = distanceDialog.findViewById(R.id.ok);
        distance_wheel_cancel = distanceDialog.findViewById(R.id.cancel);
        distance_wheel_title = distanceDialog.findViewById(R.id.number_wheel_title);
        distance_wheel_title.setText("Choose distance in Meter");
        formHandler.cancel_dialog(distance_wheel_cancel, distanceDialog);
        formHandler.set_wheel(MAX_DISTANCE, MIN_DISTANCE, DISTANCE_STEP, distance_wheel);
        formHandler.show_dialog(distance, distanceDialog);
        get_distance();
        distance_listener();

        // Days of ADS
        daysOfAdsDialog = new Dialog(this, R.style.CustomDialogTheme);
        daysOfAdsDialog.setContentView(R.layout.number_wheel);
        daysOFAdsBtn = findViewById(R.id.days_of_ads);
        days_wheel = daysOfAdsDialog.findViewById(R.id.number_picker);
        days_wheel_ok = daysOfAdsDialog.findViewById(R.id.ok);
        days_wheel_cancel = daysOfAdsDialog.findViewById(R.id.cancel);
        days_wheel_title = daysOfAdsDialog.findViewById(R.id.number_wheel_title);
        days_wheel_title.setText("Choose days that ads remains by days");
        formHandler.show_dialog(daysOFAdsBtn, daysOfAdsDialog);
        formHandler.cancel_dialog(days_wheel_cancel, daysOfAdsDialog);
        formHandler.set_wheel(MAX_DAYS, MIN_DAYS, DAYS_STEP, days_wheel);
        days_wheel_listener();
        get_days_of_ads();

        //category
        categoryDialog = new Dialog(this, R.style.CustomDialogTheme);
        categoryDialog.setContentView(R.layout.category);
        categoryList = categoryDialog.findViewById(R.id.category_list_view);
        no_category = categoryDialog.findViewById(R.id.no_category);
        categoryBtn = findViewById(R.id.category_btn);
        category_wheel_ok = categoryDialog.findViewById(R.id.cok);
        category_wheel_cancel = categoryDialog.findViewById(R.id.ccancel);
        category_parent = categoryDialog.findViewById(R.id.category_check_boxs);
        formHandler.show_dialog(categoryBtn, categoryDialog);
        formHandler.cancel_dialog(category_wheel_cancel, categoryDialog);
        formHandler.cancel_dialog(category_wheel_ok, categoryDialog);
        adsPresenter.initCategory(AdsForm.this);

    }

    public void PickImage() {
        image_picker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AdsImgBitMap == null) {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("image/jpeg");
                    intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                    startActivityForResult(intent, 101);
                } else {
                    AdsImgBitMap = null;
                    ADS_IMG = null;
                    adsImg.setVisibility(View.GONE);
                    image_picker.setImageResource(R.drawable.ic_insert_photo_white_20dp);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == 101) {
                Uri imgUri = data.getData();
                try {
                    ADS_IMG = imgUri;
                    AdsImgBitMap = MediaStore.Images.Media.getBitmap(getContentResolver(), imgUri);
                    adsImg.setImageBitmap(AdsImgBitMap);
                    adsImg.setVisibility(View.VISIBLE);
                    image_picker.setImageResource(R.drawable.ic_close);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.post, menu);
        this.menu = menu;
        formHandler.setMenu(menu);
        return super.onCreateOptionsMenu(menu);
    }

    private boolean hasCategory() {
        for (int i = 0; i < adsPresenter.getCategories().size(); i++) {
            if (adsPresenter.getCategories().get(i).isChecked())
                return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_name) {
            if (String.valueOf(locationAttachment.getVisibility()).equals(String.valueOf(View.VISIBLE))) {
                addLocation = true;
            }
            if (hasCategory()) {
                if (ADS_IMG != null) {
                    adding_in_progress.setVisibility(View.VISIBLE);
                    firebaseStorageService.get_photo_local_uri(ADS_IMG, new ServerCallback() {
                        @Override
                        public void onSuccess(String result) {
                            adsPresenter.addAdvertisment(AdsForm.this, postTitle.getText().toString(), postBody.getText().toString().trim(), feelingDesc, feelingEmoji, FaceBook, attachedBusinessNumbersIDs, addLocation, result, shared_distance, days_of_ads, adsPresenter.getCategories());
                        }
                    });
                } else {
                    adding_in_progress.setVisibility(View.VISIBLE);
                    adsPresenter.addAdvertisment(AdsForm.this, postTitle.getText().toString(), postBody.getText().toString().trim(), feelingDesc, feelingEmoji, FaceBook, attachedBusinessNumbersIDs, addLocation, null, shared_distance, days_of_ads, adsPresenter.getCategories());
                }
            } else {
                Snackbar.make(userFeelingSection, "you must add category", Snackbar.LENGTH_LONG).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    //distance added mina_magdy 19-6-2018
    private void get_distance() {
        distance_wheel_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shared_distance = temp_distance;
                distanceDialog.dismiss();
            }
        });
    }

    // Days of ADS
    private void get_days_of_ads() {
        days_wheel_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                days_of_ads = temp_days;
                daysOfAdsDialog.dismiss();
            }
        });
    }

    //distance added mina_magdy 19-6-2018
    private void distance_listener() {
        distance_wheel.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int old_number, int new_number) {
                temp_distance = numberPicker.getDisplayedValues()[new_number];
            }
        });
    }

    // Days of ADS
    private void days_wheel_listener() {
        days_wheel.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int old_number, int new_number) {
                temp_days = numberPicker.getDisplayedValues()[new_number];
            }
        });
    }

    public void feelingListener() {
        happy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!feelingDesc.equals("Feeling Happy")) {
                    feelingEmoji = R.drawable.ic_emoji;
                    feelingDesc = "Feeling Happy";
                    formHandler.setBorder(happy, sad, intrested, shocked);
                } else {
                    feelingEmoji = 0;
                    feelingDesc = "";
                    formHandler.removeBorder(happy);
                }
            }
        });

        sad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!feelingDesc.equals("Feeling Sad")) {
                    feelingEmoji = R.drawable.ic_sad_emoji;
                    feelingDesc = "Feeling Sad";
                    formHandler.setBorder(sad, happy, intrested, shocked);
                } else {
                    feelingEmoji = 0;
                    feelingDesc = "";
                    formHandler.removeBorder(sad);
                }
            }
        });

        intrested.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!feelingDesc.equals("Feeling Intrested")) {
                    feelingEmoji = R.drawable.ic_intrested_emoji;
                    feelingDesc = "Feeling Intrested";
                    formHandler.setBorder(intrested, happy, sad, shocked);
                } else {
                    feelingEmoji = 0;
                    feelingDesc = "";
                    formHandler.removeBorder(intrested);
                }
            }
        });

        shocked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!feelingDesc.equals("Feeling Shocked")) {
                    feelingEmoji = R.drawable.ic_shocked_emoji;
                    feelingDesc = "Feeling Shocked";
                    formHandler.setBorder(shocked, happy, sad, intrested);
                } else {
                    feelingEmoji = 0;
                    feelingDesc = "";
                    formHandler.removeBorder(shocked);
                }
            }
        });
    }

}
