package com.example.lenovo.notarea.HttpRequests;

import android.os.AsyncTask;
import android.util.Log;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GET extends AsyncTask<String , Void  , String> {

    private final String TAG = "GET";

    @Override
    protected String doInBackground(String... strings) {

        String JSON = null;
        URL url = null;
        HttpURLConnection conn = null;
        InputStream inputStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader bufferedReader = null;

        try
        {
            url = new URL(strings[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            if(conn.getResponseCode() == 200 || conn.getResponseCode() == 201) {
                inputStream = conn.getInputStream();
                inputStreamReader = new InputStreamReader(inputStream);
                bufferedReader = new BufferedReader(inputStreamReader);
                String readLine = bufferedReader.readLine();
                while (readLine != null && !readLine.isEmpty()) {
                    JSON += readLine;
                    readLine = bufferedReader.readLine();
                }
                inputStreamReader.close();
                inputStream.close();

                return JSON;
            }else
            {
                return null;
            }



        }catch (Exception e)
        {
            Log.d(TAG, "-------------------------------------------------------");
            Log.d(TAG, "Get JSON ERROR: " + e);
            Log.d(TAG, "-------------------------------------------------------");
        }

        return JSON;
    }

    @Override
    protected void onPostExecute(String JSON) {
        super.onPostExecute(JSON);
    }

}
