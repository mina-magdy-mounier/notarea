package com.example.lenovo.notarea.View.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Constants;
import com.example.lenovo.notarea.FireBase.FirebaseStorageService;
import com.example.lenovo.notarea.Model.ExtraNumbers;
import com.example.lenovo.notarea.Presenter.SessionHandler;
import com.example.lenovo.notarea.Presenter.UserPreseneter;
import com.example.lenovo.notarea.Presenter.Validation.UserValidation;
import com.example.lenovo.notarea.Presenter.view_model_adapters.BusinessListAdapter;
import com.example.lenovo.notarea.Presenter.view_model_adapters.FamilyListAdapter;
import com.example.lenovo.notarea.R;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;

public class ProfileEditor extends AppCompatActivity {


    private Constants constants = new Constants();
    final int MAX_AGE = constants.getMAX_AGE(),
            MIN_AGE = constants.getMIN_AGE(),
            current_year = constants.getCurrent_year();

    public ProfileEditor() {
    }

    boolean edited_parts[] = new boolean[]{false, false, false, false, false, false};
    boolean hasImage = false;
    String AGE = null, GENDER = null, PHONE = null;
    Uri PROFILE_IMG_URI = null;
    Bitmap PROFILE = null;

    public Bitmap profile_image = null;
    ArrayList<String> deleted_numbers = new ArrayList<>();
    FirebaseStorageService firebaseStorageService = new FirebaseStorageService();

    Button gender, business_btn, family_btn, add_new_phone_btn_family, add_new_phone_btn_business;
    Button age, gender_ok, gender_cancel, age_ok, age_cancel;
    EditText phone, add_new_phone_input_family, add_new_phone_input_business;
    TextView age_calculated;
    ImageView profile_img;
    FloatingActionButton img_picker;
    SharedPreferences session;
    Dialog gender_dialog, age_dialog, business_dialog, family_dialog;
    public Dialog loading_dialog;
    RadioGroup user_gender_group;
    RadioButton gender_radio;
    NumberPicker numberPicker;
    ListView family_listView, business_listView;

    UserPreseneter userPreseneter;
    UserValidation userValidation;
    BusinessListAdapter businessListAdapter;
    FamilyListAdapter familyListAdapter;
    Menu menu;
    SessionHandler sessionHandler = new SessionHandler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        if (sessionHandler.get_theme(session) != R.style.AppTheme) {
            setTheme(sessionHandler.get_theme(session));
        }
        setContentView(R.layout.profile_editor);

        firebaseStorageService.setActivity(ProfileEditor.this);
//        session = getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        userPreseneter = new UserPreseneter(session);
        userValidation = new UserValidation();
        gender_dialog = new Dialog(this, R.style.CustomDialogTheme);
        age_dialog = new Dialog(this, R.style.CustomDialogTheme);
        family_dialog = new Dialog(this, R.style.CustomDialogTheme);
        business_dialog = new Dialog(this, R.style.CustomDialogTheme);

        gender_dialog.setContentView(R.layout.gender_picker);
        age_dialog.setContentView(R.layout.number_wheel);
        family_dialog.setContentView(R.layout.add_user_extra_phones_form);
        business_dialog.setContentView(R.layout.add_user_extra_phones_form);

        family_listView = family_dialog.findViewById(R.id.phoneList);
        business_listView = business_dialog.findViewById(R.id.phoneList);
        profile_img = findViewById(R.id.profile_img);
        img_picker = findViewById(R.id.floatingActionButton);
        phone = findViewById(R.id.phone);
        age = findViewById(R.id.age);
        gender = findViewById(R.id.gender);
        business_btn = findViewById(R.id.business_list);
        family_btn = findViewById(R.id.family_list);
        numberPicker = age_dialog.findViewById(R.id.number_picker);
        numberPicker.setMaxValue(MAX_AGE);
        numberPicker.setMinValue(MIN_AGE);
        numberPicker.setWrapSelectorWheel(false);
        age_calculated = age_dialog.findViewById(R.id.age_value);
        user_gender_group = gender_dialog.findViewById(R.id.gender_group);

        loading_dialog = new Dialog(this, R.style.CustomDialogTheme);
        loading_dialog.setContentView(R.layout.loading);
        loading_dialog.setCanceledOnTouchOutside(false);

        add_new_phone_input_business = business_dialog.findViewById(R.id.phoneNumber);
        add_new_phone_input_family = family_dialog.findViewById(R.id.phoneNumber);
        add_new_phone_btn_family = family_dialog.findViewById(R.id.addPhoneNumber);
        add_new_phone_btn_business = business_dialog.findViewById(R.id.addPhoneNumber);

        gender_ok = gender_dialog.findViewById(R.id.gok);
        gender_cancel = gender_dialog.findViewById(R.id.gcancel);

        age_ok = age_dialog.findViewById(R.id.ok);
        age_cancel = age_dialog.findViewById(R.id.cancel);

        profileEditorInit();
        imagePicker();
        update_phone_number();
        gender_picker();
        age_picker();
        family_picker();
        business_picker();
        addNewFamilyNumber();
        addNewBusinessNumber();
        delete_family_number();
        delete_business_number();
        calculate_age();
        get_age();
        get_gender();
        cancel(gender_dialog, gender_cancel);
        cancel(age_dialog, age_cancel);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.update, menu);
        this.menu = menu;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.update) {
            String validate_phone = userValidation.validateUserPhone(phone.getText().toString());
            if (validate_phone != null) {
                phone.setError(validate_phone);
            } else {
                final ArrayList<ExtraNumbers> FAMILY = userPreseneter.getUser().getFamilyNumbers();
                final ArrayList<ExtraNumbers> BUSINESS = userPreseneter.getUser().getBusinessNumbers();
                if (PROFILE_IMG_URI != null) {
                    loading_dialog.show();
                    firebaseStorageService.get_photo_local_uri(PROFILE_IMG_URI, new ServerCallback() {
                        @Override
                        public void onSuccess(String result) {
                            userPreseneter.editProfileInfo(ProfileEditor.this, result, PHONE, AGE, GENDER, FAMILY, BUSINESS, deleted_numbers, edited_parts);
                        }
                    });
                } else {
                    loading_dialog.show();
                    userPreseneter.editProfileInfo(ProfileEditor.this, null, PHONE, AGE, GENDER, FAMILY, BUSINESS, deleted_numbers, edited_parts);
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void can_update_checker() {
        MenuItem Item = menu.findItem(R.id.update);
        Item.setEnabled(false);
        for (int i = 0; i < edited_parts.length; i++) {
            if (edited_parts[i]) {
                Item.setEnabled(true);
                break;
            }
        }
    }

    private void update_phone_number() {
        phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                edited_parts[1] = false;
                can_update_checker();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!phone.getText().toString().equals("")) {
                    edited_parts[1] = true;
                    can_update_checker();
                    PHONE = phone.getText().toString();
                } else {
                    edited_parts[1] = false;
                    can_update_checker();
                    PHONE = null;
                }
            }
        });
    }

    private void profileEditorInit() {

        if (userPreseneter.getUser().getPhone() != null)
            phone.setText(userPreseneter.getUser().getPhone());
        if (userPreseneter.getUser().getProfile_Img() != null) {
            Picasso.get().load(userPreseneter.getUser().getProfile_Img()).into(profile_img);
            img_picker.setImageResource(R.drawable.ic_close_white_24dp);
            hasImage = true;
        }

        if (userPreseneter.getUser().getGender().equals("Male")) {
            user_gender_group.check(R.id.radio_male);
        } else {
            user_gender_group.check(R.id.radio_female);
        }

        age_calculated.setText(userPreseneter.getUser().getAge());
        numberPicker.setValue(current_year - Integer.valueOf(userPreseneter.getUser().getAge()));

        if (userPreseneter.getUser().getFamilyNumbers() != null) {
            familyListAdapter = new FamilyListAdapter(this, userPreseneter.getUser().getFamilyNumbers());
            family_listView.setAdapter(familyListAdapter);
            familyListAdapter.setWhereIam("close");
        }

        if (userPreseneter.getUser().getBusinessNumbers() != null) {
            businessListAdapter = new BusinessListAdapter(this, userPreseneter.getUser().getBusinessNumbers());
            business_listView.setAdapter(businessListAdapter);
            businessListAdapter.setWhereIam("close");
        }
    }

    private void gender_picker() {
        gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gender_dialog.show();
            }
        });
    }

    private void age_picker() {
        age.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                age_dialog.show();
            }
        });
    }

    private void get_age() {
        age_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AGE = age_calculated.getText().toString();
                edited_parts[2] = true;
                can_update_checker();
                age_dialog.dismiss();
            }
        });
    }

    private void get_gender() {
        gender_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selected_radio = user_gender_group.getCheckedRadioButtonId();
                gender_radio = gender_dialog.findViewById(selected_radio);
                GENDER = gender_radio.getText().toString();
                edited_parts[3] = true;
                can_update_checker();
                gender_dialog.dismiss();
            }
        });
    }

    private void family_picker() {
        family_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                family_dialog.show();
            }
        });
    }

    private void delete_family_number() {
        family_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ExtraNumbers extraNumbers = (ExtraNumbers) adapterView.getItemAtPosition(i);
                if (!extraNumbers.getId().equals("-1")) {
                    deleted_numbers.add(extraNumbers.getId());
                    edited_parts[5] = true;
                    can_update_checker();
                }
                userPreseneter.getUser().getFamilyNumbers().remove(extraNumbers);
                familyListAdapter.notifyDataSetChanged();
            }
        });
    }

    private void delete_business_number() {
        business_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ExtraNumbers extraNumbers = (ExtraNumbers) adapterView.getItemAtPosition(i);
                if (!extraNumbers.getId().equals("-1")) {
                    deleted_numbers.add(extraNumbers.getId());
                    edited_parts[4] = true;
                    can_update_checker();
                }
                userPreseneter.getUser().getBusinessNumbers().remove(extraNumbers);
                businessListAdapter.notifyDataSetChanged();
            }
        });
    }

    private void business_picker() {
        business_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                business_dialog.show();
            }
        });
    }

    private void calculate_age() {
        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int old_number, int new_number) {
                age_calculated.setText(String.valueOf(2018 - new_number));

            }
        });
    }

    private void cancel(final Dialog d, Button b) {
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.dismiss();
            }
        });
    }

    public void imagePicker() {
        img_picker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!hasImage) {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("image/jpeg");
                    hasImage = true;
                    intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                    startActivityForResult(intent, 101);
                    edited_parts[0] = true;
                    can_update_checker();
                } else {
                    profile_img.setImageResource(R.drawable.nobody);
                    img_picker.setImageResource(R.drawable.ic_camera_alt_black_24dp);
                    profile_image = null;
                    hasImage = false;
                    PROFILE = null;
                    edited_parts[0] = true;
                    PROFILE_IMG_URI = null;
                    can_update_checker();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == 101) {
                Uri imgUri = data.getData();
                try {
                    profile_image = MediaStore.Images.Media.getBitmap(getContentResolver(), imgUri);
                    profile_img.setImageBitmap(profile_image);
                    PROFILE_IMG_URI = imgUri;
                    img_picker.setImageResource(R.drawable.ic_close_white_24dp);
                    PROFILE = profile_image;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void addNewFamilyNumber() {
        add_new_phone_btn_family.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userValidation.validateExtraNumberInput(add_new_phone_input_family, getApplicationContext())) {
                    String name = add_new_phone_input_family.getText().toString().split("-")[0];
                    String number = add_new_phone_input_family.getText().toString().split("-")[1];

                    ExtraNumbers extraNumbers = new ExtraNumbers();
                    extraNumbers.setId("-1");
                    extraNumbers.setName(name);
                    extraNumbers.setNumber(number);
                    userPreseneter.getUser().getFamilyNumbers().add(extraNumbers);
                    familyListAdapter.notifyDataSetChanged();
                    add_new_phone_input_family.setText("");
                    edited_parts[5] = true;
                    can_update_checker();
                }
            }
        });
    }

    private void addNewBusinessNumber() {
        add_new_phone_btn_business.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userValidation.validateExtraNumberInput(add_new_phone_input_business, getApplicationContext())) {
                    String name = add_new_phone_input_business.getText().toString().split("-")[0];
                    String number = add_new_phone_input_business.getText().toString().split("-")[1];
                    ExtraNumbers extraNumbers = new ExtraNumbers();
                    extraNumbers.setId("-1");
                    extraNumbers.setName(name);
                    extraNumbers.setNumber(number);
                    userPreseneter.getUser().getBusinessNumbers().add(extraNumbers);
                    businessListAdapter.notifyDataSetChanged();
                    add_new_phone_input_business.setText("");
                    edited_parts[4] = true;
                    can_update_checker();
                }
            }
        });
    }
}



