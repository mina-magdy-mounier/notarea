package com.example.lenovo.notarea.View.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Shader;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.lenovo.notarea.Presenter.SessionHandler;
import com.example.lenovo.notarea.R;
import com.example.lenovo.notarea.View.adapters.SavedPostsAdpater;

public class SavedPosts extends AppCompatActivity {

    SessionHandler sessionHandler = new SessionHandler();
    SharedPreferences session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        if (sessionHandler.get_theme(session) != R.style.AppTheme) {
            setTheme(sessionHandler.get_theme(session));
        }
        setContentView(R.layout.activity_saved_posts);
        TabLayout savedPostsTabs = findViewById(R.id.savedPostsTabs);
        ViewPager savedPostsPages = findViewById(R.id.savedPostViewPager);
        SavedPostsAdpater savedPostsAdpater = new SavedPostsAdpater(getSupportFragmentManager());
        savedPostsPages.setAdapter(savedPostsAdpater);
        savedPostsPages.setOffscreenPageLimit(2);
        savedPostsTabs.setupWithViewPager(savedPostsPages);
    }
}
