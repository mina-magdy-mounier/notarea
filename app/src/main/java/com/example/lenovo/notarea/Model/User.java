package com.example.lenovo.notarea.Model;

import android.content.Context;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Constants;
import com.example.lenovo.notarea.Presenter.JsonParser;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class User {

    private final String TAG = "User";
    private Constants constants = new Constants();
    private String URL = constants.getURL();

    private String id;
    private String name;
    private String profile_Img;
    private String email;
    private String password;
    private String phone;
    private String gender;
    private String age;
    private int Premium;
    private String token;
    private Context context;
    private HashMap<String, String> familyList;
    private HashMap<String, String> businessList;

    private ArrayList<ExtraNumbers> familyNumbers;
    private ArrayList<ExtraNumbers> businessNumbers;

    private Location location = null;
    private ArrayList<Post> savedGPosts;
    private ArrayList<Advertisement> savedAds;
    private ArrayList<Post> myGPosts;
    private ArrayList<Advertisement> myAds;


    public User() {
    }

    public User(String profileImg, String name, String email, String password, String phone, String age, String gender, HashMap<String, String> familyList, HashMap<String, String> businessList, Location location, int premium) {
        this.name = name;
        this.profile_Img = profileImg;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.gender = gender;
        this.age = age;
        Premium = premium;
        this.familyList = familyList;
        this.businessList = businessList;
        this.location = location;
    }

    public User(String id, String profileImg, String phone, String age, String gender, ArrayList<ExtraNumbers> familyNumbers, ArrayList<ExtraNumbers> businessNumbers) {
        this.id = id;
        this.profile_Img = profileImg;
        this.phone = phone;
        this.age = age;
        this.gender = gender;
        this.familyNumbers = familyNumbers;
        this.businessNumbers = businessNumbers;
    }

    // ===========================  Setters & Getters ============================================//
    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_Img() {
        return profile_Img;
    }

    public void setProfile_Img(String profile_Img) {
        this.profile_Img = profile_Img;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public HashMap<String, String> getFamilyList() {
        return familyList;
    }

    public HashMap<String, String> getBusinessList() {
        return businessList;
    }

    public int getPremium() {
        return Premium;
    }

    public void setPremium(int premium) {
        Premium = premium;
    }

    public void setBusinessList(HashMap<String, String> businessList) {
        this.businessList = businessList;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public ArrayList<Post> getSavedGPosts() {
        return savedGPosts;
    }

    public void setSavedGPosts(ArrayList<Post> savedGPosts) {
        this.savedGPosts = savedGPosts;
    }

    public ArrayList<Post> getMyGPosts() {
        return myGPosts;
    }

    public void setMyGPosts(ArrayList<Post> myGPosts) {
        this.myGPosts = myGPosts;
    }

    public ArrayList<Advertisement> getSavedAds() {
        return savedAds;
    }

    public void setSavedAds(ArrayList<Advertisement> savedAds) {
        this.savedAds = savedAds;
    }

    public ArrayList<Advertisement> getMyAds() {
        return myAds;
    }

    public void setMyAds(ArrayList<Advertisement> myAds) {
        this.myAds = myAds;
    }

    public ArrayList<ExtraNumbers> getFamilyNumbers() {
        return familyNumbers;
    }

    public void setFamilyNumbers(ArrayList<ExtraNumbers> familyNumbers) {
        this.familyNumbers = familyNumbers;
    }

    public ArrayList<ExtraNumbers> getBusinessNumbers() {
        return businessNumbers;
    }

    public void setBusinessNumbers(ArrayList<ExtraNumbers> businessNumbers) {
        this.businessNumbers = businessNumbers;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    // ===========================  CRUD ============================================//

    public void addUser(JSONObject userJson, final ServerCallback callback) {
        try {
            String url = URL + "api/notarea/users";
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                    (Request.Method.POST, url, userJson, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject responsee) {
                            callback.onSuccess(responsee.toString());
                            Log.d(TAG, "onErrorResponse: " + responsee.toString());
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            NetworkResponse response = error.networkResponse;
                            if (response != null && response.data != null) {
                                switch (response.statusCode) {
                                    case 400:
                                        callback.onSuccess("Email is already taken");
                                        break;
                                    case 422:
                                        callback.onSuccess("failed validation Error");
                                        break;
                                }
                            }
                        }
                    });

            // Access the RequestQueue through your singleton class.
            Singleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
    }

    public void sign_out(JSONObject user_id, final ServerCallback callback) {
        try {
            String url = URL + "api/notarea/users/logout";
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                    (Request.Method.POST, url, user_id, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject responsee) {
                            callback.onSuccess(responsee.toString());
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            NetworkResponse response = error.networkResponse;
                            if (response != null && response.data != null) {
                                switch (response.statusCode) {
                                    case 404:
                                        callback.onSuccess("error");
                                        break;
                                    case 500:
                                        callback.onSuccess("php_error");
                                        break;
                                    case 422:
                                        callback.onSuccess("error");
                                        break;
                                }
                            }
                        }
                    });

            Singleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
    }

    public void getUser(String email, String password, String token, final ServerCallback callback) {
        try {
            JsonParser jsonParser = new JsonParser();
            JSONObject userJson = jsonParser.signIn(email, password, token);
            Log.d(TAG, "getUser: " + userJson);
            String url = URL + "api/notarea/users/login";
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                    (Request.Method.POST, url, userJson, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject responsee) {
                            callback.onSuccess(responsee.toString());
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            NetworkResponse response = error.networkResponse;
                            if (response != null && response.data != null) {
                                switch (response.statusCode) {
                                    case 404:
                                        callback.onSuccess("Not Found");
                                        break;
                                    case 400:
                                        callback.onSuccess("password is incorrect");
                                        break;
                                    case 422:
                                        callback.onSuccess("failed validation Error");
                                        break;
                                    case 320:
                                        callback.onSuccess("token_error");
                                        break;

                                }
                            }
                        }
                    });
            // Access the RequestQueue through your singleton class.
            Singleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
        } catch (Exception e) {
            Log.d(TAG, "===================getUser in User.java========================");
            Log.d(TAG, e.toString());
            Log.d(TAG, "===============================================================");
        }

    }

    public void updateUserProfile(JSONObject user_edited, final ServerCallback callback) {

        String url = URL + "api/notarea/users/changeinfo";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, user_edited,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String Response = response.optString("message");
                        Log.d(TAG, "onResponse: " + response.toString());
                        callback.onSuccess(Response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 404:
                            callback.onSuccess("0");
                            break;
                        case 400:
                            callback.onSuccess("0");
                            break;
                        case 422:
                            callback.onSuccess("0");
                            break;
                    }
                }
            }
        });
        Singleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public void changeUserName(JSONObject user_edited, final ServerCallback callback) {
        String url = URL + "api/notarea/users/changeusername";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, user_edited,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String Response = response.optString("message");
                        if (Response.equals("changed")) {
                            callback.onSuccess("user name changed succesfully");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    if (response.statusCode == 404) {
                        callback.onSuccess("username already taken");
                    }
                }
            }
        });
        Singleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public void changePassword(JSONObject user_edited, final ServerCallback callback) {
        String url = URL + "api/notarea/users/changepassword";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, user_edited,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String Response = response.optString("message");
                        Log.d(TAG, "onResponse: " + Response);
                        if (Response.equals("changed")) {
                            callback.onSuccess(Response);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    if (response.statusCode == 404) {
                        callback.onSuccess("Current Password is not correct");
                    }
                }
            }
        });
        Singleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public boolean checkUserName(String userName) {
        return false;
    }

    public boolean checkUserEmail(String userEmail) {
        return false;
    }
}
