package com.example.lenovo.notarea.View.adapters;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.Fragment;

import com.example.lenovo.notarea.View.activity.MainActivity;
import com.example.lenovo.notarea.View.fragments.AdsPageFragment;
import com.example.lenovo.notarea.View.fragments.EmergencyPageFragment;
import com.example.lenovo.notarea.View.fragments.GeneralPageFragemnt;

public class MainMenuAdapter extends FragmentPagerAdapter {

    MainActivity mainActivity;
    public MainMenuAdapter(FragmentManager fragmentManager, MainActivity mainActivity) {
        super(fragmentManager);
        this.mainActivity = mainActivity;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            GeneralPageFragemnt generalPageFragemnt = new GeneralPageFragemnt();
            generalPageFragemnt.setMainActivity(mainActivity);
            return generalPageFragemnt;
        } else if (position == 1) {
            AdsPageFragment adsPageFragment = new AdsPageFragment();
            adsPageFragment.setMainActivity(mainActivity);
            return adsPageFragment;
        } else {
            EmergencyPageFragment emergencyPageFragment = new EmergencyPageFragment();
            emergencyPageFragment.setMainActivity(mainActivity);
            return emergencyPageFragment;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0)
            return "Home";
        else if (position == 1)
            return "ADS";
        else
            return "Emergency";
    }
}
