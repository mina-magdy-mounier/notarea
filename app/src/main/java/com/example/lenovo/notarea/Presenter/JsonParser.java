package com.example.lenovo.notarea.Presenter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import com.example.lenovo.notarea.Model.Advertisement;
import com.example.lenovo.notarea.Model.Attachment;
import com.example.lenovo.notarea.Model.Category;
import com.example.lenovo.notarea.Model.Emergency;
import com.example.lenovo.notarea.Model.EmergencyNumber;
import com.example.lenovo.notarea.Model.ExtraNumbers;
import com.example.lenovo.notarea.Model.Location;
import com.example.lenovo.notarea.Model.Post;
import com.example.lenovo.notarea.Model.User;
import com.example.lenovo.notarea.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class JsonParser {

    final String TAG = "JsonParser";

    public JsonParser() {
    }

    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 10, byteArrayOutputStream);
        byte[] bytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    public Bitmap StringToBitMap(String img) {
        try {
            byte[] bytes = Base64.decode(img, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            return bitmap;
        } catch (Exception e) {
            return null;
        }
    }

    public JSONObject encodeUser(User user) {
        JSONObject root = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject location = new JSONObject();
        try {
            root.put("name", user.getName());
            root.put("email", user.getEmail());
            root.put("password", user.getPassword());
            root.put("phone", user.getPhone());
            root.put("age", user.getAge());
            root.put("gender", user.getGender());
            if (user.getToken() != null) {
                root.put("token", user.getToken());
            }
            root.put("isPremium", user.getPremium());
            if (user.getLocation() != null) {
                location.put("latitude", user.getLocation().getLatitude());
                location.put("longitude", user.getLocation().getLongitude());
                root.put("location", location);
            }
            if (user.getProfile_Img() != null) {

                root.put("photo", user.getProfile_Img());
            }

            if (user.getFamilyList() != null || user.getFamilyList().size() != 0) {
                Iterator iterator = user.getFamilyList().entrySet().iterator();
                while (iterator.hasNext()) {
                    JSONObject extraNumber = new JSONObject();
                    Map.Entry pair = (Map.Entry) iterator.next();
                    String name = pair.getValue().toString();
                    String number = pair.getKey().toString();
                    extraNumber.put("name", name);
                    extraNumber.put("number", number);
                    extraNumber.put("type", "f");
                    array.put(extraNumber);
                }
            }
            user.getBusinessList().put(user.getPhone(), user.getName());
            if (user.getBusinessList() != null || user.getBusinessList().size() != 0) {
                Iterator iterator = user.getBusinessList().entrySet().iterator();
                while (iterator.hasNext()) {
                    JSONObject extraNumber = new JSONObject();
                    Map.Entry pair = (Map.Entry) iterator.next();
                    String name = pair.getValue().toString();
                    String number = pair.getKey().toString();
                    extraNumber.put("name", name);
                    extraNumber.put("number", number);
                    extraNumber.put("type", "b");
                    array.put(extraNumber);
                }
            }
            if (user.getBusinessList().size() > 0 || user.getFamilyList().size() > 0) {
                root.put("extraNumbers", array);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        Log.d(TAG, "encodeUser: " + root.toString());
        return root;
    }

    public JSONObject encodeUserProfileEditor(User user, ArrayList<String> deleted_list, boolean edited_parts[]) {
        JSONObject root = new JSONObject();
        JSONArray family = new JSONArray();
        JSONArray business = new JSONArray();
        JSONArray deleted = new JSONArray();
        try {

            root.put("user_id", user.getId());
            if (edited_parts[0]) {
                if (user.getProfile_Img() != null) {
                    root.put("image", user.getProfile_Img());
                } else {
                    root.put("image", "");
                }
            }

            if (edited_parts[1]) {
                if (user.getPhone() != null) {
                    root.put("phonenumber", user.getPhone());
                }
            }

            if (edited_parts[2]) {
                if (user.getAge() != null) {
                    root.put("age", user.getAge());
                }
            }

            if (edited_parts[3]) {
                if (user.getGender() != null) {
                    root.put("gender", user.getGender());
                }
            }

            if (edited_parts[4]) {
                boolean isUpdated = false;
                if (user.getBusinessNumbers().size() != 0) {
                    for (int i = 0; i < user.getBusinessNumbers().size(); i++) {
                        if (user.getBusinessNumbers().get(i).getId().equals("-1")) {
                            isUpdated = true;
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("name", user.getBusinessNumbers().get(i).getName());
                            jsonObject.put("number", user.getBusinessNumbers().get(i).getNumber());
                            business.put(jsonObject);
                        }
                    }
                    if (isUpdated) {
                        root.put("business", business);
                    }
                }
            }

            if (edited_parts[5]) {
                boolean isUpdated = false;
                if (user.getFamilyNumbers().size() != 0) {
                    for (int i = 0; i < user.getFamilyNumbers().size(); i++) {
                        if (user.getFamilyNumbers().get(i).getId().equals("-1")) {
                            isUpdated = true;
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("name", user.getFamilyNumbers().get(i).getName());
                            jsonObject.put("number", user.getFamilyNumbers().get(i).getNumber());
                            family.put(jsonObject);
                        }
                    }
                    if (isUpdated) {
                        root.put("family", family);
                    }
                }
            }

            if (deleted_list.size() != 0) {
                for (int i = 0; i < deleted_list.size(); i++) {
                    deleted.put(deleted_list.get(i));
                }
                root.put("deleted", deleted);
            }

            return root;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public User decodeUser(String userJson) {
        User currentUser = new User();
        HashMap<String, String> businessList = new HashMap<>();
        try {
            JSONObject user, root;
            root = new JSONObject(userJson);
            user = root.optJSONObject("user");
            currentUser.setId(String.valueOf(user.optInt("id")));
            currentUser.setName(user.optString("name"));
            currentUser.setEmail(user.optString("email"));
            currentUser.setPassword(user.optString("password"));
            currentUser.setGender(user.optString("gender"));
            currentUser.setAge(user.optString("age"));
            currentUser.setPhone(user.optString("userphone"));
            currentUser.setPremium(user.optInt("premium"));
            if (!user.optString("photo").equals("null")) {
                currentUser.setProfile_Img(user.optString("photo"));

            } else {
                currentUser.setProfile_Img(null);
            }
            if (root.has("extranumbers")) {
                JSONArray extraNumbers = root.optJSONArray("extranumbers");
                if (extraNumbers.length() != 0) {
                    currentUser.setFamilyNumbers(new ArrayList<ExtraNumbers>());
                    currentUser.setBusinessNumbers(new ArrayList<ExtraNumbers>());
                    for (int i = 0; i < extraNumbers.length(); i++) {
                        JSONObject phoneDetails = extraNumbers.optJSONObject(i);
                        if (phoneDetails.optString("type").equals("b")) {
                            String id = String.valueOf(phoneDetails.optInt("id"));
                            String name = phoneDetails.optString("name");
                            String number = phoneDetails.optString("phone");
                            ExtraNumbers extraNumbers1 = new ExtraNumbers(id, name, number, "f");
                            currentUser.getBusinessNumbers().add(extraNumbers1);
                        } else {
                            String id = String.valueOf(phoneDetails.optInt("id"));
                            String name = phoneDetails.optString("name");
                            String number = phoneDetails.optString("phone");
                            ExtraNumbers extraNumbers1 = new ExtraNumbers(id, name, number, "f");
                            currentUser.getFamilyNumbers().add(extraNumbers1);
                        }
                    }
                }
                currentUser.setBusinessList(businessList);
            }
            if (root.has("location")) {
                JSONObject location = root.optJSONObject("location");
                currentUser.setLocation(new Location(String.valueOf(location.optInt("id")), location.optDouble("longitude"), location.optDouble("latitude")));
            } else {
                currentUser.setLocation(null);
            }
        } catch (
                JSONException e)

        {
            e.printStackTrace();
            return null;
        }

        return currentUser;
    }

    public void setProfileImage(User currentUser, String photo) {

        if (photo != null) {
            if (!photo.equals("null")) {
                currentUser.setProfile_Img(photo);

            }
        } else {
            currentUser.setProfile_Img(null);
        }
    }

    public void setUserLocation(User currentUser, String json) {
        JSONObject root = null;
        try {
            root = new JSONObject(json);
            if (root.has("location")) {
                JSONObject location = root.optJSONObject("location");
                currentUser.setLocation(new Location(String.valueOf(location.optInt("id")), location.optDouble("longitude"), location.optDouble("latitude")));
            } else {
                currentUser.setLocation(null);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setExtraNumbers(User currentUser, String json) {
        JSONArray extraNumbers = null;
        try {
            extraNumbers = new JSONArray(json);
            if (extraNumbers.length() != 0) {
                currentUser.setFamilyNumbers(new ArrayList<ExtraNumbers>());
                currentUser.setBusinessNumbers(new ArrayList<ExtraNumbers>());
                for (int i = 0; i < extraNumbers.length(); i++) {
                    JSONObject phoneDetails = extraNumbers.optJSONObject(i);
                    if (phoneDetails.optString("type").equals("b")) {
                        String id = String.valueOf(phoneDetails.optInt("id"));
                        String name = phoneDetails.optString("name");
                        String number = phoneDetails.optString("phone");
                        ExtraNumbers extraNumbers1 = new ExtraNumbers(id, name, number, "f");
                        currentUser.getBusinessNumbers().add(extraNumbers1);
                    } else {
                        String id = String.valueOf(phoneDetails.optInt("id"));
                        String name = phoneDetails.optString("name");
                        String number = phoneDetails.optString("phone");
                        ExtraNumbers extraNumbers1 = new ExtraNumbers(id, name, number, "f");
                        currentUser.getFamilyNumbers().add(extraNumbers1);
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public JSONObject signIn(String email, String password, String token) {
        JSONObject root = new JSONObject();
        try {
            root.put("email", email);
            root.put("password", password);
            if (token != null) {
                root.put("token", token);
            }
            return root;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject signOut(String user_id) {
        JSONObject root = new JSONObject();
        try {
            root.put("user_id", user_id);
            return root;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject encodeGPost(Post post) {
        JSONObject root = new JSONObject();
        JSONObject attachedlocation = new JSONObject();
        JSONObject location = new JSONObject();
        JSONObject attachment = new JSONObject();
        JSONArray extraNumbersIDs = new JSONArray();
        try {
            if (post.getTitle() != null) {
                root.put("title", post.getTitle());
            }
            if (post.getPostBody() != null) {
                root.put("content", post.getPostBody());
            }
            root.put("user_id", post.getUser().getId());
            if (post.getUserFeelingImg() > 0) {
                root.put("feelingnum", post.getUserFeelingImg());
            }
            if (post.getDistance() > 0) {
                root.put("distance", post.getDistance());
            }
            location.put("longitude", post.getLocation().getLongitude());
            location.put("latitude", post.getLocation().getLatitude());
            root.put("location", location);

            if (post.getAttachment() != null) {
                if (post.getAttachment().getLocation() != null) {
                    attachedlocation.put("longitude", post.getAttachment().getLocation().getLongitude());
                    attachedlocation.put("latitude", post.getAttachment().getLocation().getLatitude());
                    attachment.put("location", attachedlocation);
                }

                if (post.getAttachment().getFacebook_url() != null) {
                    attachment.put("facebook_url", post.getAttachment().getFacebook_url());
                }

                if (post.getAttachment().getPhoneNumbersIDs() != null) {
                    if (post.getAttachment().getPhoneNumbersIDs().size() > 0) {
                        for (int i = 0; i < post.getAttachment().getPhoneNumbersIDs().size(); i++) {
                            extraNumbersIDs.put(post.getAttachment().getPhoneNumbersIDs().get(i).toString());
                        }
                        attachment.put("extra_numbers", extraNumbersIDs);
                    }
                }

                root.put("attachment", attachment);
            }
            return root;
        } catch (JSONException e) {
            Log.d(TAG, "=====================encode post object============================");
            e.printStackTrace();
            Log.d(TAG, "=================================================");
            return null;
        }
    }

    public JSONObject encodeGEditor(Post post) {
        try {
            JSONObject parent = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            JSONObject root = new JSONObject();
            JSONObject postt = new JSONObject();
            JSONObject user = new JSONObject();
            JSONObject location = new JSONObject();
            JSONObject attachment = new JSONObject();

            postt.put("id", post.getPostID());
            if (post.getDistance() > 0) {
                postt.put("distance", post.getDistance());
            }
            if (post.getTitle() != null) postt.put("title", post.getTitle());
            postt.put("content", post.getPostBody());
            if (post.getUserFeelingImg() != 0) postt.put("feelingnum", post.getUserFeelingImg());
            user.put("id", post.getUser().getId());
            user.put("name", post.getUser().getName());
            if (post.getUser().getProfile_Img() != null)
                user.put("photo", post.getUser().getProfile_Img());
            location.put("id", post.getLocation().getId());
            location.put("longitude", post.getLocation().getLongitude());
            location.put("latitude", post.getLocation().getLatitude());
            if (post.getAttachment() != null) {
                attachment.put("id", post.getAttachment().getId());
                if (post.getAttachment().getFacebook_url() != null)
                    attachment.put("facebook_url", post.getAttachment().getFacebook_url());
                if (post.getAttachment().getPhoneNumber() != null) {
                    JSONArray extra_numbers = new JSONArray();
                    for (int i = 0; i < post.getAttachment().getPhoneNumber().size(); i++) {
                        JSONObject obj = new JSONObject();
                        obj.put("id", post.getAttachment().getPhoneNumber().get(i).getId());
                        obj.put("name", post.getAttachment().getPhoneNumber().get(i).getName());
                        obj.put("phone", post.getAttachment().getPhoneNumber().get(i).getNumber());
                        extra_numbers.put(obj);
                    }
                    attachment.put("extra_numbers", extra_numbers);
                }
                if (post.getAttachment().getLocation() != null) {
                    JSONObject loc = new JSONObject();
                    loc.put("id", post.getAttachment().getLocation().getId());
                    loc.put("longitude", post.getAttachment().getLocation().getLongitude());
                    loc.put("latitude", post.getAttachment().getLocation().getLatitude());
                    attachment.put("location", loc);
                }
                root.put("attachment", attachment);
            }
            root.put("post", postt);
            root.put("user", user);
            root.put("location", location);


            jsonArray.put(root);
            parent.put("posts", jsonArray);
            return parent;
        } catch (Exception e) {
            Log.d(TAG, "encodeGEditor: " + e.toString());
            return null;
        }
    }

    public JSONObject encodeEditedGPost(Post post, final Boolean editedParts[]) {
        JSONObject root = new JSONObject();
        JSONObject attachedlocation = new JSONObject();
        JSONObject location = new JSONObject();
        JSONObject attachment = new JSONObject();
        JSONArray extraNumbersIDs = new JSONArray();
        try {
            if (post.getTitle() != null) {
                root.put("title", post.getTitle());
            }
            if (post.getPostBody() != null) {
                root.put("content", post.getPostBody());
            }
            root.put("user_id", post.getUser().getId());
            if (editedParts[5]) {
                if (post.getUserFeelingImg() > 0) {
                    root.put("feelingnum", post.getUserFeelingImg());
                }
            }

            if (post.getDistance() > 0) {
                root.put("distance", post.getDistance());
            }

            if (post.getAttachment() != null) {
                if (editedParts[4]) {
                    if (post.getAttachment().getLocation() != null) {
                        attachedlocation.put("longitude", post.getAttachment().getLocation().getLongitude());
                        attachedlocation.put("latitude", post.getAttachment().getLocation().getLatitude());
                        attachment.put("location", attachedlocation);
                    } else {
                        attachment.put("location", "");
                    }
                }

                if (editedParts[3]) {
                    if (post.getAttachment().getPhoneNumbersIDs() != null) {
                        for (int i = 0; i < post.getAttachment().getPhoneNumbersIDs().size(); i++) {
                            extraNumbersIDs.put(post.getAttachment().getPhoneNumbersIDs().get(i).toString());
                        }
                        attachment.put("extra_numbers", extraNumbersIDs);
                    }
                }

                if (editedParts[2]) {
                    if (post.getAttachment().getFacebook_url() != null) {
                        attachment.put("facebook_url", post.getAttachment().getFacebook_url());
                    } else {
                        attachment.put("facebook_url", "");
                    }
                }

                root.put("attachment", attachment);
            }
            return root;
        } catch (JSONException e) {
            Log.d(TAG, "=====================encode post object============================");
            e.printStackTrace();
            Log.d(TAG, "=================================================");
            return null;
        }
    }

    public ArrayList<Post> decodeGlobalGPost(String postJson) {
        ArrayList<Post> posts = new ArrayList<>();
        JSONArray extraNumbers, rootArray;
        try {
//            JSONObject rooot = new JSONObject(postJson);
            rootArray = new JSONArray(postJson);
            for (int i = 0; i < rootArray.length(); i++) {
                JSONObject root = rootArray.optJSONObject(i);

                Post post = new Post();
                post.setUser(new User());
                post.setLocation(new Location());
                post.setAttachment(new Attachment());

                JSONObject currentPost = root.optJSONObject("post");
                post.setPostID(String.valueOf(currentPost.optInt("id")));
                if (currentPost.has("distance")) {
                    if (currentPost.getInt("distance") > 0) {
                        post.setDistance(currentPost.getInt("distance"));
                    }
                }

                if (currentPost.has("title")) {
                    if (currentPost.optString("title") != null) {
                        if (!currentPost.getString("title").equals("null")) {
                            post.setTitle(currentPost.optString("title"));
                        }
                    }
                }
                if (currentPost.optString("content") != null) {
                    if (!currentPost.getString("content").equals("null")) {
                        post.setPostBody(currentPost.optString("content"));
                    }
                }

                if (currentPost.has("feelingnum")) {
                    post.setUserFeelingImg(currentPost.optInt("feelingnum"));
                    if (post.getUserFeelingImg() == R.drawable.ic_emoji) {
                        post.setUserFeeling("Feeling Happy");
                    } else if (post.getUserFeelingImg() == R.drawable.ic_sad_emoji) {
                        post.setUserFeeling("Feeling Sad");
                    } else if (post.getUserFeelingImg() == R.drawable.ic_intrested_emoji) {
                        post.setUserFeeling("Feeling Intrested");
                    } else if (post.getUserFeelingImg() == R.drawable.ic_shocked_emoji) {
                        post.setUserFeeling("Feeling Shocked");
                    } else {
                        post.setUserFeelingImg(0);
                        post.setUserFeeling(null);
                    }
                }
                post.setNumberOfLikes(currentPost.optInt("number_of_likes"));
                post.setNumberOfDislikes(currentPost.optInt("number_of_dislikes"));
                post.setDate(currentPost.optString("created_at"));

                if (root.getJSONObject("user") != null) {
                    if (!root.getJSONObject("user").equals("null")) {
                        JSONObject user = root.optJSONObject("user");
                        post.setUserID(String.valueOf(user.optInt("id")));
                        post.getUser().setId(String.valueOf(user.optInt("id")));
                        post.getUser().setName(user.optString("name"));
                        if (user.has("photo")) {
                            if (!user.optString("photo").equals("null")) {
                                post.getUser().setProfile_Img(user.optString("photo"));
                            } else {
                                post.getUser().setProfile_Img(null);
                            }
                        } else {
                            post.getUser().setProfile_Img(null);
                        }
                    }
                }

                if (root.has("reaction")) {
                    JSONObject currentUserReaction = root.optJSONObject("reaction");
                    post.setIsLiked(currentUserReaction.optInt("liked"));
                    post.setIsDisliked(currentUserReaction.optInt("disliked"));
                    post.setIsSaved(currentUserReaction.optInt("saved"));
                }

                if (root.has("location")) {
                    JSONObject postLocation = root.optJSONObject("location");
                    post.getLocation().setId(String.valueOf(postLocation.optInt("id")));
                    post.getLocation().setLongitude(postLocation.optDouble("longitude"));
                    post.getLocation().setLatitude(postLocation.optDouble("latitude"));
                }

                if (root.has("attachment") && root.getJSONObject("attachment") != null) {
                    JSONObject attachment = root.optJSONObject("attachment");
                    if (attachment.has("id")) {
                        post.getAttachment().setId(String.valueOf(attachment.optInt("id")));
                    }
                    if (attachment.has("facebook_url") && !attachment.getString("facebook_url").equals("null") && !attachment.getString("facebook_url").equals("")) {
                        post.getAttachment().setFacebook_url(attachment.optString("facebook_url"));
                    } else {
                        post.getAttachment().setFacebook_url(null);
                    }

                    if (attachment.has("location") && !attachment.getString("location").equals("null")) {
                        post.getAttachment().setLocation(new Location());
                        JSONObject attachmentLocation = attachment.optJSONObject("location");
                        post.getAttachment().getLocation().setId(attachmentLocation.optString("id"));
                        post.getAttachment().getLocation().setLongitude(attachmentLocation.optDouble("longitude"));
                        post.getAttachment().getLocation().setLatitude(attachmentLocation.optDouble("latitude"));

                    } else {
                        post.getAttachment().setLocation(null);
                    }


                    if (attachment.has("extra_numbers") && !attachment.getString("extra_numbers").equals("[]")) {
                        extraNumbers = attachment.optJSONArray("extra_numbers");
                        post.getAttachment().setPhoneNumber(new ArrayList<ExtraNumbers>());
                        for (int ii = 0; ii < extraNumbers.length(); ii++) {
                            JSONObject business = extraNumbers.optJSONObject(ii);
                            String name = business.optString("name");
                            String number = business.optString("phone");
                            String id = String.valueOf(business.optInt("id"));
                            ExtraNumbers extraNumbers1 = new ExtraNumbers(id, name, number, "b");
                            post.getAttachment().getPhoneNumber().add(extraNumbers1);
                        }
                    } else {
                        post.getAttachment().setPhoneNumber(null);
                    }

                } else {
                    post.getAttachment().setLocation(null);
                    post.getAttachment().setPhoneNumber(null);
                    post.getAttachment().setFacebook_url(null);
                    post.setAttachment(null);
                }


                posts.add(post);

            }
            return posts;
        } catch (JSONException e) {
            Log.d(TAG, "=============decode G-Post Json in JsonParser.java====================");
            Log.d(TAG, e.toString());
            Log.d(TAG, "======================================================================");
            return null;
        }
    }

    public ArrayList<Post> decodeNearestGPost(String postJson) {
        Log.d(TAG, "decodeGPost: " + postJson);
        ArrayList<Post> posts = new ArrayList<>();
        JSONArray extraNumbers, rootArray;
        try {
            JSONObject rooot = new JSONObject(postJson);
            rootArray = rooot.optJSONArray("posts");
            for (int i = 0; i < rootArray.length(); i++) {
                JSONObject root = rootArray.optJSONObject(i);

                Post post = new Post();
                post.setUser(new User());
                post.setLocation(new Location());
                post.setAttachment(new Attachment());

                JSONObject currentPost = root.optJSONObject("post");
                post.setPostID(String.valueOf(currentPost.optInt("id")));
                if (currentPost.has("distance")) {
                    if (currentPost.getInt("distance") > 0) {
                        post.setDistance(currentPost.getInt("distance"));
                    }
                }

                if (currentPost.has("title")) {
                    if (currentPost.optString("title") != null) {
                        if (!currentPost.getString("title").equals("null")) {
                            post.setTitle(currentPost.optString("title"));
                        }
                    }
                }
                if (currentPost.optString("content") != null) {
                    if (!currentPost.getString("content").equals("null")) {
                        post.setPostBody(currentPost.optString("content"));
                    }
                }

                if (currentPost.has("feelingnum")) {
                    post.setUserFeelingImg(currentPost.optInt("feelingnum"));
                    if (post.getUserFeelingImg() == R.drawable.ic_emoji) {
                        post.setUserFeeling("Feeling Happy");
                    } else if (post.getUserFeelingImg() == R.drawable.ic_sad_emoji) {
                        post.setUserFeeling("Feeling Sad");
                    } else if (post.getUserFeelingImg() == R.drawable.ic_intrested_emoji) {
                        post.setUserFeeling("Feeling Intrested");
                    } else if (post.getUserFeelingImg() == R.drawable.ic_shocked_emoji) {
                        post.setUserFeeling("Feeling Shocked");
                    } else {
                        post.setUserFeelingImg(0);
                        post.setUserFeeling(null);
                    }
                }
                post.setNumberOfLikes(currentPost.optInt("number_of_likes"));
                post.setNumberOfDislikes(currentPost.optInt("number_of_dislikes"));
                post.setDate(currentPost.optString("created_at"));

                if (root.getJSONObject("user") != null) {
                    if (!root.getJSONObject("user").equals("null")) {
                        JSONObject user = root.optJSONObject("user");
                        post.setUserID(String.valueOf(user.optInt("id")));
                        post.getUser().setId(String.valueOf(user.optInt("id")));
                        post.getUser().setName(user.optString("name"));
                        if (user.has("photo")) {
                            if (!user.optString("photo").equals("null")) {
                                post.getUser().setProfile_Img(user.optString("photo"));
                            } else {
                                post.getUser().setProfile_Img(null);
                            }
                        } else {
                            post.getUser().setProfile_Img(null);
                        }
                    }
                }

                if (root.has("reaction")) {
                    JSONObject currentUserReaction = root.optJSONObject("reaction");
                    post.setIsLiked(currentUserReaction.optInt("liked"));
                    post.setIsDisliked(currentUserReaction.optInt("disliked"));
                    post.setIsSaved(currentUserReaction.optInt("saved"));
                }

                if (root.has("location")) {
                    JSONObject postLocation = root.optJSONObject("location");
                    post.getLocation().setId(String.valueOf(postLocation.optInt("id")));
                    post.getLocation().setLongitude(postLocation.optDouble("longitude"));
                    post.getLocation().setLatitude(postLocation.optDouble("latitude"));
                }

                if (root.has("attachment") && root.getJSONObject("attachment") != null) {
                    JSONObject attachment = root.optJSONObject("attachment");
                    if (attachment.has("id")) {
                        post.getAttachment().setId(String.valueOf(attachment.optInt("id")));
                    }
                    if (attachment.has("facebookurl")) {
                        if (!attachment.getString("facebookurl").equals("null")) {
                            post.getAttachment().setFacebook_url(attachment.getString("facebookurl"));
                        }
                    } else {
                        post.getAttachment().setFacebook_url(null);
                    }

                    if (attachment.has("location") && !attachment.getString("location").equals("null")) {
                        post.getAttachment().setLocation(new Location());
                        JSONObject attachmentLocation = attachment.optJSONObject("location");
                        post.getAttachment().getLocation().setId(attachmentLocation.optString("id"));
                        post.getAttachment().getLocation().setLongitude(attachmentLocation.optDouble("longitude"));
                        post.getAttachment().getLocation().setLatitude(attachmentLocation.optDouble("latitude"));

                    } else {
                        post.getAttachment().setLocation(null);
                    }


                    if (attachment.has("extra_numbers") && !attachment.getString("extra_numbers").equals("[]")) {
                        extraNumbers = attachment.optJSONArray("extra_numbers");
                        post.getAttachment().setPhoneNumber(new ArrayList<ExtraNumbers>());
                        for (int ii = 0; ii < extraNumbers.length(); ii++) {
                            JSONObject business = extraNumbers.optJSONObject(ii);
                            String name = business.optString("name");
                            String number = business.optString("phone");
                            String id = String.valueOf(business.optInt("id"));
                            ExtraNumbers extraNumbers1 = new ExtraNumbers(id, name, number, "b");
                            post.getAttachment().getPhoneNumber().add(extraNumbers1);
                        }
                    } else {
                        post.getAttachment().setPhoneNumber(null);
                    }

                } else {
                    post.getAttachment().setLocation(null);
                    post.getAttachment().setPhoneNumber(null);
                    post.getAttachment().setFacebook_url(null);
                    post.setAttachment(null);
                }


                posts.add(post);

            }
            return posts;
        } catch (JSONException e) {
            Log.d(TAG, "=============decode G-Post Json in JsonParser.java====================");
            Log.d(TAG, e.toString());
            Log.d(TAG, "======================================================================");
            return null;
        }
    }

    public JSONObject encodeAdsEditor(Advertisement post) {
        try {
            JSONObject parent = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            JSONObject root = new JSONObject();
            JSONObject postt = new JSONObject();
            JSONObject user = new JSONObject();
            JSONObject location = new JSONObject();
            JSONObject attachment = new JSONObject();

            postt.put("id", post.getAdsID());
            postt.put("post_id", post.getPostID());

            if (post.getDistance() > 0) {
                postt.put("distance", post.getDistance());
            }
            if (post.getAds_Img() != null) {
                if (!post.getAds_Img().equals("null")) {
                    postt.put("image", post.getAds_Img());
                }
            }
            if (post.getTitle() != null) postt.put("title", post.getTitle());
            postt.put("content", post.getPostBody());
            if (post.getAds_Img() != null) {
                if (!post.getAds_Img().equals("null")) {
                    postt.put("image", post.getAds_Img());
                }
            }
            if (post.getUserFeelingImg() != 0) postt.put("feelingnum", post.getUserFeelingImg());
            user.put("id", post.getUser().getId());
            user.put("name", post.getUser().getName());
            if (post.getUser().getProfile_Img() != null)
                user.put("photo", post.getUser().getProfile_Img());
            location.put("id", post.getLocation().getId());
            location.put("longitude", post.getLocation().getLongitude());
            location.put("latitude", post.getLocation().getLatitude());
            if (post.getAttachment() != null) {
                attachment.put("id", post.getAttachment().getId());
                if (post.getAttachment().getFacebook_url() != null)
                    attachment.put("facebook_url", post.getAttachment().getFacebook_url());
                if (post.getAttachment().getPhoneNumber() != null) {
                    JSONArray extra_numbers = new JSONArray();
                    for (int i = 0; i < post.getAttachment().getPhoneNumber().size(); i++) {
                        JSONObject obj = new JSONObject();
                        obj.put("id", post.getAttachment().getPhoneNumber().get(i).getId());
                        obj.put("name", post.getAttachment().getPhoneNumber().get(i).getName());
                        obj.put("phone", post.getAttachment().getPhoneNumber().get(i).getNumber());
                        extra_numbers.put(obj);
                    }
                    attachment.put("extra_numbers", extra_numbers);
                }
                if (post.getAttachment().getLocation() != null) {
                    JSONObject loc = new JSONObject();
                    loc.put("id", post.getAttachment().getLocation().getId());
                    loc.put("longitude", post.getAttachment().getLocation().getLongitude());
                    loc.put("latitude", post.getAttachment().getLocation().getLatitude());
                    attachment.put("location", loc);
                }
                root.put("attachment", attachment);
            }
            root.put("advertisment", postt);
            root.put("user", user);
            root.put("location", location);

            jsonArray.put(root);
            parent.put("advertisements", jsonArray);
            Log.d(TAG, "encodeAdsEditor: " + parent.toString());
            return parent;
        } catch (Exception e) {
            Log.d(TAG, "encodeGEditor: " + e.toString());
            return null;
        }
    }

    public JSONObject encodeEditedAdsPost(Advertisement post, final Boolean editedParts[]) {
        JSONObject root = new JSONObject();
        JSONObject attachedlocation = new JSONObject();
        JSONObject location = new JSONObject();
        JSONObject attachment = new JSONObject();
        JSONArray extraNumbersIDs = new JSONArray();
        try {
            if (post.getTitle() != null) {
                root.put("title", post.getTitle());
            }
            if (post.getPostBody() != null) {
                root.put("content", post.getPostBody());
            }
            if (post.getDistance() > 0) {
                root.put("distance", post.getDistance());
            }
            root.put("user_id", post.getUser().getId());
            if (editedParts[5]) {
                if (post.getUserFeelingImg() > 0) {
                    root.put("feelingnum", post.getUserFeelingImg());
                }
            }

            if (editedParts[6]) {
                if (post.getAds_Img() != null) {
                    if (!post.getAds_Img().equals("null")) {
                        root.put("image", post.getAds_Img());
                    }
                } else {
                    root.put("image", "");
                }
            }

            if (post.getAttachment() != null) {

                if (editedParts[2]) {
                    if (post.getAttachment().getFacebook_url() != null) {
                        attachment.put("facebook_url", post.getAttachment().getFacebook_url());
                    } else {
                        attachment.put("facebook_url", "");
                    }
                }

                if (editedParts[4]) {
                    if (post.getAttachment().getLocation() != null) {
                        attachedlocation.put("longitude", post.getAttachment().getLocation().getLongitude());
                        attachedlocation.put("latitude", post.getAttachment().getLocation().getLatitude());
                        attachment.put("location", attachedlocation);
                    } else {
                        attachment.put("location", "");
                    }
                }

                if (editedParts[3]) {
                    if (post.getAttachment().getPhoneNumbersIDs() != null) {
                        for (int i = 0; i < post.getAttachment().getPhoneNumbersIDs().size(); i++) {
                            extraNumbersIDs.put(post.getAttachment().getPhoneNumbersIDs().get(i).toString());
                        }
                        attachment.put("extra_numbers", extraNumbersIDs);
                    }
                }

                root.put("attachment", attachment);
            }
            return root;
        } catch (JSONException e) {
            Log.d(TAG, "================encode post object===============");
            e.printStackTrace();
            Log.d(TAG, "=================================================");
            return null;
        }
    }

    public JSONObject encodeAdsPost(Advertisement post) {
        JSONObject root = new JSONObject();
        JSONObject attachedlocation = new JSONObject();
        JSONObject location = new JSONObject();
        JSONObject attachment = new JSONObject();
        JSONArray extraNumbersIDs = new JSONArray();
        try {
            if (post.getTitle() != null) {
                root.put("title", post.getTitle());
            }
            if (post.getDistance() > 0) {
                root.put("distance", post.getDistance());
            }
            if (post.getDaysOfAD() > 0) {
                root.put("days", post.getDaysOfAD());
            }
            if (post.getCategories() != null) {
                if (post.getCategories().size() > 0) {
                    JSONArray categories = new JSONArray();
                    for (int i = 0; i < post.getCategories().size(); i++) {
                        if (post.getCategories().get(i).isChecked()) {
                            categories.put(post.getCategories().get(i).getId());
                        }
                        root.put("categories", categories);
                    }
                }
            }

            if (post.getPostBody() != null) {
                root.put("content", post.getPostBody());
            }
            root.put("user_id", post.getUser().getId());
            if (post.getUserFeelingImg() > 0) {
                root.put("feelingnum", post.getUserFeelingImg());
            }
            if (post.getAds_Img() != null) {
                root.put("image", post.getAds_Img());
            }
            location.put("longitude", post.getLocation().getLongitude());
            location.put("latitude", post.getLocation().getLatitude());
            root.put("location", location);

            if (post.getAttachment() != null) {
                if (post.getAttachment().getLocation() != null) {
                    attachedlocation.put("longitude", post.getAttachment().getLocation().getLongitude());
                    attachedlocation.put("latitude", post.getAttachment().getLocation().getLatitude());
                    attachment.put("location", attachedlocation);
                }

                if (post.getAttachment().getFacebook_url() != null) {
                    attachment.put("facebook_url", post.getAttachment().getFacebook_url());
                }

                if (post.getAttachment().getPhoneNumbersIDs() != null) {
                    if (post.getAttachment().getPhoneNumbersIDs().size() > 0) {
                        for (int i = 0; i < post.getAttachment().getPhoneNumbersIDs().size(); i++) {
                            extraNumbersIDs.put(post.getAttachment().getPhoneNumbersIDs().get(i).toString());
                        }
                        attachment.put("extra_numbers", extraNumbersIDs);
                    }
                }

                root.put("attachment", attachment);
            }
            return root;
        } catch (JSONException e) {
            Log.d(TAG, "=====================encode post object============================");
            e.printStackTrace();
            Log.d(TAG, "=================================================");
            return null;
        }
    }

    public ArrayList<Advertisement> decodeGlobaleAdsPost(String postJson) {
        ArrayList<Advertisement> posts = new ArrayList<>();
        JSONArray extraNumbers, rootArray;
        try {
            rootArray = new JSONArray(postJson);
            for (int i = 0; i < rootArray.length(); i++) {
                JSONObject root = rootArray.optJSONObject(i);

                Advertisement post = new Advertisement();
                post.setUser(new User());
                post.setLocation(new Location());
                post.setAttachment(new Attachment());

                JSONObject currentPost = root.optJSONObject("advertisment");
                post.setPostID(String.valueOf(currentPost.optInt("post_id")));
                post.setAdsID(String.valueOf(currentPost.optInt("id")));
                if (currentPost.has("distance")) {
                    post.setDistance(currentPost.getInt("distance") * 1000);
                }
                if (currentPost.has("image")) {
                    if (currentPost.optString("image") != null) {
                        if (!currentPost.optString("image").equals("null")) {
                            post.setAds_Img(currentPost.optString("image"));
                        }
                    } else {
                        post.setAds_Img(null);
                    }
                } else {
                    post.setAds_Img(null);
                }

                if (currentPost.has("title")) {
                    if (currentPost.optString("title") != null) {
                        if (!currentPost.getString("title").equals("null")) {
                            post.setTitle(currentPost.getString("title"));
                        }
                    }
                }

                if (currentPost.optString("content") != null) {
                    if (!currentPost.getString("content").equals("null")) {
                        post.setPostBody(currentPost.optString("content"));
                    }
                }

                if (currentPost.has("feelingnum")) {
                    post.setUserFeelingImg(currentPost.optInt("feelingnum"));
                    if (post.getUserFeelingImg() == R.drawable.ic_emoji) {
                        post.setUserFeeling("Feeling Happy");
                    } else if (post.getUserFeelingImg() == R.drawable.ic_sad_emoji) {
                        post.setUserFeeling("Feeling Sad");
                    } else if (post.getUserFeelingImg() == R.drawable.ic_intrested_emoji) {
                        post.setUserFeeling("Feeling Intrested");
                    } else if (post.getUserFeelingImg() == R.drawable.ic_shocked_emoji) {
                        post.setUserFeeling("Feeling Shocked");
                    } else {
                        post.setUserFeelingImg(0);
                        post.setUserFeeling(null);
                    }
                }
                post.setNumberOfLikes(currentPost.optInt("number_of_likes"));
                post.setNumberOfDislikes(currentPost.optInt("number_of_dislikes"));
                post.setDate(currentPost.optString("created_at"));

                if (root.getJSONObject("user") != null) {
                    if (!root.getJSONObject("user").equals("null")) {
                        JSONObject user = root.optJSONObject("user");
                        post.setUserID(String.valueOf(user.optInt("id")));
                        post.getUser().setId(String.valueOf(user.optInt("id")));
                        post.getUser().setName(user.optString("name"));
                        if (user.has("photo")) {
                            if (!user.optString("photo").equals("null")) {
                                post.getUser().setProfile_Img(user.optString("photo"));
                            } else {
                                post.getUser().setProfile_Img(null);
                            }
                        } else {
                            post.getUser().setProfile_Img(null);
                        }
                    }
                }

                if (root.has("reaction")) {
                    JSONObject currentUserReaction = root.optJSONObject("reaction");
                    post.setIsLiked(currentUserReaction.optInt("liked"));
                    post.setIsDisliked(currentUserReaction.optInt("disliked"));
                    post.setIsSaved(currentUserReaction.optInt("saved"));
                }

                if (root.has("location")) {
                    JSONObject postLocation = root.optJSONObject("location");
                    post.getLocation().setId(String.valueOf(postLocation.optInt("id")));
                    post.getLocation().setLongitude(postLocation.optDouble("longitude"));
                    post.getLocation().setLatitude(postLocation.optDouble("latitude"));
                }

                if (root.has("attachment") && root.getJSONObject("attachment") != null) {
                    JSONObject attachment = root.optJSONObject("attachment");
                    if (attachment.has("id")) {
                        post.getAttachment().setId(String.valueOf(attachment.optInt("id")));
                    }
                    if (attachment.has("facebook_url") && !attachment.getString("facebook_url").equals("null") && !attachment.getString("facebook_url").equals("")) {
                        post.getAttachment().setFacebook_url(attachment.optString("facebook_url"));
                    } else {
                        post.getAttachment().setFacebook_url(null);
                    }

                    if (attachment.has("location") && !attachment.getString("location").equals("null")) {
                        post.getAttachment().setLocation(new Location());
                        JSONObject attachmentLocation = attachment.optJSONObject("location");
                        post.getAttachment().getLocation().setId(attachmentLocation.optString("id"));
                        post.getAttachment().getLocation().setLongitude(attachmentLocation.optDouble("longitude"));
                        post.getAttachment().getLocation().setLatitude(attachmentLocation.optDouble("latitude"));

                    } else {
                        post.getAttachment().setLocation(null);
                    }


                    if (attachment.has("extra_numbers") && !attachment.getString("extra_numbers").equals("[]")) {
                        extraNumbers = attachment.optJSONArray("extra_numbers");
                        post.getAttachment().setPhoneNumber(new ArrayList<ExtraNumbers>());
                        for (int ii = 0; ii < extraNumbers.length(); ii++) {
                            JSONObject business = extraNumbers.optJSONObject(ii);
                            String name = business.optString("name");
                            String number = business.optString("phone");
                            String id = String.valueOf(business.optInt("id"));
                            ExtraNumbers extraNumbers1 = new ExtraNumbers(id, name, number, "b");
                            post.getAttachment().getPhoneNumber().add(extraNumbers1);
                        }
                    } else {
                        post.getAttachment().setPhoneNumber(null);
                    }

                } else {
                    post.getAttachment().setLocation(null);
                    post.getAttachment().setPhoneNumber(null);
                    post.getAttachment().setFacebook_url(null);
                    post.setAttachment(null);
                }


                posts.add(post);

            }
            return posts;
        } catch (JSONException e) {
            Log.d(TAG, e.toString());
            return null;
        }
    }

    public ArrayList<Advertisement> decodeNearestAdsPost(String postJson) {
        Log.d(TAG, "decodeNearestAdsPost: " + postJson);
        ArrayList<Advertisement> posts = new ArrayList<>();
        JSONArray extraNumbers, rootArray;
        try {
            JSONObject parent = new JSONObject(postJson);
            rootArray = parent.optJSONArray("advertisements");
            for (int i = 0; i < rootArray.length(); i++) {
                JSONObject root = rootArray.optJSONObject(i);

                Advertisement post = new Advertisement();
                post.setUser(new User());
                post.setLocation(new Location());
                post.setAttachment(new Attachment());

                JSONObject currentPost = root.optJSONObject("advertisment");
                post.setPostID(String.valueOf(currentPost.optInt("post_id")));
                post.setAdsID(String.valueOf(currentPost.optInt("id")));
                if (currentPost.has("distance")) {
                    double tmp = currentPost.getDouble("distance") * 1000;
                    post.setDistance(((int) tmp));
                }
                if (currentPost.has("image")) {
                    if (currentPost.optString("image") != null) {
                        if (!currentPost.optString("image").equals("null")) {
                            post.setAds_Img(currentPost.optString("image"));
                        }
                    } else {
                        post.setAds_Img(null);
                    }
                } else {
                    post.setAds_Img(null);
                }

                if (currentPost.has("title")) {
                    if (currentPost.optString("title") != null) {
                        if (!currentPost.getString("title").equals("null")) {
                            post.setTitle(currentPost.getString("title"));
                        }
                    }
                }

                if (currentPost.has("categories")) {
                    if (currentPost.optJSONArray("categories") != null) {
                        JSONArray categories = currentPost.getJSONArray("categories");
                        post.setCategories(new ArrayList<Category>());
                        for (int j = 0; j < categories.length(); j++) {
                            JSONObject category = categories.getJSONObject(j);
                            Category category1 = new Category();
                            category1.setChecked(false);
//                                category1.setId(String.valueOf(category.optInt("id")));
                            category1.setId("id");
                            category1.setName(category.optString("category"));
                            post.getCategories().add(category1);
                        }
                    }
                }

                if (currentPost.optString("content") != null) {
                    if (!currentPost.getString("content").equals("null")) {
                        post.setPostBody(currentPost.optString("content"));
                    }
                }

                if (currentPost.has("feelingnum")) {
                    post.setUserFeelingImg(currentPost.optInt("feelingnum"));
                    if (post.getUserFeelingImg() == R.drawable.ic_emoji) {
                        post.setUserFeeling("Feeling Happy");
                    } else if (post.getUserFeelingImg() == R.drawable.ic_sad_emoji) {
                        post.setUserFeeling("Feeling Sad");
                    } else if (post.getUserFeelingImg() == R.drawable.ic_intrested_emoji) {
                        post.setUserFeeling("Feeling Intrested");
                    } else if (post.getUserFeelingImg() == R.drawable.ic_shocked_emoji) {
                        post.setUserFeeling("Feeling Shocked");
                    } else {
                        post.setUserFeelingImg(0);
                        post.setUserFeeling(null);
                    }
                }
                post.setNumberOfLikes(currentPost.optInt("number_of_likes"));
                post.setNumberOfDislikes(currentPost.optInt("number_of_dislikes"));
                post.setDate(currentPost.optString("created_at"));

                if (root.getJSONObject("user") != null) {
                    if (!root.getJSONObject("user").equals("null")) {
                        JSONObject user = root.optJSONObject("user");
                        post.setUserID(String.valueOf(user.optInt("id")));
                        post.getUser().setId(String.valueOf(user.optInt("id")));
                        post.getUser().setName(user.optString("name"));
                        if (user.has("photo")) {
                            if (!user.optString("photo").equals("null")) {
                                post.getUser().setProfile_Img(user.optString("photo"));
                            } else {
                                post.getUser().setProfile_Img(null);
                            }
                        } else {
                            post.getUser().setProfile_Img(null);
                        }
                    }
                }

                if (root.has("reaction")) {
                    JSONObject currentUserReaction = root.optJSONObject("reaction");
                    post.setIsLiked(currentUserReaction.optInt("liked"));
                    post.setIsDisliked(currentUserReaction.optInt("disliked"));
                    post.setIsSaved(currentUserReaction.optInt("saved"));
                }

                if (root.has("location")) {
                    JSONObject postLocation = root.optJSONObject("location");
                    post.getLocation().setId(String.valueOf(postLocation.optInt("id")));
                    post.getLocation().setLongitude(postLocation.optDouble("longitude"));
                    post.getLocation().setLatitude(postLocation.optDouble("latitude"));
                }

                if (root.has("attachment") && root.getJSONObject("attachment") != null) {
                    JSONObject attachment = root.optJSONObject("attachment");
                    if (attachment.has("id")) {
                        post.getAttachment().setId(String.valueOf(attachment.optInt("id")));
                    }
                    if (attachment.has("facebookurl")) {
                        if (!attachment.getString("facebookurl").equals("null")) {
                            post.getAttachment().setFacebook_url(attachment.optString("facebookurl"));
                        }
                    } else {
                        post.getAttachment().setFacebook_url(null);
                    }

                    if (attachment.has("location") && !attachment.getString("location").equals("null")) {
                        post.getAttachment().setLocation(new Location());
                        JSONObject attachmentLocation = attachment.optJSONObject("location");
                        post.getAttachment().getLocation().setId(attachmentLocation.optString("id"));
                        post.getAttachment().getLocation().setLongitude(attachmentLocation.optDouble("longitude"));
                        post.getAttachment().getLocation().setLatitude(attachmentLocation.optDouble("latitude"));

                    } else {
                        post.getAttachment().setLocation(null);
                    }


                    if (attachment.has("extra_numbers") && !attachment.getString("extra_numbers").equals("[]")) {
                        extraNumbers = attachment.optJSONArray("extra_numbers");
                        post.getAttachment().setPhoneNumber(new ArrayList<ExtraNumbers>());
                        for (int ii = 0; ii < extraNumbers.length(); ii++) {
                            JSONObject business = extraNumbers.optJSONObject(ii);
                            String name = business.optString("name");
                            String number = business.optString("phone");
                            String id = String.valueOf(business.optInt("id"));
                            ExtraNumbers extraNumbers1 = new ExtraNumbers(id, name, number, "b");
                            post.getAttachment().getPhoneNumber().add(extraNumbers1);
                        }
                    } else {
                        post.getAttachment().setPhoneNumber(null);
                    }

                } else {
                    post.getAttachment().setLocation(null);
                    post.getAttachment().setPhoneNumber(null);
                    post.getAttachment().setFacebook_url(null);
                    post.setAttachment(null);
                }


                posts.add(post);

            }
            return posts;
        } catch (JSONException e) {
            Log.d(TAG, e.toString());
            return null;
        }
    }

    public String encodeUserEmail(String email) {
        JSONObject json = new JSONObject();
        try {
            json.put("email", email);
            return json.toString();
        } catch (JSONException e) {
            Log.d(TAG, "========Erroor in parsing User Email in JsonParsing.java=========");
            e.printStackTrace();
            Log.d(TAG, "==================================================================");
            return null;
        }

    }

    public String encodeUserName(String name) {
        JSONObject json = new JSONObject();
        try {
            json.put("name", name);
            return json.toString();
        } catch (JSONException e) {
            Log.d(TAG, "========Erroor in parsing User Name in JsonParsing.java=========");
            e.printStackTrace();
            Log.d(TAG, "==================================================================");
            return null;
        }

    }

    public JSONObject encodePasswordEditor(String old, String New, String user_id) {
        JSONObject root = new JSONObject();
        try {
            root.put("oldpassword", old);
            root.put("newpassword", New);
            root.put("user_id", user_id);
            return root;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject encodeUserNameEditor(String user_id, String user_name) {
        JSONObject root = new JSONObject();
        try {
            root.put("user_id", user_id);
            root.put("name", user_name);
            return root;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject encodeEmergency(Emergency emergency) {
        JSONObject root = new JSONObject();
        JSONObject location = new JSONObject();
        try {
            if (emergency.getTitle() != null) {
                root.put("title", emergency.getTitle());
            }
            if (emergency.getPostBody() != null) {
                root.put("content", emergency.getPostBody());
            }
            root.put("country_id", emergency.getCountryId());
            location.put("longitude", emergency.getLocation().getLongitude());
            location.put("latitude", emergency.getLocation().getLatitude());
            root.put("location", location);
            Log.d(TAG, "encodeEmergency: " + root.toString());
            return root;
        } catch (JSONException e) {
            Log.d(TAG, "=====================encode post object============================");
            e.printStackTrace();
            Log.d(TAG, "=================================================");
            return null;
        }
    }

    public ArrayList<Emergency> decodeEmergency(String emergecyJson) {
        ArrayList<Emergency> emergencies = new ArrayList<>();
        JSONObject rootObject;
        try {
            rootObject = new JSONObject(emergecyJson);
            JSONArray emergenciesArray = rootObject.optJSONArray("emergencies");
            for (int i = 0; i < emergenciesArray.length(); i++) {
                JSONObject root = emergenciesArray.optJSONObject(i);
                Emergency emergency = new Emergency();
                emergency.setUser(new User());
                emergency.setLocation(new Location());
                JSONObject currentEmergency = root.optJSONObject("emergency");
                emergency.setId(String.valueOf(currentEmergency.optInt("id")));
                emergency.setCountryId(String.valueOf(currentEmergency.optInt("country_id")));
                if (String.valueOf(currentEmergency.optInt("active")).equals("1") || String.valueOf(currentEmergency.optInt("active")) != null || !String.valueOf(currentEmergency.optInt("active")).isEmpty())
                    emergency.setActive(true);
                else emergency.setActive(false);
                emergency.setRate(currentEmergency.optDouble("rating"));
                emergency.setDate(currentEmergency.optString("created_at"));
                if (root.has("emergency_numbers")) {
                    JSONArray Numbers = root.optJSONArray("emergency_numbers");
                    if (Numbers.length() != 0) {
                        emergency.setEmeNumbers(new ArrayList<EmergencyNumber>());
                        for (int y = 0; y < Numbers.length(); y++) {
                            JSONObject phoneDetails = Numbers.optJSONObject(y);
                            String emergency_type = phoneDetails.optString("emergency_type");
                            String emergency_number = phoneDetails.optString("emergency_number");
                            EmergencyNumber emergencyNumber = new EmergencyNumber(emergency_type, emergency_number);
                            emergency.getEmeNumbers().add(emergencyNumber);
                        }
                    }
                }

                if (root.has("post")) {
                    JSONObject post = root.optJSONObject("post");
                    emergency.setTitle(post.optString("title"));
                    emergency.setPostBody(post.optString("content"));
                }

                if (root.optJSONObject("user") != null) {
                    JSONObject user = root.optJSONObject("user");
                    emergency.setUserID(String.valueOf(user.optInt("id")));
                    emergency.getUser().setId(String.valueOf(user.optInt("id")));
                    emergency.getUser().setName(user.optString("name"));
                    if (!user.optString("photo").equals("null")) {
                        emergency.getUser().setProfile_Img(user.optString("photo"));
                    } else {
                        emergency.getUser().setProfile_Img(null);
                    }
                }
                if (root.has("location")) {
                    JSONObject postLocation = root.optJSONObject("location");
                    emergency.getLocation().setId(String.valueOf(postLocation.optInt("id")));
                    emergency.getLocation().setLongitude(postLocation.optDouble("longitude"));
                    emergency.getLocation().setLatitude(postLocation.optDouble("latitude"));
                } else {
                    emergency.setUser(null);
                }

                emergencies.add(emergency);

            }
            return emergencies;
        } catch (JSONException e) {
            Log.d(TAG, e.toString());
            return null;
        }
    }

    public ArrayList<Category> encodeCategory(String Categories) {

        ArrayList<Category> categories = new ArrayList<>();
        try {
            JSONArray root = new JSONArray(Categories);
            for (int i = 0; i < root.length(); i++) {
                JSONObject category = root.optJSONObject(i);
                String id = String.valueOf(category.optInt("id"));
                String name = category.optString("category");
                categories.add(new Category(id, name));
            }
            return categories;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public JSONObject encodeLocation(Location location, String countryCode) {
        JSONObject root = new JSONObject();
        JSONObject loc = new JSONObject();
        try {
            loc.put("longitude", location.getLongitude());
            loc.put("latitude", location.getLatitude());
            root.put("location", loc);
            root.put("country_id", countryCode);
            return root;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Emergency decodeEmergencyNotification(String emergecyJson) {
        Emergency emergency = new Emergency();
        JSONObject rootObject;
        try {
            emergency.setUser(new User());
            emergency.setLocation(new Location());
            rootObject = new JSONObject(emergecyJson);
            JSONObject currentEmergency = rootObject.optJSONObject("emergency");
            emergency.setId(String.valueOf(currentEmergency.optInt("id")));
            emergency.setTitle(currentEmergency.optString("title"));
            emergency.setPostBody(currentEmergency.optString("content"));
            // emergency.setCountryId(String.valueOf(currentEmergency.optInt("country_id")));
            if (String.valueOf(currentEmergency.optInt("active")).equals("1") || String.valueOf(currentEmergency.optInt("active")) != null || !String.valueOf(currentEmergency.optInt("active")).isEmpty())
                emergency.setActive(true);
            else emergency.setActive(false);
            emergency.setRate(currentEmergency.optDouble("rating"));
            JSONObject CreatedAt = currentEmergency.optJSONObject("created_at");
            emergency.setDate(CreatedAt.optString("date"));
            if (currentEmergency.has("emergency_numbers")) {
                JSONArray Numbers = currentEmergency.optJSONArray("emergency_numbers");
                if (Numbers.length() != 0) {
                    emergency.setEmeNumbers(new ArrayList<EmergencyNumber>());
                    for (int y = 0; y < Numbers.length(); y++) {
                        JSONObject phoneDetails = Numbers.optJSONObject(y);
                        String emergency_type = phoneDetails.optString("emergency_type");
                        String emergency_number = phoneDetails.optString("emergency_number");
                        EmergencyNumber emergencyNumber = new EmergencyNumber(emergency_type, emergency_number);
                        emergency.getEmeNumbers().add(emergencyNumber);
                    }
                }
            }

            if (rootObject.optJSONObject("user") != null) {
                JSONObject user = rootObject.optJSONObject("user");
                emergency.setUserID(String.valueOf(user.optInt("id")));
                emergency.getUser().setId(String.valueOf(user.optInt("id")));
                emergency.getUser().setName(user.optString("name"));
                if (user.has("photo")) {
                    if (!user.optString("photo").equals("null")) {
                        emergency.getUser().setProfile_Img(user.optString("photo"));
                    } else {
                        emergency.getUser().setProfile_Img(null);
                    }
                } else {
                    emergency.getUser().setProfile_Img(null);
                }
            }

            if (rootObject.has("location")) {
                JSONObject postLocation = rootObject.optJSONObject("location");
                emergency.getLocation().setId(String.valueOf(postLocation.optInt("id")));
                emergency.getLocation().setLongitude(postLocation.optDouble("longitude"));
                emergency.getLocation().setLatitude(postLocation.optDouble("latitude"));
            } else {
                emergency.setUser(null);
            }

            return emergency;
        } catch (JSONException e) {
            Log.d(TAG, e.toString());
            return null;
        }
    }

    public Post decodeGPostNotification(String json) {
        Post post = new Post();
        try {
            JSONObject root = new JSONObject(json);
            JSONObject post1 = root.optJSONObject("post");
            post.setDate(post1.optString("created_at"));
            post.setUserID(post1.optString("user_id"));
            post.setPostID(String.valueOf(post1.optInt("id")));
            if (post1.has("title")) {
                if (!post1.optString("title").equals("null")) {
                    post.setTitle(post1.optString("title"));
                }
            }

            if (post1.has("content")) {
                if (!post1.optString("content").equals("null")) {
                    post.setPostBody(post1.optString("content"));
                }
            }

            if (post1.has("feelingnum")) {
                post.setUserFeelingImg(post1.optInt("feelingnum"));
                if (post.getUserFeelingImg() == R.drawable.ic_emoji) {
                    post.setUserFeeling("Feeling Happy");
                } else if (post.getUserFeelingImg() == R.drawable.ic_sad_emoji) {
                    post.setUserFeeling("Feeling Sad");
                } else if (post.getUserFeelingImg() == R.drawable.ic_intrested_emoji) {
                    post.setUserFeeling("Feeling Intrested");
                } else if (post.getUserFeelingImg() == R.drawable.ic_shocked_emoji) {
                    post.setUserFeeling("Feeling Shocked");
                } else {
                    post.setUserFeelingImg(0);
                    post.setUserFeeling(null);
                }
            }

            if (root.has("user")) {
                JSONObject user = root.optJSONObject("user");
                post.setUser(new User());
                post.getUser().setId(String.valueOf(user.optInt("id")));
                if (user.has("photo")) {
                    if (!user.optString("photo").equals("null")) {
                        post.getUser().setProfile_Img(user.optString("photo"));
                    }
                }
                post.getUser().setName(user.optString("name"));
            }

            if (root.has("location")) {
                JSONObject location = root.optJSONObject("location");
                post.setLocation(new Location());
                post.getLocation().setId(String.valueOf(location.optInt("id")));
                post.getLocation().setLatitude(location.optInt("latitude"));
                post.getLocation().setLongitude(location.optInt("longitude"));
            }

            if (root.has("attachment") || root.has("locationAttached") || root.has("numbersAttached")) {
                post.setAttachment(new Attachment());
                if (root.has("attachment")) {
                    JSONObject attachment = root.optJSONObject("attachment");
                    post.getAttachment().setId(String.valueOf(attachment.optInt("id")));
                    if (attachment.has("facebookurl")) {
                        if (!attachment.optString("facebookurl").equals("null")) {
                            post.getAttachment().setFacebook_url(attachment.optString("facebookurl"));
                        }
                    }
                } else {
                    post.getAttachment().setFacebook_url(null);
                }


                if (root.has("locationAttached")) {
                    post.getAttachment().setLocation(new Location());
                    JSONObject locationAttached = root.optJSONObject("locationAttached");
                    post.getAttachment().getLocation().setId(String.valueOf(locationAttached.optInt("id")));
                    post.getAttachment().getLocation().setLongitude(locationAttached.optDouble("longitude"));
                    post.getAttachment().getLocation().setLatitude(locationAttached.optDouble("latitude"));
                } else {
                    post.getAttachment().setLocation(null);
                }

                if (root.has("numbersAttached")) {
                    post.getAttachment().setPhoneNumber(new ArrayList<ExtraNumbers>());
                    JSONArray attachment = root.optJSONArray("numbersAttached");
                    if (!attachment.equals("[]")) {
                        post.getAttachment().setPhoneNumber(new ArrayList<ExtraNumbers>());
                        for (int ii = 0; ii < attachment.length(); ii++) {
                            JSONObject business = attachment.optJSONObject(ii);
                            String name = business.optString("name");
                            String number = business.optString("phone");
                            String id = String.valueOf(business.optInt("id"));
                            ExtraNumbers extraNumbers1 = new ExtraNumbers(id, name, number, "b");
                            post.getAttachment().getPhoneNumber().add(extraNumbers1);
                        }
                    } else {
                        post.getAttachment().setPhoneNumber(null);
                    }
                }
            } else {
                post.setAttachment(null);
            }

            return post;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

    public Advertisement decodeAdsNotification(String json) {
        Advertisement post = new Advertisement();
        try {
            JSONObject root = new JSONObject(json);
            JSONObject post1 = root.optJSONObject("post");
            JSONObject ads = root.optJSONObject("advertisment");
            post.setDate(post1.optString("created_at"));
            post.setUserID(post1.optString("user_id"));
            post.setPostID(String.valueOf(post1.optInt("id")));
            if (post1.has("title")) {
                if (!post1.optString("title").equals("null")) {
                    post.setTitle(post1.optString("title"));
                }
            }

            if (post1.has("content")) {
                if (!post1.optString("content").equals("null")) {
                    post.setPostBody(post1.optString("content"));
                }
            }

            if (ads.has("image")) {
                if (ads.optString("image") != null) {
                    if (!ads.optString("image").equals("null")) {
                        post.setAds_Img(ads.optString("image"));
                    }
                }
            }

            post.setAdsID(ads.optString("id"));

            if (ads.has("distance")) {
                if (ads.optInt("distance") > 0) {
                    post.setDistance(ads.optInt("distance") * 1000);
                }
            }

            if (ads.has("days")) {
                if (ads.optInt("days") > 0) {
                    post.setDaysOfAD(ads.optInt("days"));
                }
            }

            if (post1.has("feelingnum")) {
                post.setUserFeelingImg(post1.optInt("feelingnum"));
                if (post.getUserFeelingImg() == R.drawable.ic_emoji) {
                    post.setUserFeeling("Feeling Happy");
                } else if (post.getUserFeelingImg() == R.drawable.ic_sad_emoji) {
                    post.setUserFeeling("Feeling Sad");
                } else if (post.getUserFeelingImg() == R.drawable.ic_intrested_emoji) {
                    post.setUserFeeling("Feeling Intrested");
                } else if (post.getUserFeelingImg() == R.drawable.ic_shocked_emoji) {
                    post.setUserFeeling("Feeling Shocked");
                } else {
                    post.setUserFeelingImg(0);
                    post.setUserFeeling(null);
                }
            }

            if (root.has("user")) {
                JSONObject user = root.optJSONObject("user");
                post.setUser(new User());
                post.getUser().setId(String.valueOf(user.optInt("id")));
                if (user.has("photo")) {
                    if (!user.optString("photo").equals("null")) {
                        post.getUser().setProfile_Img(user.optString("photo"));
                    }
                }
                post.getUser().setName(user.optString("name"));
            }

            if (root.has("location")) {
                JSONObject location = root.optJSONObject("location");
                post.setLocation(new Location());
                post.getLocation().setId(String.valueOf(location.optInt("id")));
                post.getLocation().setLatitude(location.optInt("latitude"));
                post.getLocation().setLongitude(location.optInt("longitude"));
            }

            if (root.has("attachment") || root.has("locationAttached") || root.has("numbersAttached")) {
                post.setAttachment(new Attachment());
                if (root.has("attachment")) {
                    JSONObject attachment = root.optJSONObject("attachment");
                    post.getAttachment().setId(String.valueOf(attachment.optInt("id")));
                    if (attachment.has("facebookurl")) {
                        if (!attachment.optString("facebookurl").equals("null")) {
                            post.getAttachment().setFacebook_url(attachment.optString("facebookurl"));
                        }
                    }
                } else {
                    post.getAttachment().setFacebook_url(null);
                }


                if (root.has("locationAttached")) {
                    post.getAttachment().setLocation(new Location());
                    JSONObject locationAttached = root.optJSONObject("locationAttached");
                    post.getAttachment().getLocation().setId(String.valueOf(locationAttached.optInt("id")));
                    post.getAttachment().getLocation().setLongitude(locationAttached.optDouble("longitude"));
                    post.getAttachment().getLocation().setLatitude(locationAttached.optDouble("latitude"));
                } else {
                    post.getAttachment().setLocation(null);
                }

                if (root.has("numbersAttached")) {
                    post.getAttachment().setPhoneNumber(new ArrayList<ExtraNumbers>());
                    JSONArray attachment = root.optJSONArray("numbersAttached");
                    if (!attachment.equals("[]")) {
                        post.getAttachment().setPhoneNumber(new ArrayList<ExtraNumbers>());
                        for (int ii = 0; ii < attachment.length(); ii++) {
                            JSONObject business = attachment.optJSONObject(ii);
                            String name = business.optString("name");
                            String number = business.optString("phone");
                            String id = String.valueOf(business.optInt("id"));
                            ExtraNumbers extraNumbers1 = new ExtraNumbers(id, name, number, "b");
                            post.getAttachment().getPhoneNumber().add(extraNumbers1);
                        }
                    } else {
                        post.getAttachment().setPhoneNumber(null);
                    }
                }
            } else {
                post.setAttachment(null);
            }

            if (root.has("categories")) {
                if (root.optJSONArray("categories") != null) {
                    JSONArray categories = root.getJSONArray("categories");
                    post.setCategories(new ArrayList<Category>());
                    for (int j = 0; j < categories.length(); j++) {
                        JSONObject category = categories.getJSONObject(j);
                        Category category1 = new Category();
                        category1.setChecked(false);
//                                category1.setId(String.valueOf(category.optInt("id")));
                        category1.setId("id");
                        category1.setName(category.optString("category"));
                        post.getCategories().add(category1);
                    }
                }
            } else {
                post.setCategories(new ArrayList<Category>());
            }

            return post;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

    public JSONObject encodeRateEmergency(Emergency emergency) {
        JSONObject root = new JSONObject();
        try {
            root.put("user_id", Integer.parseInt(emergency.getUser().getId()));
            root.put("emergency_id", Integer.parseInt(emergency.getId()));
            root.put("rating", (int) emergency.getRate());
            Log.d(TAG, "encodeRateEmergency: " + root.toString());
            return root;
        } catch (JSONException e) {
            Log.d(TAG, "=====================encode post object============================");
            e.printStackTrace();
            Log.d(TAG, "=================================================");
            return null;
        }
    }

    public ArrayList<EmergencyNumber> decodeEmergencyNumbers(String emergencyNumbersJson) {
        JSONObject root;
        ArrayList<EmergencyNumber> emergencyNumbers;
        try {
            root = new JSONObject(emergencyNumbersJson);
            emergencyNumbers = new ArrayList<EmergencyNumber>();
            if (root.has("numbers")) {
                JSONArray Numbers = root.optJSONArray("numbers");
                if (Numbers.length() != 0) {
                    //emergency.setEmeNumbers(new ArrayList<EmergencyNumber>());
                    for (int y = 0; y < Numbers.length(); y++) {
                        JSONObject phoneDetails = Numbers.optJSONObject(y);
                        String emergency_type = phoneDetails.optString("emergency_type");
                        String emergency_number = phoneDetails.optString("emergency_number");
                        EmergencyNumber emergencyNumber = new EmergencyNumber(emergency_type, emergency_number);
                        emergencyNumbers.add(emergencyNumber);
                    }
                }
                return emergencyNumbers;
            }
        } catch (JSONException e) {
            Log.d(TAG, e.toString());
            return null;
        }
        return null;
    }

    public ArrayList<ExtraNumbers> decodeEmergenciesNumbers(String emergencyNumbersJson) {
        JSONObject root;
        ArrayList<ExtraNumbers> extraNumbers = null;
        try {
            root = new JSONObject(emergencyNumbersJson);
            if (root.has("numbers")) {
                JSONArray Numbers = root.optJSONArray("numbers");
                extraNumbers = new ArrayList<>();
                if (Numbers.length() > 0) {
                    for (int y = 0; y < Numbers.length(); y++) {
                        JSONObject phoneDetails = Numbers.optJSONObject(y);
                        String emergency_type = phoneDetails.optString("emergency_type");
                        String emergency_number = phoneDetails.optString("emergency_number");
                        ExtraNumbers extraNumber = new ExtraNumbers(emergency_type, emergency_number, "e");
                        extraNumbers.add(extraNumber);
                    }
                }
                return extraNumbers;
            }
        } catch (JSONException e) {
            Log.d(TAG, e.toString());
            return null;
        }
        return null;
    }

    public JSONObject encodePostsNearest(Location location, String user_id) {
        JSONObject root = new JSONObject();
        JSONObject loc = new JSONObject();
        try {
            loc.put("latitude", location.getLatitude());
            loc.put("longitude", location.getLongitude());
            root.put("location", loc);
            root.put("user_id", Integer.parseInt(user_id));
            return root;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject encodeReport(String user_id, String post_id, String report) {
        JSONObject root = new JSONObject();
        try {
            root.put("user_id", user_id);
            root.put("post_id", post_id);
            root.put("content", report);
            return root;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject encodeLocationNotificationUpdater(Context context) {
        Location locatoin = new Location(Double.parseDouble(context.getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLng", "")), Double.parseDouble(context.getApplicationContext().getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLat", "")));
        double lng = locatoin.getLongitude();
        double lat = locatoin.getLatitude();
        String emergencyID = context.getSharedPreferences("currentUser", Context.MODE_PRIVATE).getString("emergencyID", "");
        String userID = context.getSharedPreferences("currentUser", Context.MODE_PRIVATE).getString("user_id", "");
        JSONObject root = new JSONObject();
        try {
            root.put("emergency_id", Integer.parseInt(emergencyID));
            root.put("user_id", Integer.parseInt(userID));
            JSONObject loc = new JSONObject();
            loc.put("longitude", lng);
            loc.put("latitude", lat);
            root.put("location", loc);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return root;
    }

    public JSONObject encodeLocationUpdater(Context context) {
        Location locatoin = new Location(Double.parseDouble(context.getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLng", "")), Double.parseDouble(context.getApplicationContext().getApplicationContext().getSharedPreferences("currentUser", Context.MODE_MULTI_PROCESS).getString("currentLat", "")));
        double lng = locatoin.getLongitude();
        double lat = locatoin.getLatitude();
        String userID = context.getSharedPreferences("currentUser", Context.MODE_PRIVATE).getString("user_id", "");
        JSONObject root = new JSONObject();
        try {
            root.put("user_id", Integer.parseInt(userID));
            JSONObject loc = new JSONObject();
            loc.put("longitude", lng);
            loc.put("latitude", lat);
            root.put("location", loc);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return root;
    }

}
