package com.example.lenovo.notarea.Presenter.view_model_adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lenovo.notarea.Model.ExtraNumbers;
import com.example.lenovo.notarea.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EmergencyNumbersAdapter extends ArrayAdapter<ExtraNumbers> {


    private HashMap<String, Integer> map = new HashMap<>();

    public EmergencyNumbersAdapter(@NonNull Context context, @NonNull ArrayList<ExtraNumbers> objects) {
        super(context, 0, objects);
        this.map.put("ambulance", R.drawable.ambulance);
        this.map.put("fire", R.drawable.fire);
        this.map.put("mechanic", R.drawable.khrba2i);
        this.map.put("police", R.drawable.police);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listViewItem = convertView;
        if (listViewItem == null) {
            listViewItem = LayoutInflater.from(getContext()).inflate(R.layout.emergency_number_item, parent, false);
        }

        ExtraNumbers emergency = getItem(position);

        LinearLayout emergency_parent = listViewItem.findViewById(R.id.emergency_parent);
        ImageView img = listViewItem.findViewById(R.id.emergency_number_img);
        TextView name = listViewItem.findViewById(R.id.emergency_name);
        TextView number = listViewItem.findViewById(R.id.emergency_number);

        if (emergency.getName() != null) {
            img.setImageResource(map.get(emergency.getName()));
            name.setVisibility(View.VISIBLE);
            name.setText(emergency.getName());
        } else {
            name.setVisibility(View.GONE);
            img.setVisibility(View.GONE);
        }

        if (emergency.getNumber() != null) {
            number.setVisibility(View.VISIBLE);
            number.setText(emergency.getNumber());
        } else {
            number.setVisibility(View.GONE);
        }

        return listViewItem;
    }
}
