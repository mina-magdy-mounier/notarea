package com.example.lenovo.notarea.View.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Presenter.AdsPresenter;
import com.example.lenovo.notarea.Presenter.JsonParser;
import com.example.lenovo.notarea.Presenter.UserPreseneter;
import com.example.lenovo.notarea.Presenter.view_model_adapters.AdsPostsAdapter;
import com.example.lenovo.notarea.R;

public class UserSavedADSPosts extends Fragment {

    UserPreseneter userPreseneter;
    JsonParser jsonParser = new JsonParser();
    AdsPresenter adsPresenter;
    SharedPreferences session;
    static AdsPostsAdapter adsPostsAdapter;
    private LinearLayout my_posts;
    private LinearLayout has_posts;
    ListView listView;

    public UserSavedADSPosts() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_saved_adsposts, container, false);
        session = view.getContext().getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        userPreseneter = new UserPreseneter(session);
        adsPresenter = new AdsPresenter(session);
        listView = view.findViewById(R.id.savedADSpostsList);
        my_posts = view.findViewById(R.id.my_posts);
        has_posts = view.findViewById(R.id.has_posts);
        init();

        return view;
    }

    public void init() {
        my_posts.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);
        has_posts.setVisibility(View.GONE);
        userPreseneter.getMySavedADSPosts(new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                if (result != null) {
                    if (!result.equals("error") && !result.equals("php_error")) {
                        try {
                            my_posts.setVisibility(View.GONE);
                            listView.setVisibility(View.VISIBLE);
                            has_posts.setVisibility(View.GONE);
                            userPreseneter.getUser().setSavedAds(jsonParser.decodeGlobaleAdsPost(result));
                            adsPostsAdapter = new AdsPostsAdapter(getActivity(), userPreseneter.getUser().getSavedAds(), userPreseneter.getUser().getId());
                            adsPostsAdapter.setUserSavedADSPosts(UserSavedADSPosts.this);
                            listView.setAdapter(adsPostsAdapter);
                        } catch (Exception e) {

                        }
                    } else if (result.equals("error")) {
                        has_posts.setVisibility(View.VISIBLE);
                        my_posts.setVisibility(View.GONE);
                        listView.setVisibility(View.GONE);
                    } else {
                        Snackbar.make(listView, "connection error", Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    public void RemoveListViewItems(final int postion, final String postID) {
        adsPresenter.removePost(postion, postID, new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                if (result.equals("\"Advertisment Deleted\"")) {
                    userPreseneter.getUser().getSavedAds().remove(postion);
                    adsPostsAdapter.notifyDataSetChanged();
                }
            }
        });
    }
}
