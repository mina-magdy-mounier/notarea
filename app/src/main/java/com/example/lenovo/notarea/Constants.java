package com.example.lenovo.notarea;


public class Constants {

    private final String URL = "http://notarea-env.wkixcnxppq.eu-west-2.elasticbeanstalk.com/";
    private final String FIREBASE_STORAGE = "notarea";
    private final String MAIL = "notarea@gmail.com";
    private final String MAIL_SUBJECT = "Notarea User Message";

    // GENERAL
    private int G_MAX_DISTANCE = 5000;
    private int G_MIN_DISTANCE = 500;
    private int G_DISTANCE_STEP = 500;

    // ADS
    private int ADS_MAX_DAYS = 60;
    private int ADS_MIN_DAYS = 1;
    private int ADS_DAYS_STEP = 1;

    private int ADS_MAX_DISTANCE = 20000;
    private int ADS_MIN_DISTANCE = 500;
    private int ADS_DISTANCE_STEP = 500;

    //Age
    private int MAX_AGE = 2002;
    private int MIN_AGE = 1800;
    private int current_year = 2018;

    private int RATE_MIN = 1;


    private int RATE_MAX = 10;
    private int RATE_STEP = 1;

    public Constants() {
    }

    public String getURL() {
        return URL;
    }

    public String getFIREBASE_STORAGE() {
        return FIREBASE_STORAGE;
    }

    public int getG_MAX_DISTANCE() {
        return G_MAX_DISTANCE;
    }

    public int getG_MIN_DISTANCE() {
        return G_MIN_DISTANCE;
    }

    public int getG_DISTANCE_STEP() {
        return G_DISTANCE_STEP;
    }

    public int getADS_MAX_DAYS() {
        return ADS_MAX_DAYS;
    }

    public int getADS_MIN_DAYS() {
        return ADS_MIN_DAYS;
    }

    public int getADS_DAYS_STEP() {
        return ADS_DAYS_STEP;
    }

    public int getADS_MAX_DISTANCE() {
        return ADS_MAX_DISTANCE;
    }

    public int getADS_MIN_DISTANCE() {
        return ADS_MIN_DISTANCE;
    }

    public int getADS_DISTANCE_STEP() {
        return ADS_DISTANCE_STEP;
    }

    public int getMAX_AGE() {
        return MAX_AGE;
    }

    public int getMIN_AGE() {
        return MIN_AGE;
    }

    public int getCurrent_year() {
        return current_year;
    }

    public String getMAIL() {
        return MAIL;
    }

    public int getRATE_MIN() {
        return RATE_MIN;
    }

    public int getRATE_MAX() {
        return RATE_MAX;
    }

    public int getRATE_STEP() {
        return RATE_STEP;
    }

    public String getMAIL_SUBJECT() {
        return MAIL_SUBJECT;
    }


}
