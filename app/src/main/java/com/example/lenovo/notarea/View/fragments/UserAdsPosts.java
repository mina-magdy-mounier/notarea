package com.example.lenovo.notarea.View.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.lenovo.notarea.CallBacks.ServerCallback;
import com.example.lenovo.notarea.Presenter.AdsPresenter;
import com.example.lenovo.notarea.Presenter.JsonParser;
import com.example.lenovo.notarea.Presenter.UserPreseneter;
import com.example.lenovo.notarea.Presenter.view_model_adapters.AdsPostsAdapter;
import com.example.lenovo.notarea.R;

public class UserAdsPosts extends Fragment {

    private UserPreseneter userPreseneter;
    private AdsPresenter adsPresenter;
    private SharedPreferences session;
    private JsonParser jsonParser = new JsonParser();
    private AdsPostsAdapter adsPostsAdapter;
    public ListView adsListView;
    private LinearLayout my_posts;
    private LinearLayout has_posts;


    public UserAdsPosts() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View ads = inflater.inflate(R.layout.user_ads_posts, container, false);

        session = ads.getContext().getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        userPreseneter = new UserPreseneter(session);
        adsPresenter = new AdsPresenter(session);
        adsListView = ads.findViewById(R.id.ads_list_view);
        my_posts = ads.findViewById(R.id.my_posts);
        has_posts = ads.findViewById(R.id.has_posts);
        init();

        return ads;
    }


    private void init() {
        my_posts.setVisibility(View.VISIBLE);
        adsListView.setVisibility(View.GONE);
        userPreseneter.getMyADSPosts(new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                if (result != null) {
                    if (!result.equals("error") && !result.equals("php_error")) {
                        try {
                            my_posts.setVisibility(View.GONE);
                            adsListView.setVisibility(View.VISIBLE);
                            has_posts.setVisibility(View.GONE);
                            userPreseneter.getUser().setMyAds(jsonParser.decodeGlobaleAdsPost(result));
                            adsPostsAdapter = new AdsPostsAdapter(getActivity(), userPreseneter.getUser().getMyAds(), userPreseneter.getUser().getId());
                            adsPostsAdapter.setUserAdsPosts(UserAdsPosts.this);
                            adsListView.setAdapter(adsPostsAdapter);
                        } catch (Exception e) {

                        }
                    } else if (result.equals("error")) {
                        has_posts.setVisibility(View.VISIBLE);
                        my_posts.setVisibility(View.GONE);
                        adsListView.setVisibility(View.GONE);
                    } else {
                        Snackbar.make(adsListView, "connection error", Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    public void RemoveListViewItems(final int postion, final String postID) {
        adsPresenter.removePost(postion, postID, new ServerCallback() {
            @Override
            public void onSuccess(String result) {
                if (result.equals("\"Advertisment Deleted\"")) {
                    userPreseneter.getUser().getMyAds().remove(postion);
                    adsPostsAdapter.notifyDataSetChanged();
                }
            }
        });
    }

}
