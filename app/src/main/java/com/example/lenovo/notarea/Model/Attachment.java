package com.example.lenovo.notarea.Model;

import java.util.ArrayList;

public class Attachment {
    private String id;
    private String postID;
    private String facebook_url;
    private ArrayList<String> phoneNumbersIDs;
    private ArrayList<ExtraNumbers> phoneNumber;
    private Location location;


    public Attachment() {
    }

    // ===========================  Setters & Getters ============================================//

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPostID() {
        return postID;
    }

    public void setPostID(String postID) {
        this.postID = postID;
    }

    public String getFacebook_url() {
        return facebook_url;
    }

    public void setFacebook_url(String facebook_url) {
        this.facebook_url = facebook_url;
    }

    public ArrayList<ExtraNumbers> getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(ArrayList<ExtraNumbers> phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public ArrayList<String> getPhoneNumbersIDs() {
        return phoneNumbersIDs;
    }

    public void setPhoneNumbersIDs(ArrayList<String> phoneNumbersIDs) {
        this.phoneNumbersIDs = phoneNumbersIDs;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    // ===========================  CRUD ============================================//
}
