package com.example.lenovo.notarea.FireBase;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.lenovo.notarea.Presenter.SessionHandler;
import com.example.lenovo.notarea.R;
import com.example.lenovo.notarea.View.activity.MainActivity;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {
    final String TAG = "FirebaseMessaging";

    private MainActivity mainActivity = new MainActivity();
    private SharedPreferences session;
    private String User_id, user_id, c_user_name, postType, content, json, user_name;
    private SessionHandler sessionHandler = new SessionHandler();
    private JSONObject Json;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        session = getSharedPreferences("currentUser", Context.MODE_PRIVATE);
        user_id = session.getString("user_id", null);
        c_user_name = session.getString("user_name", null);
        if (remoteMessage.getData() != null) {
            postType = remoteMessage.getData().get("type");
            json = remoteMessage.getData().get("json");
            User_id = remoteMessage.getData().get("User_id");
            user_name = remoteMessage.getData().get("User_name");

            if (postType.equals("general")) {
                run(0, "GeneralPageFragemnt", json, "post", "general post");
            } else if (postType.equals("advertisment")) {
                run(1, "AdsPageFragment", json, "post", "advertisment");
            } else if (postType.equals("emergency")) {
                try {
                    Json = new JSONObject(json);
                    content = Json.optJSONObject("emergency").optString("content");
                    if (!User_id.equals(user_id)) {
                        make_notify(2, "emergency", content, MainActivity.class);
                    }
                    goToWith("EmergencyPageFragment", json);
                    goToWith("MapsActivity", json);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void goToWith(String activity_name, String json) {
        Intent intent = new Intent(activity_name);
        intent.putExtra("json", json);
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.sendBroadcast(intent);
    }


    private void make_notify(int id, String title, String content, Class activity) {
        Intent intent = new Intent(this, activity);
        intent.putExtra("which_activity", String.valueOf(id));
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(FirebaseMessagingService.this, "ChannelID");
        builder.setDefaults(Notification.PRIORITY_MIN);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle(title);
        builder.setContentText(content);
        builder.setContentIntent(pendingIntent);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(FirebaseMessagingService.this);
        notificationManager.notify(id, builder.build());

    }

    private void run(int id, String fragment_name, String json, String key, String post_type) {
        try {
            Json = new JSONObject(json);
            content = Json.optJSONObject(key).optString("content");
            goToWith(fragment_name, json);
            if (!User_id.equals(user_id) && sessionHandler.get_notification_state(session) == 1) {
                make_notify(id, "HI, " + c_user_name, user_name + " added new " + post_type, MainActivity.class);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
